<?php
Route::group(['middleware' => ['web']], function () {
	/*
	|--------------------------------------------------------------------------
	| Application Routes
	|--------------------------------------------------------------------------
	|
	| Here is where you can register all of the routes for an application.
	| It's a breeze. Simply tell Laravel the URIs it should respond to
	| and give it the controller to call when that URI is requested.
	|
	*/
	#Patterns
	Route::pattern('id', '[0-9]+');
	Route::pattern('id2', '[0-9]+');
	#END------------------------------------------------------------------------------------------------------

	#Index
	Route::get('/', 'HomeController@indexGet');
	#END------------------------------------------------------------------------------------------------------

	#Login, Register & Recover--------------------------------------------------------------------------------
	Route::post('entrar', 'Auth\AuthController@loginPost');

	Route::get('registar', 'Auth\AuthController@registerGet');
	Route::post('registar', 'Auth\AuthController@registerPost');
	
	Route::get('recuperar-password', 'Auth\PasswordController@getRecover');
	Route::post('recuperar-password', 'Auth\PasswordController@postRecover');
	Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
	Route::post('password/reset/', 'Auth\PasswordController@postReset');
	Route::get('sair', 'Auth\AuthController@logout');
	

	#END------------------------------------------------------------------------------------------------------
	
	
	
	#Backoffice Routes----------------------------------------------------------------------------------------
	if (Request::is('backoffice/*') || Request::is('backoffice'))
		require __DIR__.'/backoffice_routes.php';
	#END------------------------------------------------------------------------------------------------------

});

Route::group(['middleware' => ['web2']], function () {
	#Front----------------------------------------------------------------------------------------------------
	Route::get('intro', 'HomeController@introGet');
	
	Route::get('record', 'HomeController@recordGet');
	Route::post('record', 'HomeController@recordPost');
	
	Route::post('getDegrees', 'HomeController@getDegrees');
	
	Route::get('vemo-nos-em-maio', 'HomeController@maioGet');
	
	#END------------------------------------------------------------------------------------------------------
});