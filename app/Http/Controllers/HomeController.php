<?php namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;

use App\Models\User;
use App\Models\Video;
use App\Models\Degree;

use App\Http\Requests\SimpleRequest;

use Mail;
use File;
use Response;

class HomeController extends BaseController{

    /**
     * Initializer.
     *
     * @return \AdminController
     */
    public function __construct()
    {
		$user_id = Auth::id();
		
		$this->user = NULL;
		
		if($user_id){
			$this->user = User::find($user_id);
			if(!empty($this->user)){
				foreach($this->user->roles as $role){
					$name = $role->name;
					$this->user->$name = TRUE;
				}
				$full_name_segments = explode(" ", $this->user->name);
				$this->user->first_name = $full_name_segments[0];
			}
		}
		elseif(\Route::currentRouteAction() != 'App\Http\Controllers\HomeController@indexGet' &&
				\Route::currentRouteAction() != 'App\Http\Controllers\HomeController@getDegrees'){
			\Redirect::to('/')->send();
		}
		
		view()->share(['user'=> $this->user]);	
    }

	public function indexGet(){
		
		return view('site.index');
	}
	
	public function recordGet(){
		$videos = $this->user->videos()->get();
		foreach($videos as $item){
			if(!empty($item) && $item->video_path != NULL)
				return redirect('vemo-nos-em-maio');
		}
		return view('site.record', compact('videos'));
	}
	
	public function recordPost(SimpleRequest $request){
		$videoObject = Video::where(['user_id' => $this->user->id])->first();
		
		if(empty($videoObject)){
			$videoObject = new Video ();
			$videoObject->user_id = $this->user->id;
		}
			
		foreach(['video', 'audio'] as $type) {
			if($request->hasFile("${type}-blob")){
				$file = $request->file("${type}-blob");
				$name = str_random(40).'.'.($type == 'video' ? 'webm' : 'wav');
				$file->move('appfiles/videos', $name);
				if($type == 'video')
					$videoObject->video_path = $name;
				else
					$videoObject->audio_path = $name;
				
				$videoObject->save();
				
				$user = $this->user;
			}
		}
		if(!empty($videoObject->video_path))
			Mail::send('emails.record', [], function ($m) use ($user) {
				$m->from('hello@ytconference.com', 'Tell Us Your Story');
				$m->to($user->email, $user->name)->subject('A tua história já cá canta!');
			});
		
	}
	
	public function introGet(){
		
		return view('site.intro');
	}
	
	public function maioGet(){	
		return view('site.maio');
	}
	
	public function getDegrees(SimpleRequest $request){
		$not_in_list = new Degree(['name' => 'Outro']);
		$deg = new Degree(['name' => 'Curso']);
		$degrees = Degree::orderBy('name')->where(['university_id' => $request->id])->get()->prepend($not_in_list)->prepend($deg);
		
		return Response::json($degrees);
	}
	
}
