<?php
namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\UserLoginRequest;
use App\Http\Requests\Auth\UserRegisterRequest;

use App\Models\User;
use App\Models\University;
use App\Models\Degree;

use App\Models\AssignedRoles;
use Carbon\Carbon;
use Mail;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
	
	protected $guard = 'web';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'first_name' => $data['first_name'],
			'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
	
	/**
	 * Show the application register.
	 *
	 * @Get("register")
	 *
	 * @return Response
	 */
	public function registerGet(){
		$uni = new University(['name' => 'Universidade']);
		$not_in_list = new University(['name' => 'Outra']);
		$universities = University::orderBy('name')->get()->prepend($not_in_list)->prepend($uni);
		return view('site.register', compact('universities'));
	}
	
	/**
	 * Handle a registration request for the application.
	 *
	 * @Post("auth/register")
	 *
	 * @param RegisterRequest $request            
	 * @return Response
	 */
	public function registerPost(UserRegisterRequest $request) {
		$user = new User();
		
		$user->name = $request->name;
		$user->email = $request->email;
		$user->birthdate = $request->birthdate;
		$user->phone = $request->phone;
		$user->address = $request->address;
		$user->university = $request->university;
		$user->university_custom = $request->university_custom;
		$user->degree = $request->degree;
		$user->degree_custom = $request->degree_custom;
		$user->password = \Hash::make($request->password);
		$user->confirmed = 1;
		$user->save();
		
		$role = new AssignedRoles();
		$role -> role_id = 2;
		$role -> user_id = $user -> id;
		$role -> save();

		Mail::send('emails.register', ['user' => $user, 'password' => $request->password], function ($m) use ($user) {
            $m->from('hello@ytconference.com', 'Tell Us Your Story');
            $m->to($user->email, $user->name)->subject('Conta-nos a tua história! ');
        });
		
		Auth::login($user);

		//return redirect('/intro')->with('alert', ['msg'=>'Utilizador registado com sucesso', 'class'=>'alert-success', 'hide'=>true]);
	}

	/**
	 * Handle a login request to the application.
	 *
	 * @Post("auth/login")
	 *
	 * @param LoginRequest $request            
	 * @return Response
	 */
	public function loginPost(UserLoginRequest $request) {
		
		if (Auth::guard($this->guard)->attempt(['email' => $request->email, 'password' => $request->password], $request->RememberMe)){
			if($id = Auth::id()){
				$user = User::find($id);
				if($user->is('Admin'))
					return redirect('backoffice');
				else
					return redirect('intro');
			}
			
			
		}
		else{
			return redirect('/')->withErrors([
				'email' => 'Utilizador ou password inválidos'
			]);
		}
	}

	/**
	 * Log the user out of the application.
	 *
	 * @Get("auth/logout")
	 *
	 * @return Response
	 */
	public function logout() {
		Auth::logout();
		setcookie("floatwork_token", "", time()-3600, '/');
		return redirect('/');
	}
	
}
