<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;

use App\Http\Requests\Api\SetScoreRequestApi;

use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Contracts\Auth\Guard;

use App\Models\File as File_Model;
use App\Models\File_interaction;

use App\Helpers\VideoStream;
use Carbon\Carbon;
use Response;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use File; 

class FilesController extends ApiController{
    protected $user;

    /**
     * Inject the models.
     * 
     * @param \Post $post            
     * @param \User $user            
     */
    public function __construct(Guard $auth, PasswordBroker $passwords)
    {
        parent::__construct();
		$this->auth = $auth;
		$this->passwords = $passwords;
    }
	
	/**
	 * Retrieves a list of new or updated files wich the user has access to, based on a date.
	 *
	 * @GET("files/new")
	 *
	 * @param Unix Timestamp $date
	 * @return Json
	 */
	public function getNewOrUpdated($date){
		$user = JWTAuth::parseToken()->toUser();
		
		$date = Carbon::createFromFormat('U', $date);

		$files = File_Model::
				where('available_at', '<', Carbon::now())->
				where('updated_at', '>', $date)->get();
		
		foreach($files as $file){
			if(!empty($file->thumbnail) && File::exists('appfiles/'.$file->thumbnail)){
				$file->thumbnail_base64 = base64_encode(File::get('appfiles/'.$file->thumbnail));
			}
			else
				$file->thumbanail_base64 = NULL;
		}
		
		return Response::json($files);
	}
	
	/**
	 * Download file based on id checking first if the user has permission.
	 *
	 * @GET("files/download/{id}")
	 *
	 * @param Int id
	 * @return File
	 */
	public function download($id){
		
		$user = JWTAuth::parseToken()->toUser();
		
		$file = File_model::find($id);
		
		if(empty($file))
			return Response::json(['success'=>false, 'message'=>'Ficheiro indisponível'], 422);
		
		return Response::download('appfiles/files/'. $file->path);
		
	}
	
	/**
	 * Download file based on id checking first if the user has permission.
	 *
	 * @GET("files/download/{id}")
	 *
	 * @param Int id
	 * @return File
	 */
	public function stream($id, $token){
		if(!$id)
			return Response::json('error id');
		if(!$token)
			return Response::json('error token');
			
		
		try{
			$user = JWTAuth::authenticate($token);
		}
		catch(JWTException $e){
			return Response::json(['success'=>false, 'message'=>'Não possui credenciais válidas'], 422);
		}
		
		$file = File_model::find($id);
		
		
		
		if(empty($file))
			return Response::json(['success'=>false, 'message'=>'Ficheiro indisponível'], 422);
		
		$interaction = File_interaction::where(['user_id'=>$user->id, 'file_id'=>$file->id])->first();
		
		if(!empty($interaction)){
			$interaction->view = 1;
		}
		else{
			$interaction = new File_interaction();
			$interaction->file_id = $file->id;
			$interaction->user_id = $user->id;
			$interaction->view = 1;
		}
		$interaction->save();
		if(in_array($file->file_type_id, [10, 19, 20])){
			$stream = new VideoStream('appfiles/files/' . $file->path, 1, $file->file_duration);
		}
		else
			$stream = new VideoStream('appfiles/files/' . $file->path);
		$stream->start();
	}
	
	/**
	 * Sets stars to file
	 *
	 * @POST("files/star/setScore")
	 *
	 * @param int $id_file, int $score
	 * @return Json
	 */
	public function setScore(SetScoreRequestApi $request){
		$user = JWTAuth::parseToken()->toUser();
		$star = File_interaction::where(['file_id'=>$request->file_id, 'user_id'=>$user->id])->first();
		$file = File_Model::find($request->file_id);
		if(!empty($star)){
			$star->score = $request->score;
		}
		else{
			$star = new File_interaction();
			$star->file_id = $request->file_id;
			$star->user_id = $user->id;
			$star->score = $request->score;
		}
		$star->save();
		
		$count = File_interaction::where('file_id', $request->file_id)->where('score', '>',0)->count();
		$score = File_interaction::where('file_id', $request->file_id)->where('score', '>',0)->sum('score');
		$average = round($score / $count, 2);
		
		$file -> votes_count = $count;
		$file -> votes_score = $score;
		$file -> votes_average = $average;
		$file -> save();
		
		Return Response::json(['success'=>true, 'average'=>$average]);
	}
	
	/**
	 * Get stars of file
	 *
	 * @GET("files/stars/getScore")
	 *
	 * @param int $id_file, int $score
	 * @return Json
	 */
	public function getScore($id){
		$user = JWTAuth::parseToken()->toUser();
		$star = File_interaction::where(['file_id'=>$id, 'user_id'=>$user->id])->first();
		
		$file = File_Model::find($id);
		
		Return Response::json(['success'=>true, 'average'=>!empty($file) ? $file->votes_average : 0, 'my_vote' => !empty($star) ? $star->score : 0]);
	}
	
	/**
	 * Get rank
	 *
	 * @GET("files/stars/getRank")
	 *
	 * @return Json
	 */
	public function getRank(){
		
		$files = File_Model::select('id', 'name', 'votes_average')->orderBy('votes_average', 'DESC')->get();
		
		Return Response::json($files);
	}
	
	/**
	 * Get rank
	 *
	 * @GET("files/stars/getRank")
	 *
	 * @return Json
	 */
	public function viewed(){
		$user = JWTAuth::parseToken()->toUser();
		$files = File_Model::select('id')->get();
		foreach($files as $file){
			$interaction = File_interaction::where(['file_id'=>$file->id, 'user_id'=>$user->id])->first();
			if(empty($interaction))
				$file->viewed = false;
			else
				$file->viewed = $interaction->view;
		}
		
		Return Response::json($files);
	}
	
} 