<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;

use App\Http\Requests\Api\UserLoginRequestApi;
use App\Http\Requests\Api\UserRegisterRequestApi;
use App\Http\Requests\Api\UserRecoverRequestApi;
use App\Http\Requests\Api\UserUpdateRequestApi;

use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;

use App\Models\User_code;
use App\Models\User;

use Carbon\Carbon;

use Response;
use JWTAuth;
use Input;

class UserController extends ApiController{
    protected $user;

    /**
     * Inject the models.
     * 
     * @param \Post $post            
     * @param \User $user            
     */
    public function __construct(Guard $auth, PasswordBroker $passwords)
    {
        parent::__construct();
		$this->auth = $auth;
		$this->passwords = $passwords;
    }

    public function login(UserLoginRequestApi $request){
		$credentials = Input::only('email', 'password');
		if ( !$token = JWTAuth::attempt($credentials))
			return Response::json(['success' => false, 'errors' => ['email' => ['Autenticação falhada.']]], 403);
		
		return Response::json(['success' => true, 'message' => 'Login efetuado com sucesso', 'token' => $token]);
    }
	
	public function confirmToken(){
		if (empty(JWTAuth::parseToken()->toUser()))
			return Response::json(['success' => false, 'errors' => ['email' => ['Autenticação falhada.']]], 403);
		else
			return Response::json(['success' => true, 'message' => 'Login efetuado com sucesso']);
    }
	
	public function register(UserRegisterRequestApi $request){

		$credentials = $request->all();

		$user = User::create($credentials);
		
		$token = JWTAuth::fromUser($user);

		return Response::json(['success' => true, 'message' => 'Registo efetuado com sucesso', 'token' => $token]);
	}
	
	public function update(UserUpdateRequestApi $request){
		$user = JWTAuth::parseToken()->toUser();
		$user->fill($request->all());
		$user->save();
		
		return Response::json(['success' => true, 'message' => 'Perfil alterado com sucesso']);
	}
	
	public function recover(UserRecoverRequestApi $request){
		
		$response = $this->passwords->sendResetLink($request->only('email'), function($message){
			$message->subject('Rheumachannel - Redefinir password');
		});
		
		switch ($response){
			case PasswordBroker::RESET_LINK_SENT:
				return Response::json(['success' => true, 'message' => 'Pedido de recuperação enviado com sucesso. Por favor verifique o seu E-mail.']);

			case PasswordBroker::INVALID_USER:
				return Response::json(['success' => false, 'message' => 'Não existe utilizador associado a este E-mail.'], 422);
		}
		
	}
	
	public function profile(){
		$user = JWTAuth::parseToken()->toUser();
		
		return Response::json(['success' => true, 'user' => $user->toArray()]);
	}
	
} 