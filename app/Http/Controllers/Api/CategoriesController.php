<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;


use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Contracts\Auth\Guard;

use App\Models\Category;
use Response;
use JWTAuth;
use File;

class CategoriesController extends ApiController{
    protected $user;

    /**
     * Inject the models.
     * 
     * @param \Post $post            
     * @param \User $user            
     */
    public function __construct(Guard $auth, PasswordBroker $passwords)
    {
        parent::__construct();
		$this->auth = $auth;
		$this->passwords = $passwords;
    }
	
	/**
	 * Retrieves a list of user categories
	 *
	 * @GET("categories")
	 *
	 * @param Unix Timestamp $date
	 * @return Json
	 */
	public function categories(){
		$user = JWTAuth::parseToken()->toUser();
		$categories = Category::all();
		foreach($categories as $category){
			if(!empty($category->thumbnail) && File::exists('appfiles/'.$category->thumbnail)){
				$category->thumbnail_base64 = base64_encode(File::get('appfiles/'.$category->thumbnail));
			}
			else
				$category->thumbanail_base64 = NULL;
		}
		return Response::json($categories);
		
	}
	
	/**
	 * Retrieves a category
	 *
	 * @GET("categories")
	 *
	 * @param Unix Timestamp $date
	 * @return Json
	 */
	public function item($id){
		$category = Category::find($id);
		if(!empty($category->thumbnail) && File::exists('appfiles/'.$category->thumbnail)){
				$category->thumbnail_base64 = base64_encode(File::get('appfiles/'.$category->thumbnail));
			}
			else
				$category->thumbanail_base64 = NULL;
		return Response::json($category);
		
	}
	
} 