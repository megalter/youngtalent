<?php namespace app\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

use App\Models\User;

use View;

class BaseController extends Controller {

    /**
     * Initializer.
     *
     * @access   public
     * @return \BaseController
     */
    public function __construct(){
		$this->middleware('auth');
		
		$user_id = Auth::id();
		
		if($user_id){
			$this->user = User::find($user_id);
			foreach($this->user->roles as $role){
				$name = $role->name;
				$this->user->$name = TRUE;
			}
		}
		else{
			$this->user = NULL;
		}
		
		
		view()->share(['user'=> $this->user]);	
    }
	
	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	
	
}