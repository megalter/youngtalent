<?php namespace App\Http\Controllers;

use Illuminate\Routing\Controller;

class ApiController extends Controller {

    /**
     * Initializer.
     *
     * @return \AdminController
     */
    public function __construct(){
		// Apply the jwt.auth middleware to all methods in this controller
		// except for the authenticate method. We don't want to prevent
		// the user from retrieving their token if they don't already have it
		$this->middleware('jwt.auth', ['except' => ['login', 'register', 'recover', 'stream', 'confirmToken']]);
    }
	
}