<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Models\Role;
use App\Models\Permission;
use App\Models\PermissionRole;
use App\Http\Requests\Admin\RoleRequest;
use App\Http\Requests\Admin\DeleteRequest;
use Datatables;

class RoleController extends AdminController {
    /*
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index()
    {
        // Show the page
        return view('backoffice.roles.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate() {
        // Get all the available permissions
        $permissionsAdmin = Permission::where('is_admin','=',1)->get();
        $permissionsUser = Permission::where('is_admin','=',0)->get();
		$permissions = Permission::all();
		$readPermissions = array();
		$writePermissions = array();
        // Selected permissions
        $permisionsadd =array();

        // Show the page
        return view('backoffice.roles.create_edit', compact('permissionsAdmin', 'permissionsUser','permisionsadd','permissions', 'readPermissions', 'writePermissions'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate(RoleRequest $request) {

        $is_admin = 0;
        // Check if role is for admin user
        if(!empty($request->permission)){
	    	$permissionsAdmin = Permission::where('is_admin','=',1)->get();
		    foreach ($permissionsAdmin as $perm){
	            foreach($request->permission as $item){
	                if($item==$perm['id'] && $perm['is_admin']=='1')
	                {
	                    $is_admin = 1;
	                }
	            }
	        }
		}
        $role = new Role();
        $role -> is_admin = $is_admin;
        $role -> name = $request->name;
        $role -> save();
		if($request->readPermissions){
			foreach($request->readPermissions as $item){
				$permission = new PermissionRole();
				$permission->permission_id = $item;
				$permission->role_id = $role->id;
				
				if($request->writePermissions && in_array($item, $request->writePermissions))
					$permission->value = 3;
				else
					$permission->value = 2;
				
				$permission->save();
			}
		}
		if($request->writePermissions){
			foreach($request->writePermissions as $item){
				if($request->readPermissions && !in_array($item, $request->readPermissions) || !$request->readPermissions){
					$permission = new PermissionRole();
					$permission->permission_id = $item;
					$permission->role_id = $role->id;
					$permission->value = 1;
					$permission->save();
				}
					
			}
		}
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $role
     * @return Response
     */
    public function getEdit($id) {
        $role = Role::find($id);
        $permissionsAdmin = Permission::where('is_admin','=',1)->get();
        $permissionsUser = Permission::where('is_admin','=',0)->get();
        $permisionsadd = PermissionRole::where('role_id','=',$id)->select('permission_id')->get();
		$permissions = Permission::all();
		$readPermissions = PermissionRole::where('role_id', $id)->where('value', '>', 1)->lists('permission_id')->toArray();
		
		$writePermissions = PermissionRole::where('role_id', $id)->where(function($query){
			$query
					->where('value', 1)
					->orWhere('value', 3);
		})->lists('permission_id')->toArray();
		
        // Show the page
        return view('backoffice.roles.create_edit', compact('role', 'permissionsAdmin', 'permissionsUser','permisionsadd', 'permissions', 'readPermissions', 'writePermissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $role
     * @return Response
     */
    public function postEdit(RoleRequest $request, $id) {
        $is_admin = 0;
		if(!empty($request->permission)){
	        $permissionsAdmin = Permission::where('is_admin','=',1)->get();
	        foreach ($permissionsAdmin as $perm){
	            foreach($request->permission as $item){
	                if($item==$perm['id'] && $perm['is_admin']=='1')
	                {
	                    $is_admin = 1;
	                }
	            }
	        }
		}
        $role = Role::find($id);
        $role -> is_admin = $is_admin;
        $role -> name = $request->name;
        $role -> save();

        PermissionRole::where('role_id','=',$id) -> delete();
		if($request->readPermissions){
			foreach($request->readPermissions as $item){
				$permission = new PermissionRole();
				$permission->permission_id = $item;
				$permission->role_id = $role->id;
				
				if($request->writePermissions && in_array($item, $request->writePermissions))
					$permission->value = 3;
				else
					$permission->value = 2;
				
				$permission->save();
			}
		}
		if($request->writePermissions){
			foreach($request->writePermissions as $item){
				if($request->readPermissions && !in_array($item, $request->readPermissions || !$request->readPermissions)){
					$permission = new PermissionRole();
					$permission->permission_id = $item;
					$permission->role_id = $role->id;
					$permission->value = 1;
					$permission->save();
				}
					
			}
		}
		
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $role
     * @return Response
     */

    public function getDelete($id)
    {
        $role = Role::find($id);
        // Show the page
        return view('backoffice.roles.delete', compact('role'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $post
     * @return Response
     */
    public function postDelete(DeleteRequest $request,$id)
    {
        $role = Role::find($id);
        $role->delete();
    }
    /**
     * Show a list of all the languages posts formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function data()
    {
        $roles = Role::select(array('roles.id','roles.name','roles.created_at'));

        return Datatables::of($roles)
            ->add_column('actions', '<a href="{{{ URL::to(\'backoffice/roles/\' . $id . \'/edit\' ) }}}" class="btn btn-success btn-sm iframe" ><span class="glyphicon glyphicon-pencil"></span>  {{ Lang::get("admin/modal.edit") }}</a>
                    <a href="{{{ URL::to(\'backoffice/roles/\' . $id . \'/delete\' ) }}}" class="btn btn-sm btn-danger iframe"><span class="glyphicon glyphicon-trash"></span> {{ Lang::get("admin/modal.delete") }}</a>
                ')
            ->remove_column('id')
            ->make();
    }

}
