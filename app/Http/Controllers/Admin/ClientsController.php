<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Models\Client;
use App\Http\Requests\Admin\ClientRequest;
use App\Http\Requests\Admin\DeleteRequest;
use Datatables;
use Response;

class ClientsController extends AdminController {

    /*
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index($id = 0){
		
        return view('backoffice.clients.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate() {
        return view('backoffice.clients.create_edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate(ClientRequest $request) {

         $client = new Client ();
        $client -> name = $request->name;
		$client -> folder = $request->folder;
		
		if($request->hasFile('logo') && $request->file('logo')->isValid()){
			$file = $request->file('logo');
			$extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
			$name = 'clients/'.str_random(40).'.'.$extension;
			$file->move('appfiles/clients', $name);
			$client -> logo = $name;
		}
        $client -> save();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $user
     * @return Response
     */
    public function getEdit($id) {

        $edit = Client::find($id);
        return view('backoffice.clients.create_edit', compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $user
     * @return Response
     */
    public function postEdit(ClientRequest $request, $id) {
		
        $client = Client::find($id);
        $client -> name = $request->name;
		$client -> folder = $request->folder;
        
		if($request->hasFile('logo')){
			if(!empty($client->logo))
				\File::delete('clients/' . $client->logo);
			$file = $request->file('logo');
			$extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
			$name = 'clients/'.str_random(40).'.'.$extension;
			$file->move('appfiles/clients', $name);
			$client -> logo = $name;
		}
		
        $client -> save();
        
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param $user
     * @return Response
     */

    public function getDelete($id)
    {
        $client = Client::find($id);
        // Show the page
        return view('backoffice.clients.delete', compact('client'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $user
     * @return Response
     */
    public function postDelete(DeleteRequest $request)
    {
        $client= Client::find($request->id);
        $client->delete();
    }

    /**
     * Show a list of all the languages posts formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function data($id = 0)
    {
		$clients = Client::select('id', 'name','folder', 'logo', 'created_at');
		
        return Datatables::of($clients)
            ->edit_column('logo', '@if(!empty($logo))<img src="{{asset("appfiles")}}/{{$logo}}" class="img-thumbnail">@endif')
            ->add_column('actions', '<a href="{{{ URL::to(\'backoffice/clients/edit/\' . $id) }}}" class="btn btn-success btn-sm iframe" ><span class="glyphicon glyphicon-pencil"></span>  {{ Lang::get("admin/modal.edit") }}
                    <a href="{{{ URL::to(\'backoffice/clients/delete/\' . $id) }}}" class="btn btn-sm btn-danger iframe"><span class="glyphicon glyphicon-trash"></span> {{ Lang::get("admin/modal.delete") }}</a>
                ')
            ->remove_column('id', 'updated_at', 'deleted_at')

            ->make();
    }
	
}

