<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;

use App\Models\AssignedRoles;
use App\Models\Permission;
use App\Models\Avaliation;
use App\Models\Video;
use App\Models\Role;
use App\Models\User;
use App\Models\University;
use App\Models\Degree;

use App\Http\Requests\Admin\AvaliationRequest;
use App\Http\Requests\Admin\UserRequest;
use App\Http\Requests\Admin\UserEditRequest;
use App\Http\Requests\Admin\DeleteRequest;
use App\Http\Requests\SimpleRequest;
use Carbon\Carbon;
use Datatables;
use CsvWriter;
use Response;
use Hash;

class UserController extends AdminController {

    /*
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index($id = 0){
		$roles = Role::all();
		$all_roles = new Role();
		$all_roles->name = 'Função';
		$roles->prepend($all_roles);
		if($id != 0)
			$role = Role::find($id);
        // Show the page
        return view('backoffice.users.index', compact('roles', 'role'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate() {
        $roles = Role::all();
		$permissions = Permission::where('front_end', 1)->get();
		$readPermissions = array();
		$writePermissions = array();
        // Selected groups
        $selectedRoles = array();
		$selectedPermissions = $permissions->lists('id')->toArray();
        return view('backoffice.users.create_edit', compact('roles','permissions', 'selectedRoles', 'selectedPermissions', 'readPermissions', 'writePermissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate(UserRequest $request) {

        $user = new User ();
        $user -> first_name = $request->first_name;
		$user -> last_name = $request->last_name;
        $user -> email = $request->email;
        $user -> password = Hash::make($request->password);
        $user -> confirmation_code = str_random(32);
        $user -> confirmed = $request->confirmed;
        $user -> save();

		if($request->roles){
			foreach($request->roles as $item)
			{
				$role = new AssignedRoles();
				$role -> role_id = $item;
				$role -> user_id = $user -> id;
				$role -> save();
			}
		}
		else{
			$role = new AssignedRoles();
			$role -> role_id = 2;
			$role -> user_id = $user -> id;
			$role -> save();
		}
		if($request->readPermissions){
			foreach($request->readPermissions as $item){
				if($request->writePermissions && in_array($item, $request->writePermissions))
					$user->permissions()->attach($item, array('value'=>3, 'updated_at'=>Date('Y-m-d h:i:s')));
				else
					$user->permissions()->attach($item, array('value'=>2, 'updated_at'=>Date('Y-m-d h:i:s')));
			}
		}
		if($request->writePermissions){
			foreach($request->writePermissions as $item){
				if($request->readPermissions && !in_array($item, $request->readPermissions))
					$user->permissions()->attach($item, array('value'=>1, 'updated_at'=>Date('Y-m-d h:i:s')));
			}
		}
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $user
     * @return Response
     */
    public function getEdit($id) {

        $edit = User::find($id);
        $roles = Role::all();
		$permissions = Permission::where('front_end', 1)->get();
        $selectedRoles = AssignedRoles::where('user_id','=',$edit->id)->lists('role_id')->all();
		$readPermissions = $edit->permissions()->where('value', '>', 1)->lists('permissions.id')->all();
		$writePermissions = $edit->permissions()->where(function($query){
			$query
					->where('value', 1)
					->orWhere('value', 3);
		})->lists('permissions.id')->all();
		
		$uni = new University(['name' => 'Universidade']);
		$not_in_list = new University(['name' => 'Outra']);
		$universities = University::orderBy('name')->get()->prepend($not_in_list)->prepend($uni);
		
		$degrees = Degree::where('university_id', $edit->university)->get();
        return view('backoffice.users.create_edit', compact('edit', 'roles','permissions', 'selectedRoles', 'readPermissions','writePermissions','universities', 'degrees'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $user
     * @return Response
     */
    public function postEdit(UserEditRequest $request, $id) {
		
        $user = User::find($id);
        $user -> first_name = $request->first_name;
		$user -> last_name = $request->last_name;
        $user -> confirmed = $request->confirmed;

        $password = $request->password;
        $passwordConfirmation = $request->password_confirmation;

        if (!empty($password)) {
            if ($password === $passwordConfirmation) {
                $user -> password = Hash::make($password);
            }
        }
        $user -> save();
        AssignedRoles::where('user_id','=',$user->id)->delete();
		if($request->roles)
			foreach($request->roles as $item)
			{
				$role = new AssignedRoles;
				$role -> role_id = $item;
				$role -> user_id = $user -> id;
				$role -> save();
			}
			
		\DB::table('users_permissions')->where(array('user_id' => $id))->delete();
		if($request->readPermissions){
			foreach($request->readPermissions as $item){
				if($request->writePermissions && in_array($item, $request->writePermissions))
					$user->permissions()->attach($item, array('value'=>3, 'updated_at'=>Date('Y-m-d h:i:s')));
				else
					$user->permissions()->attach($item, array('value'=>2, 'updated_at'=>Date('Y-m-d h:i:s')));
			}
		}
		if($request->writePermissions){
			foreach($request->writePermissions as $item){
				if($request->readPermissions && !in_array($item, $request->readPermissions))
					$user->permissions()->attach($item, array('value'=>1, 'updated_at'=>Date('Y-m-d h:i:s')));
			}
		}
    }
	
	/**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getAvaliate($id){
		$video = Video::where('user_id', '=', $id)->first();
		$avaliation = Avaliation::where('user_id', '=', $id)->first();
		if(empty($avaliation)){
			$avaliation = Avaliation::create(array('user_id' => $id));
		}
			
        return view('backoffice.users.avaliate', compact('avaliation', 'id', 'video'));
    }
	
	 /**
     * Update the specified resource in storage.
     *
     * @param $avaliate
     * @return Response
     */
    public function postAvaliate(AvaliationRequest $request, $id) {
		
        $avaliation = Avaliation::where('user_id', '=', $id)->first();
		$fields = ['agility', 'leadership', 'connectivity', 'determination', 'pragmatism'];
		$total = 0;
		
		foreach($fields as $item){
			for($i = 1; $i < 4; $i++){
				$field = $item . $i;
				$avaliation -> $field = $request -> $field;
				$total += $request -> $field;
			}
		}
		
		$avaliation -> notes = $request->notes;
		$avaliation -> average = number_format( $total / 15, 2);
		$avaliation->save();
    }
	
    /**
     * Remove the specified resource from storage.
     *
     * @param $user
     * @return Response
     */

    public function getDelete($id)
    {
        $user = User::find($id);
        // Show the page
        return view('backoffice.users.delete', compact('user'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $user
     * @return Response
     */
    public function postDelete(DeleteRequest $request)
    {
        $user= User::find($request->id);
        $user->delete();
    }
	
	/**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getVideo($id) {
        $video = Video::find($id);
        return view('backoffice.users.video', compact('video'));
    }

		/**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function postExportCsv(SimpleRequest $request) {
		return Response::json(['name' => self::createCsv($request->id)]);
    }

		/**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getExportCsv($name) {
		return Response::download('appfiles/csv/'.$name.'.csv', $name.'.csv', ['Content-Type' => 'text/csv']);
    }
	
	
	private function createCsv($ids = null){
		$name = Date('Y-m-d');
		$csv_writer = CsvWriter::create('appfiles/csv/'.$name.'.csv');
		if(!empty($ids))
			$users = User::leftJoin('universities', 'universities.id', '=', 'users.university')
				->leftJoin('degrees', 'degrees.id', '=', 'users.degree')
				->select('users.name', 'email', 'birthdate', 'phone', 'address', 'universities.name as university', 'university_custom', 'degrees.name as degree', 'degrees.degree_type', 'degree_custom', 'created_at')
				->whereIn('users.id', $ids)->get()->toarray();
		else
			$users = User::leftJoin('universities', 'universities.id', '=', 'users.university')
				->leftJoin('degrees', 'degrees.id', '=', 'users.degree')
				->select('users.name', 'email', 'birthdate', 'phone', 'address', 'universities.name as university', 'university_custom', 'degrees.name as degree', 'degrees.degree_type', 'degree_custom', 'created_at')
				->get()->toarray();
		
		array_unshift( $users , ['Nome', 'E-mail', 'Data de nascimento', 'Telefone', 'Morada', 'Univeridade', 'Universidade (manual)', 'Curso', 'Grau', 'Curso (manual)', 'Data criação']);
		
		$csv_writer->writeAll($users);
		
		return $name;
	}
	
	
    /**
     * Show a list of all the languages posts formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function data(){

			$users = User::select(array('users.id','users.name','users.email', 'videos.id as video', 'avaliation.average', 'users.created_at'))
					->leftjoin('avaliation', 'avaliation.user_id', '=', 'users.id')
					->leftjoin('videos', 'videos.user_id', '=', 'users.id');

		
        return Datatables::of($users)
			->edit_column('id', '<input type="checkbox" name="id[{{$id}}]" value="{{$id}}" class="cbox">')
			->edit_column('video', '@if(!empty($video)) <a href="{{{ URL::to(\'backoffice/users/video/\' . $video) }}}" class="iframe"> <span class="glyphicon glyphicon-film"></span></a> @endif')
            ->add_column('actions', '<a href="{{{ URL::to(\'backoffice/users/edit/\' . $id) }}}" class="btn btn-success btn-sm iframe" ><span class="glyphicon glyphicon-pencil"></span>  {{ Lang::get("admin/modal.edit") }}</a>
                    <a href="{{{ URL::to(\'backoffice/users/delete/\' . $id) }}}" class="btn btn-sm btn-danger iframe"><span class="glyphicon glyphicon-trash"></span> {{ Lang::get("admin/modal.delete") }}</a>
					<a href="{{{ URL::to(\'backoffice/users/avaliate/\' . $id) }}}" class="btn btn-sm btn-info iframe"><span class="glyphicon glyphicon-star-empty"></span> {{ Lang::get("admin/modal.avaliate") }}</a>
                ')
            ->remove_column('last_name', 'path')

            ->make();
    }
	
}

