<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Models\User;
use App\Models\Channel;
use App\Models\Category;
use App\Models\AssignedRoles;
use App\Models\Role;
use App\Models\Permission;
use App\Http\Requests\Admin\CategoryRequest;
use App\Http\Requests\Admin\UserEditRequest;
use App\Http\Requests\Admin\DeleteRequest;
use Datatables;
use Hash;
use Response;

class CategoriesController extends AdminController {

    /*
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index($id = 1){
		$categories = Category::all();
        // Show the page
        return view('backoffice.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate() {
		
        return view('backoffice.categories.create_edit', compact('channel', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate(CategoryRequest $request) {

        $category = new Category ();
        $category -> name = $request->name;
		$category -> description = $request->description;
		
		if($request->hasFile('thumbnail') && $request->file('thumbnail')->isValid()){
			$file = $request->file('thumbnail');
			$extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
			$name = 'categories/'.str_random(40).'.'.$extension;
			$file->move('appfiles/categories', $name);
			$category -> thumbnail = $name;
		}
        $category -> save();
		
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $user
     * @return Response
     */
    public function getEdit($id) {

        $edit = Category::find($id);
		
        return view('backoffice.categories.create_edit', compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $user
     * @return Response
     */
    public function postEdit(CategoryRequest $request, $id) {
		
		$editCategory = Category::find($id);
        $editCategory -> name = $request->name;
		$editCategory -> description = $request->description;
		
		if($request->hasFile('thumbnail')){
			if(!empty($editCategory->thumbnail))
				\File::delete('appfiles/' . $editCategory->thumbnail);
			$file = $request->file('thumbnail');
			$extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
			$name = 'categories/'.str_random(40).'.'.$extension;
			$file->move('appfiles/categories', $name);
			$editCategory -> thumbnail = $name;
		}
		
        $editCategory -> save();
        
    }
	
    /**
     * Remove the specified resource from storage.
     *
     * @param $user
     * @return Response
     */

    public function getDelete($id)
    {
        $category = Category::find($id);
        // Show the page
        return view('backoffice.categories.delete', compact('category'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $user
     * @return Response
     */
    public function postDelete(DeleteRequest $request, $id)
    {
        $category= Category::find($id);
		if(!empty($category->thumbnail))
			\File::delete('appfiles/' . $category->thumbnail);
        $category->delete();
    }

    /**
     * Show a list of all the languages posts formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function data()
    {
		
        $categories = Category::select(array('categories.id','categories.name', 'categories.thumbnail','categories.created_at'))
            ->orderBy('categories.id', 'ASC');
		
        return Datatables::of($categories)
			->edit_column('thumbnail', '@if(!empty($thumbnail))<img src="{{asset("appfiles")}}/{{$thumbnail}}" class="img-thumbnail" width="50" height="50">@endif')
            ->add_column('actions', '<a href="{{{ URL::to(\'backoffice/categories/edit/\' . $id) }}}" class="btn btn-success btn-sm iframe" ><span class="glyphicon glyphicon-pencil"></span>  {{ Lang::get("admin/modal.edit") }}</a>
                    <a href="{{{ URL::to(\'backoffice/categories/delete/\'. $id) }}}" class="btn btn-sm btn-danger iframe"><span class="glyphicon glyphicon-trash"></span> {{ Lang::get("admin/modal.delete") }}</a>
                ')
            ->remove_column('id','category_id','css_class')

            ->make();
    }
	
}

