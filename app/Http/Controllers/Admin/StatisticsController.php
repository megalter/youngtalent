<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Models\File as File_Model;
use Illuminate\Support\Collection;
use App\Models\File_interaction;
use App\Models\File_type;
use App\Models\Category;
use App\Models\User;
use Carbon\Carbon;
use Datatables;
use Response;
use File;

class StatisticsController extends AdminController {

    /*
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index(){
		
        // Show the page
        return view('backoffice.statistics.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate($id = NULL) {
		$category = Category::find($id);
		$categories = Category::all();
        return view('backoffice.files.create_edit', compact('category','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate(FileRequest $request) {

        $file = new File_Model ();
        $file -> name = $request->name;
		$file -> description = $request->description;
		$file -> file_updated_at = Carbon::now();
		$file -> category_id = $request->category_id;
		try{
			if(empty($request->available_at))
				$file -> available_at = Carbon::now();
			if($request->hasFile('thumbnail') && $request->file('thumbnail')->isValid()){
				$thumb_file = $request->file('thumbnail');
				$extension = pathinfo($thumb_file->getClientOriginalName(), PATHINFO_EXTENSION);
				$name = str_random(40).'.'.$extension;
				$thumb_file->move('appfiles/files', $name);
				$file -> thumbnail = 'files/'.$name;
			}
			if($request->hasFile('path') && $request->file('path')->isValid()){
				$path_file = $request->file('path');
				$extension = pathinfo($path_file->getClientOriginalName(), PATHINFO_EXTENSION);
				$ftype = File_type::where('name', $extension)->first();
				$name = str_random(40).'.'.$extension;
				$path_file->move('appfiles/files', $name);
				$file -> path = $name;
				if(!empty($ftype))
					$file -> file_type_id = $ftype->id;

				//Get lenght of movie/audio
				$getID3 = new getID3;
				$info = $getID3->analyze('appfiles/files/' . $file->path);
				$file->file_duration = $info['playtime_string'];
			}
			
			$file -> save();
		}
		catch(Exception $e){
			if(!empty($file -> thumbnail) && File::exists('appfiles/' . $file -> thumbnail))
				File::delete('appfiles/' . $file -> thumbnail);
			if(!empty($file -> path) && File::exists('appfiles/files/' . $file -> path))
				File::delete('appfiles/files/' . $file -> path);
			var_dump($info);
		}

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $user
     * @return Response
     */
    public function getEdit($id) {
		$edit = File_Model::find($id);
		$categories = Category::all();

        return view('backoffice.files.create_edit', compact('edit', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $user
     * @return Response
     */
    public function postEdit(FileEditRequest $request, $id) {
		$file = File_Model::find($id);
		$file -> name = $request->name;
		$file -> description = $request->description;
		$file -> available_at = $request->available_at;
		$file -> category_id = $request->category_id;
		if($request->hasFile('thumbnail') && $request->file('thumbnail')->isValid()){
			//Delete old file
			if(!empty($file->thumbnail))
				\File::delete('appfiles/' . $file->thumbnail);
			$thumb_file = $request->file('thumbnail');
			$extension = pathinfo($thumb_file->getClientOriginalName(), PATHINFO_EXTENSION);
			$name = str_random(40).'.'.$extension;
			$thumb_file->move('appfiles/files', $name);
			$file -> thumbnail = 'files/'.$name;
		}
		if($request->hasFile('path') && $request->file('path')->isValid()){
			//Delete old file
			if(!empty($file->path))
				\File::delete('appfiles/files/' . $file->path);
			$path_file = $request->file('path');
			$extension = pathinfo($path_file->getClientOriginalName(), PATHINFO_EXTENSION);
			$ftype = File_type::where('name', $extension)->first();
			$name = str_random(40).'.'.$extension;
			$path_file->move('appfiles/files', $name);
			$file -> path = $name;
			if(!empty($ftype))
				$file -> file_type_id = $ftype->id;
			$file -> file_updated_at = Carbon::now();
		}
        $file -> save();
    }
	
    /**
     * Remove the specified resource from storage.
     *
     * @param $user
     * @return Response
     */
    public function getDelete($id)
    {
        $file = File_Model::find($id);
        // Show the page
        return view('backoffice.files.delete', compact('file'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $user
     * @return Response
     */
    public function postDelete(DeleteRequest $request)
    {
        $file= File_Model::find($request->id);
        $file->delete();
		
		return Response::json(true);
    }
	
	/**
     * Remove the specified resource from storage.
     *
     * @param $user
     * @return Response
     */
    public function postArchivate(ArchivateRequest $request){
		$file = File_Model::find($request->file_id);
		if(!empty($file))
			$file->category_id = 1;
		$file->save();
		
		return Response::json(true);
		
    }
	
	/**
	* Show a list of all the files per channel for Datatables.
	*
	* @return Datatables JSON
	*/
    public function filesData()
    {
		$files = File_model::all(['id', 'name', 'thumbnail', 'votes_count', 'votes_average', 'created_at'])->sortByDesc('created_at');
		foreach($files as $file){
			$views = File_interaction::where('file_id', $file->id)->sum('view');
			$file->views = !empty($views) ? $views : 0;
		}
		
        return Datatables::of($files)
			->edit_column('thumbnail', '@if(!empty($thumbnail))<img src="{{asset("appfiles")}}/{{$thumbnail}}" class="img-thumbnail" width="50" height="50">@endif')
			->remove_column('created_at')
            ->make();
    }

	/**
	* Show a list of all the files per channel for Datatables.
	*
	* @return Datatables JSON
	*/
    public function usersData()
    {
		$users = User::select('users.id', 'users.first_name', 'users.last_name', 'assigned_roles.role_id')
				->join('assigned_roles', 'users.id', '=', 'assigned_roles.user_id')->where('assigned_roles.role_id', '=',2)->get();
		foreach($users as $item){
			$views = File_interaction::where('user_id', $item->id)->sum('view');
			$item->views = !empty($views) ? $views : 0;
			$votes = File_interaction::where('user_id', $item->id)->where('score', '>',0)->count('score');
			$item->votes = !empty($votes) ? $votes : 0;
		}
		
        return Datatables::of($users)
				->edit_column('first_name', '{{$first_name . " " . $last_name}} ')
				->remove_column('last_name')
            ->make();
    }
}

