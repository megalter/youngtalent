<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Http\Requests\Admin\NavCreateRequest;
use App\Helpers\Routes_Manager;
use App\Models\Category;
use App\Models\User;
use App\Models\Video;
use Datatables;
use File;

use Illuminate\Support\Collection;

class DashboardController extends AdminController {

    public function __construct()
    {
        parent::__construct();
    }

	public function index()
	{
        $title = "Backoffice";
		$total_users = User::count();
        $total_videos = Video::count();
		return view('backoffice.dashboard.index',  compact('title', 'total_users', 'total_videos'));
	}
	
	public function navigation($route_id = NULL, $area_id = 0){
		$all_files = File::files('app/Http');
		$all_areas = [];
		$all_areas[0] = 'Escolha a área';
		
		$route_files = [];
		
		foreach($all_files as $key=>$file){
			if(str_contains($file, 'route')){
				$route_files[$key] = $file;
				if(($route_id != NULL && $key == $route_id) || ($route_id == NULL && $file == 'app/Http/routes.php'))
					$route_file = ['key'=>$key, 'file'=>$file];
			}
		}
		$rotrav = new Routes_Manager($all_files[$route_file['key']]);
		$lines = $rotrav->lines;
		$temp_areas = $rotrav->areas;
		foreach($temp_areas as $key => $area)
			$all_areas[$key] = $area->name;
		return view('backoffice.dashboard.navigation_index',  compact('title','route_files', 'route_file', 'all_areas', 'area_id'));
	}
	
	 /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function navGetCreate($id = NULL) {
		$all_files = File::files('app/Http');
		$all_areas = [];
		$all_areas[''] = "Nenhuma";
		$file = $all_files[$id];
		$lines = file($file);
		$all_methods = ['post', 'get', 'any'];
		
		foreach($lines as $key => $line){
			//Area
			if(str_contains($line, '#') && !str_contains($line, '#END')){
				$all_areas[$key] = str_ireplace (['#', '-'], '', $line);
			}
		}

        return view('backoffice.dashboard.navigation_create_edit', compact('file','all_areas', 'all_methods'));
    }
	
	 /**
     * Save resource.
     *
     * @return Response
     */
    public function navPostCreate(NavCreateRequest $request, $id) {
		$all_methods = ['post', 'get', 'any'];
		$new_route = "Route::".$all_methods[$request->method]."('".$request->path."', '".$request->controller."');\n";

		$lines = file($request->file, FILE_USE_INCLUDE_PATH);

		if(!empty($request->area)){
			array_splice($lines, $request->area+1, 0, $new_route);
		}
		else{
			if($lines[(count($lines)-1)] == '});')
				array_splice($lines, (count($lines)-1), 0, "	\n" . $new_route);
			else
				array_splice($lines, count($lines), 0, "\n".$new_route);
		}
		
		file_put_contents($request->file.'2', $lines);
		
    }
	
	/**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function navGetEdit($id, $id2) {
		$all_files = File::files('app/Http');
		$all_areas = [];
		$all_areas[''] = "Nenhuma";
		$file = $all_files[$id];
		$lines = file($file);
		$all_methods = ['post', 'get', 'any'];
		$edit = new \stdClass();
		
		$line = $lines[$id2];
		foreach($all_methods as $key=>$method){
			if(str_contains($line, $method))
				$edit->method = $key;
		}
		$temp = explode("'", $line);
		$edit->path = $temp[1];
		$edit->controller = $temp[3];
		
		foreach($lines as $key => $line){
			//Area
			if(str_contains($line, '#') && !str_contains($line, '#END')){
				$all_areas[$key] = str_ireplace (['#', '-'], '', $line);
			}
		}
		
		//Check area of route
		while(!str_contains($lines[$id2], ['#END', '#']) || $id2 != -1 ){
			$id2--;
		}
		if($id2 != 0)
			$edit->area = $id2;
		
        return view('backoffice.dashboard.navigation_create_edit', compact('file','all_areas', 'all_methods', 'edit'));
    }
	
	public function navdata($route_id, $area_id){
		$all_files = File::files('app/Http');
		$all_areas = [];
		$current_area = '';
		
		$navigation = [];
		$substrings = ['Route::post(', 'Route::get(', 'Route::any(', ');'];
		
        $lines = file($all_files[$route_id]);
		foreach($lines as $key=>$line){
			//Area
			if(str_contains($line, '#END'))
				$current_area = '';
			elseif(str_contains($line, '#')){
				$current_area = str_ireplace (['#', '-'], '', $line);
				$all_areas[$key] = $current_area;
			}
			
			//Method
			foreach(['post'=>'Route::post', 'get'=>'Route::get', 'any'=>'Route::any'] as $key=>$item){
				if(str_contains($line, $item)){
					$line = str_ireplace($substrings, "", $line);
					$route_segments = explode(', ', $line);
					if((isset($all_areas[$area_id]) && $all_areas[$area_id] == $current_area) || $area_id == 0)
						$navigation[] = [
							'id' => $key,
							'area' => $current_area,
							'method' => $key, 
							'name' => str_replace("'","",$route_segments[0]), 
							'controller' => str_replace("'","",$route_segments[1]),
						];
				}
			}
		}

        return Datatables::of(Collection::make($navigation))
            ->add_column('actions', '<a href="{{{ URL::to(\'backoffice/navigation/\' . $id . \'/edit\') }}}" class="btn btn-success btn-sm iframe" ><span class="glyphicon glyphicon-pencil"></span>  {{ Lang::get("admin/modal.edit") }}</a>'.
                    '<a href="{{{ URL::to(\'backoffice/navigation/\' . $id . \'/delete\' ) }}}" class="btn btn-sm btn-danger iframe"><span class="glyphicon glyphicon-trash"></span> {{ Lang::get("admin/modal.delete") }}</a>
                ')
            ->remove_column('id')

            ->make();
		
    }
}