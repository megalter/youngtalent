<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;

use App\Models\AssignedRoles;
use App\Models\Permission;
use App\Models\Avaliation;
use App\Models\Video;
use App\Models\Role;
use App\Models\User;

use App\Http\Requests\Admin\AvaliationRequest;
use App\Http\Requests\Admin\UserRequest;
use App\Http\Requests\Admin\UserEditRequest;
use App\Http\Requests\Admin\DeleteRequest;
use Datatables;
use Hash;
use Response;

class UniversityController extends AdminController {

    /*
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index(){
		
        // Show the page
        return view('backoffice.universities.index', compact('roles', 'role'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate() {
        $roles = Role::all();
		$permissions = Permission::where('front_end', 1)->get();
		$readPermissions = array();
		$writePermissions = array();
        // Selected groups
        $selectedRoles = array();
		$selectedPermissions = $permissions->lists('id')->toArray();
        return view('backoffice.users.create_edit', compact('roles','permissions', 'selectedRoles', 'selectedPermissions', 'readPermissions', 'writePermissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate(UserRequest $request) {

        $user = new User ();
        $user -> first_name = $request->first_name;
		$user -> last_name = $request->last_name;
        $user -> email = $request->email;
        $user -> password = Hash::make($request->password);
        $user -> confirmation_code = str_random(32);
        $user -> confirmed = $request->confirmed;
        $user -> save();

		if($request->roles){
			foreach($request->roles as $item)
			{
				$role = new AssignedRoles();
				$role -> role_id = $item;
				$role -> user_id = $user -> id;
				$role -> save();
			}
		}
		else{
			$role = new AssignedRoles();
			$role -> role_id = 2;
			$role -> user_id = $user -> id;
			$role -> save();
		}
		if($request->readPermissions){
			foreach($request->readPermissions as $item){
				if($request->writePermissions && in_array($item, $request->writePermissions))
					$user->permissions()->attach($item, array('value'=>3, 'updated_at'=>Date('Y-m-d h:i:s')));
				else
					$user->permissions()->attach($item, array('value'=>2, 'updated_at'=>Date('Y-m-d h:i:s')));
			}
		}
		if($request->writePermissions){
			foreach($request->writePermissions as $item){
				if($request->readPermissions && !in_array($item, $request->readPermissions))
					$user->permissions()->attach($item, array('value'=>1, 'updated_at'=>Date('Y-m-d h:i:s')));
			}
		}
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $user
     * @return Response
     */
    public function getEdit($id) {

        $edit = User::find($id);
        $roles = Role::all();
		$permissions = Permission::where('front_end', 1)->get();
        $selectedRoles = AssignedRoles::where('user_id','=',$edit->id)->lists('role_id')->all();
		$readPermissions = $edit->permissions()->where('value', '>', 1)->lists('permissions.id')->all();
		$writePermissions = $edit->permissions()->where(function($query){
			$query
					->where('value', 1)
					->orWhere('value', 3);
		})->lists('permissions.id')->all();
        return view('backoffice.users.create_edit', compact('edit', 'roles','permissions', 'selectedRoles', 'readPermissions','writePermissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $user
     * @return Response
     */
    public function postEdit(UserEditRequest $request, $id) {
		
        $user = User::find($id);
        $user -> first_name = $request->first_name;
		$user -> last_name = $request->last_name;
        $user -> confirmed = $request->confirmed;

        $password = $request->password;
        $passwordConfirmation = $request->password_confirmation;

        if (!empty($password)) {
            if ($password === $passwordConfirmation) {
                $user -> password = Hash::make($password);
            }
        }
        $user -> save();
        AssignedRoles::where('user_id','=',$user->id)->delete();
		if($request->roles)
			foreach($request->roles as $item)
			{
				$role = new AssignedRoles;
				$role -> role_id = $item;
				$role -> user_id = $user -> id;
				$role -> save();
			}
			
		\DB::table('users_permissions')->where(array('user_id' => $id))->delete();
		if($request->readPermissions){
			foreach($request->readPermissions as $item){
				if($request->writePermissions && in_array($item, $request->writePermissions))
					$user->permissions()->attach($item, array('value'=>3, 'updated_at'=>Date('Y-m-d h:i:s')));
				else
					$user->permissions()->attach($item, array('value'=>2, 'updated_at'=>Date('Y-m-d h:i:s')));
			}
		}
		if($request->writePermissions){
			foreach($request->writePermissions as $item){
				if($request->readPermissions && !in_array($item, $request->readPermissions))
					$user->permissions()->attach($item, array('value'=>1, 'updated_at'=>Date('Y-m-d h:i:s')));
			}
		}
    }
	
	/**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getAvaliate($id){
		$avaliation = Avaliation::where('user_id', '=', $id)->first();
		if(empty($avaliation)){
			$avaliation = Avaliation::create(array('user_id' => $id));
		}
			
        return view('backoffice.users.avaliate', compact('avaliation', 'id'));
    }
	
	 /**
     * Update the specified resource in storage.
     *
     * @param $avaliate
     * @return Response
     */
    public function postAvaliate(AvaliationRequest $request, $id) {
		
        $avaliation = Avaliation::where('user_id', '=', $id)->first();
		$fields = ['agility', 'leadership', 'connectivity', 'determination', 'pragmatism'];
		$total = 0;
		
		foreach($fields as $item){
			for($i = 1; $i < 4; $i++){
				$field = $item . $i;
				$avaliation -> $field = $request -> $field;
				$total += $request -> $field;
			}
		}
		
		$avaliation -> notes = $request->notes;
		$avaliation -> average = number_format( $total / 15, 2);
		$avaliation->save();
    }
	
    /**
     * Remove the specified resource from storage.
     *
     * @param $user
     * @return Response
     */

    public function getDelete($id)
    {
        $user = User::find($id);
        // Show the page
        return view('backoffice.users.delete', compact('user'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $user
     * @return Response
     */
    public function postDelete(DeleteRequest $request)
    {
        $user= User::find($request->id);
        $user->delete();
    }
	
	/**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getVideo($id) {
        $video = Video::find($id);
        return view('backoffice.users.video', compact('video'));
    }

    /**
     * Show a list of all the languages posts formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function data(){
		$universities = University::select(array('name', 'type', 'dependency', 'district'));
		
        return Datatables::of($universities)
            ->add_column('actions', '<a href="{{{ URL::to(\'backoffice/users/edit/\' . $id) }}}" class="btn btn-success btn-sm iframe" ><span class="glyphicon glyphicon-pencil"></span>  {{ Lang::get("admin/modal.edit") }}</a>
                    <a href="{{{ URL::to(\'backoffice/users/delete/\' . $id) }}}" class="btn btn-sm btn-danger iframe"><span class="glyphicon glyphicon-trash"></span> {{ Lang::get("admin/modal.delete") }}</a>
					<a href="{{{ URL::to(\'backoffice/users/avaliate/\' . $id) }}}" class="btn btn-sm btn-info iframe"><span class="glyphicon glyphicon-star-empty"></span> {{ Lang::get("admin/modal.avaliate") }}</a>
                ')
            ->remove_column('id')

            ->make();
    }
	
}

