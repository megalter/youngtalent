<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Models\Video;
use App\Models\File_type;
use App\Models\Category;
use App\Http\Requests\Admin\FileRequest;
use App\Http\Requests\Admin\FileEditRequest;
use App\Http\Requests\Admin\DeleteRequest;
use App\Http\Requests\Admin\ArchivateRequest;
use Carbon\Carbon;
use Datatables;
use Response;
use File;
use Exception;

require_once('getid3/getid3.php');
use getID3;

class VideosController extends AdminController {

    /*
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index(){
		// Show the page
        return view('backoffice.videos.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $user
     * @return Response
     */
    public function getEdit($id) {
		$edit = Video::find($id);

        return view('backoffice.videos.create_edit', compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $user
     * @return Response
     */
    public function postEdit(FileEditRequest $request, $id) {
		$video = Video::find($id);
		$video -> name = $request->name;
		$video -> description = $request->description;
		$video -> available_at = $request->available_at;
		$video -> category_id = $request->category_id;
		if($request->hasFile('thumbnail') && $request->file('thumbnail')->isValid()){
			//Delete old file
			if(!empty($video->thumbnail))
				\File::delete('appfiles/' . $video->thumbnail);
			$thumb_file = $request->file('thumbnail');
			$extension = pathinfo($thumb_file->getClientOriginalName(), PATHINFO_EXTENSION);
			$name = str_random(40).'.'.$extension;
			$thumb_file->move('appfiles/files', $name);
			$video -> thumbnail = 'files/'.$name;
		}
		if($request->hasFile('path') && $request->file('path')->isValid()){
			//Delete old file
			if(!empty($video->path))
				\File::delete('appfiles/files/' . $video->path);
			$path_file = $request->file('path');
			$extension = pathinfo($path_file->getClientOriginalName(), PATHINFO_EXTENSION);
			$ftype = File_type::where('name', $extension)->first();
			$name = str_random(40).'.'.$extension;
			$path_file->move('appfiles/files', $name);
			$video -> path = $name;
			if(!empty($ftype))
				$video -> file_type_id = $ftype->id;
			$video -> file_updated_at = Carbon::now();
		}
        $video -> save();
    }
	
    /**
     * Remove the specified resource from storage.
     *
     * @param $user
     * @return Response
     */
    public function getDelete($id)
    {
        $video = Video::find($id);
        // Show the page
        return view('backoffice.videos.delete', compact('file'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $user
     * @return Response
     */
    public function postDelete(DeleteRequest $request)
    {
        $video= Video::find($request->id);
        $video->delete();
		
		return Response::json(true);
    }
	
	/**
     * Remove the specified resource from storage.
     *
     * @param $user
     * @return Response
     */
    public function postArchivate(ArchivateRequest $request){
		$video = Video::find($request->file_id);
		if(!empty($video))
			$video->category_id = 1;
		$video->save();
		
		return Response::json(true);
		
    }
	
	/**
	* Show a list of all the files per channel for Datatables.
	*
	* @return Datatables JSON
	*/
    public function data()
    {

		$videos = Video::select(array('videos.id', 'videos.name', 'videos.thumbnail'));

        return Datatables::of($videos)
			->edit_column('thumbnail', '@if(!empty($thumbnail))<img src="{{asset("appfiles")}}/{{$thumbnail}}" class="img-thumbnail" width="50" height="50">@endif')
			->add_column('actions', '<a href="{{{ URL::to(\'backoffice/files/edit/\' . $id) }}}" class="btn btn-success btn-sm iframe" ><span class="glyphicon glyphicon-pencil"></span>  {{ Lang::get("admin/modal.edit") }}</a>')
            ->make();
    }

}

