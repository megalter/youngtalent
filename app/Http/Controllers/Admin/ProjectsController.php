<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Models\Project;
use App\Models\Platform;
use App\Models\Client;
use App\Models\Country;
use App\Http\Requests\Admin\ProjectRequest;
use App\Http\Requests\Admin\DeleteRequest;
use Datatables;
use Response;
use File;

class ProjectsController extends AdminController {

    /*
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index($id = 0){
		
        return view('backoffice.projects.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate() {
		$clients = Client::all()->sortBy('name');
		$platforms = Platform::all();
		$countries = Country::all();
        return view('backoffice.projects.create_edit', compact('clients', 'platforms', 'countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate(ProjectRequest $request) {
		$project = new Project ();
        $project -> name = $request->name;
		$project -> folder = $request->folder;
		$project -> client_id = $request->client_id;
		$project -> platform_id = $request->client_id;
		$project -> country_id = $request->country_id;
		if($request->hasFile('thumbnail') && $request->file('thumbnail')->isValid()){
			$file = $request->file('thumbnail');
			$extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
			$name = 'projects/'.str_random(40).'.'.$extension;
			$file->move('appfiles/projects', $name);
			$project -> thumbnail = $name;
		}
		
        $project -> save();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $user
     * @return Response
     */
    public function getEdit($id) {
		$clients = Client::all()->sortBy('name');
		$platforms = Platform::all();
		$countries = Country::all();
        $edit = Project::find($id);
        return view('backoffice.projects.create_edit', compact('edit', 'clients', 'platforms', 'countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $user
     * @return Response
     */
    public function postEdit(ProjectRequest $request, $id) {
		
        $project = Project::find($id);
        $project -> name = $request->name;
		$project -> folder = $request->folder;
        $project -> client_id = $request->client_id;
		$project -> platform_id = $request->client_id;
		$project -> country_id = $request->country_id;
		if($request->hasFile('thumbnail')){
			if(!empty($project->thumbnail))
				\File::delete('projects/' . $project->thumbnail);
			$file = $request->file('thumbnail');
			$extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
			$name = 'projects/'.str_random(40).'.'.$extension;
			$file->move('appfiles/projects', $name);
			$project -> thumbnail = $name;
		}
		
        $project -> save();
        
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param $user
     * @return Response
     */

    public function getDelete($id)
    {
        $project = Project::find($id);
        // Show the page
        return view('backoffice.projects.delete', compact('project'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $user
     * @return Response
     */
    public function postDelete(DeleteRequest $request)
    {
        $project= Project::find($request->id);
        $project->delete();
    }

    /**
     * Show a list of all the languages posts formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function data($id = 0)
    {
		$projects = Project::select(array('projects.id', 'projects.name', 'projects.thumbnail','projects.folder', 'countries.class','platforms.name as platform', 'clients.name as client', 'projects.created_at', 'projects.dev_path'))
				->join('platforms', 'platforms.id', '=', 'projects.platform_id')
				->join('countries', 'countries.id', '=', 'projects.country_id')
				->join('clients', 'clients.id', '=', 'projects.client_id');
		
        return Datatables::of($projects)
			->edit_column('thumbnail', '@if(!empty($thumbnail))<img src="{{asset("appfiles")}}/{{$thumbnail}}" class="img-thumbnail" width="50" height="50">@endif')
			->edit_column('name', '@if(!empty($dev_path))<a href="{{$dev_path}}" target="_blank">{{$name}}</a>@else {{$name}} @endif')
			->edit_column('class', '@if(!empty($class))<img class="flag {{$class}}">@endif')
            ->add_column('actions', '<a href="{{{ URL::to(\'backoffice/projects/edit/\' . $id) }}}" class="btn btn-success btn-sm iframe" ><span class="glyphicon glyphicon-pencil"></span>  {{ Lang::get("admin/modal.edit") }}
                    <a href="{{{ URL::to(\'backoffice/projects/delete/\' . $id) }}}" class="btn btn-sm btn-danger iframe"><span class="glyphicon glyphicon-trash"></span> {{ Lang::get("admin/modal.delete") }}</a>
                ')
            ->remove_column('id', 'dev_path')

            ->make();
    }
}

