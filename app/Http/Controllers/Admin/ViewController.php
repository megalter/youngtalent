<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Http\Requests\Admin\NavCreateRequest;
use App\Http\Requests\Admin\DeleteRequest;
use App\Helpers\Routes_Manager;
use Datatables;
use File;
use Illuminate\Support\Collection;

class ViewController extends AdminController {

	public $offices = ['backoffice', 'emails', 'errors', 'site'];
	
	public $areas = array();
	
	public $views = array();
	
    public function __construct()
    {
		foreach($this->offices as $id=>$office){
			//Offices folders aka areas
			$this->areas[$id] = File::directories('resources/views/'.$office);
			
			$this->views[$id][0] = File::files('resources/views/'.$office);
			
			foreach($this->areas[$id] as $key=>$folder){
				$this->views[$id][($key+1)] = File::files($folder);
			}
			array_unshift($this->areas[$id], 'root');
		}
		
        parent::__construct();
    }

	public function index($office_id = 0, $area_id = 0){
		$offices = $this->offices;
		$areas = $this->areas[$office_id];
		
		return view('backoffice.views.index', compact('offices', 'areas', 'office_id', 'area_id'));
	}
	
	 /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate($id) {
		$routMan = new Routes_Manager($id);
		$all_areas = $routMan->areas->output();
		$file = $routMan->file;
		$all_methods = ['post', 'get', 'any'];
		
        return view('backoffice.navigation.create_edit', compact('file','all_areas', 'all_methods'));
    }
	
	 /**
     * Save resource.
     *
     * @return Response
     */
    public function postCreate(NavCreateRequest $request, $id) {
		$routMan = new Routes_Manager($id);
		$line = array(
			'controller' => $request->controller,
			'name' => $request->path,
			'method' => $request->method
		);
		
		$routMan->addRoute($line, $request->area);
		
		$routMan->save();
    }
	
	/**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getEdit($id, $id2) {
		$routMan = new Routes_Manager($id);
		$route_id = $id;
		$all_areas = $routMan->areas->output();
		$file = $routMan->file;
		$all_methods = ['post', 'get', 'any'];
		$edit = $routMan->routes[$id2];
		
        return view('backoffice.navigation.create_edit', compact('file', 'all_areas', 'all_methods', 'edit', 'route_id'));
    }
	
	 /**
     * Save resource.
     *
     * @return Response
     */
    public function postEdit(NavCreateRequest $request, $id, $id2) {
		$routMan = new Routes_Manager($id);
		$all_methods = ['post', 'get', 'any'];
		$routMan->editRoute($id2, [
			'controller' => $request->controller,
			'method' => $all_methods[$request->method],
			'name' => $request->path,
			'Area' => $routMan->areas[$request->area]
		]);
		
		$routMan->save();
		
    }
	
	 /**
     * Remove the specified resource from storage.
     *
     * @param $user
     * @return Response
     */

    public function getDelete($id, $id2)
    {
        // Show the page
        return view('backoffice.navigation.delete', compact('id', 'id2'));
    }
	
	    /**
     * Remove the specified resource from storage.
     *
     * @param $user
     * @return Response
     */
    public function postDelete(DeleteRequest $request)
    {
        $routMan = new Routes_Manager($request->id);
        $routMan->delRoute($request->id2);
		$routMan->save();
    }
	
	public function data($office_id, $area_id){
		$temp = $this->views[$office_id][$area_id];
		$views = [];
		foreach($temp as $key => $view){
			$views[] = ['id' => $key, 'name' => $view];
		}
        return Datatables::of(Collection::make($views))
            ->add_column('actions', '<a href="{{{ URL::to(\'backoffice/views/edit/'.$office_id.'/'.$area_id.'/\' . $id) }}}" class="btn btn-success btn-sm iframe" ><span class="glyphicon glyphicon-pencil"></span>  {{ Lang::get("admin/modal.edit") }}</a>'.
                    '<a href="{{{ URL::to(\'backoffice/navigation/delete/'.$office_id.'/'.$area_id.'/\' . $id) }}}" class="btn btn-sm btn-danger iframe"><span class="glyphicon glyphicon-trash"></span> {{ Lang::get("admin/modal.delete") }}</a>
                ')
            ->remove_column('id')

            ->make();
		
    }
}