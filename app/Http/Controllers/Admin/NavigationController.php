<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Http\Requests\Admin\NavCreateRequest;
use App\Http\Requests\Admin\DeleteRequest;
use App\Helpers\Routes_Manager;
use Datatables;
use File;
use Illuminate\Support\Collection;

class NavigationController extends AdminController {

	public $areas = array();
	
	public $files = array();
	
    public function __construct()
    {
		$temp_files = File::files('app/Http');
				
		foreach($temp_files as $key=>$file){
			if(str_contains($file, 'route')){
				$this->route_files[$key] = $file;
			}
		}
		
        parent::__construct();
    }

	public function index($route_id = 6, $area_id = 0){
		
		$routMan = new Routes_Manager($route_id);
		
		$this->lines = $routMan->lines;
		
		$this->areas = $routMan->areas->output();
		
		return view('backoffice.navigation.index',  array(
			'title' => 'Navegação',
			'route_files' => $this->route_files, 
			'route_file' => $route_id, 
			'all_areas' => $this->areas, 
			'area_id' => $area_id));
	}
	
	 /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate($id) {
		$routMan = new Routes_Manager($id);
		$all_areas = $routMan->areas->output();
		$file = $routMan->file;
		$all_methods = ['post', 'get', 'any'];
		
        return view('backoffice.navigation.create_edit', compact('file','all_areas', 'all_methods'));
    }
	
	 /**
     * Save resource.
     *
     * @return Response
     */
    public function postCreate(NavCreateRequest $request, $id) {
		$routMan = new Routes_Manager($id);
		$line = array(
			'controller' => $request->controller,
			'name' => $request->path,
			'method' => $request->method
		);
		
		$routMan->addRoute($line, $request->area);
		
		$routMan->save();
    }
	
	/**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getEdit($id, $id2) {
		$routMan = new Routes_Manager($id);
		$route_id = $id;
		$all_areas = $routMan->areas->output();
		$file = $routMan->file;
		$all_methods = ['post', 'get', 'any'];
		$edit = $routMan->routes[$id2];
		
        return view('backoffice.navigation.create_edit', compact('file', 'all_areas', 'all_methods', 'edit', 'route_id'));
    }
	
	 /**
     * Save resource.
     *
     * @return Response
     */
    public function postEdit(NavCreateRequest $request, $id, $id2) {
		$routMan = new Routes_Manager($id);
		$all_methods = ['post', 'get', 'any'];
		$routMan->editRoute($id2, [
			'controller' => $request->controller,
			'method' => $all_methods[$request->method],
			'name' => $request->path,
			'Area' => $routMan->areas[$request->area]
		]);
		
		$routMan->save();
		
    }
	
	 /**
     * Remove the specified resource from storage.
     *
     * @param $user
     * @return Response
     */

    public function getDelete($id, $id2)
    {
        // Show the page
        return view('backoffice.navigation.delete', compact('id', 'id2'));
    }
	
	    /**
     * Remove the specified resource from storage.
     *
     * @param $user
     * @return Response
     */
    public function postDelete(DeleteRequest $request)
    {
        $routMan = new Routes_Manager($request->id);
        $routMan->delRoute($request->id2);
		$routMan->save();
    }
	
	public function data($route_id, $area_id){
		
		$routMan = new Routes_Manager($route_id);
		$navigation = $routMan->routes->output($area_id);
		
        return Datatables::of(Collection::make($navigation))
            ->add_column('actions', '<a href="{{{ URL::to(\'backoffice/navigation/edit/'.$route_id.'/\' . $id) }}}" class="btn btn-success btn-sm iframe" ><span class="glyphicon glyphicon-pencil"></span>  {{ Lang::get("admin/modal.edit") }}</a>'.
                    '<a href="{{{ URL::to(\'backoffice/navigation/delete/'.$route_id.'/\' . $id) }}}" class="btn btn-sm btn-danger iframe"><span class="glyphicon glyphicon-trash"></span> {{ Lang::get("admin/modal.delete") }}</a>
                ')
            ->remove_column('id')

            ->make();
		
    }
}