<?php namespace App\Http\Requests\Api;

use App\Http\Requests\Auth\UserRegisterRequest;

class UserRegisterRequestApi extends UserRegisterRequest {
	
	public function response(array $errors){
		return \Response::json(['success' => false,'errors' => $errors], 422);
	}
	
}
