<?php namespace App\Http\Requests\Api;

use App\Http\Requests\Auth\UserRecoverRequest;
use Response;

class UserRecoverRequestApi extends UserRecoverRequest {
	
	public function response(array $errors){
		return Response::json(['success' => false, 'errors' => $errors], 422);
	}
	
}
