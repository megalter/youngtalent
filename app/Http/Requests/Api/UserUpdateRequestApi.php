<?php namespace App\Http\Requests\Api;

use App\Http\Requests\Auth\UserUpdateRequest;
use Response;

class UserUpdateRequestApi extends UserUpdateRequest {
	
	public function response(array $errors){
		return Response::json(['success' => false, 'errors' => $errors], 422);
	}
	
}
