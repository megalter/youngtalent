<?php namespace App\Http\Requests\Api;

use App\Http\Requests\SetScoreRequest;

class SetScoreRequestApi extends SetScoreRequest {
	
	public function response(array $errors){
		return \Response::json(['success' => false,'errors' => $errors], 422);
	}
	
}
