<?php namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class UserRegisterRequest extends FormRequest {
	
	
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules(){
		
		return [
			'name' => 'required',
			'email' => 'required|email|unique:users',
			'password' => 'required|min:8|regex:((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20})|same:confirm_password',
			'birthdate' => 'required',
			'phone' => 'required',
			'university' => 'required_without_all:university,university_custom',
			'degree' => 'required_without_all:degree,degree_custom',
			'termos' => 'required'
		];
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize(){
		return true;
	}
	
	public function messages(){
		return [
			'user_code.exists' => 'Código inválido.',
			'password.regex' => 'A password tem de conter pelo menos uma letra maiúscula, uma letra minúscula e um algarismo.',
			'birthdate.required' => 'O campo \'Data de nascimento\' é de preenchimento obrigatório',
			'name.required' => 'O campo \'Nome\' é de preenchimento obrigatório',
			'university.required_without_all' => 'O campo \'Universidade\' é de preenchimento obrigatório',
			'degree.required_without_all' => 'O campo \'Curso\' é de preenchimento obrigatório',
			'phone.required' => 'O campo \'Telemóvel\' é de preenchimento obrigatório',
		];
	}
	
}
