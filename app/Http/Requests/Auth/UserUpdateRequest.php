<?php namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest {
	
	
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules(){

		$rules = [
			'first_name' => 'required',
			'last_name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|same:confirm_password',
			'confirm_password' => 'required|same:password',
		];
		$final_rules = [];
		foreach($rules as $key => $val){
			if(!empty($this->request->has($key)))
				$final_rules[$key] = $val;
		}
		
		return $final_rules;
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize(){
		return true;
	}
	
}
