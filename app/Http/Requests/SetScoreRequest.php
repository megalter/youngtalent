<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SetScoreRequest extends FormRequest {
	
	
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules(){
		
		return [
			'id_file' => 'required|exists:files,id',
			'score' => 'required',
		];
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize(){
		return true;
	}
	
	public function messages(){
		return [
			'id_file.exists' => 'Este ficheiro não consta da base de dados'
		];
	}
	
}
