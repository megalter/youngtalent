<?php namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class FileEditRequest extends FormRequest {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'name' => 'required|min:3',
			'path' => 'mimes:mp3,aac,ac3,mp4,avi,mov,wmv',
            'thumbnail' => 'mimes:jpg,jpeg,gif,png,bmp',
		];
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

}
