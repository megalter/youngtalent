<?php namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class FileAssociateRequest extends FormRequest {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'file_ids' => 'required',
			'id_category' => 'required|integer',
		];
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

}
