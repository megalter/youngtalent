<?php
Route::group(['prefix' => 'backoffice', 'middleware' => 'auth'], function() {
	#Patterns
    Route::pattern('id', '[0-9]+');
    Route::pattern('id2', '[0-9]+');
	#END-----------------------------------------------------------------------------
	
    #Admin Dashboard
	Route::get('dashboard', 'Admin\DashboardController@index'); 
    Route::get('', 'Admin\DashboardController@index');
	#END-----------------------------------------------------------------------------
	
    #Users
	Route::get('users/', 'Admin\UserController@index');
	Route::get('users/{id}', 'Admin\UserController@index');
    Route::get('users/create', 'Admin\UserController@getCreate');
    Route::post('users/create', 'Admin\UserController@postCreate');
    Route::get('users/edit/{id}', 'Admin\UserController@getEdit');
    Route::post('users/edit/{id}', 'Admin\UserController@postEdit');
	Route::get('users/avaliate/{id}', 'Admin\UserController@getAvaliate');
    Route::post('users/avaliate/{id}', 'Admin\UserController@postAvaliate');
    Route::get('users/delete/{id}', 'Admin\UserController@getDelete');
    Route::post('users/delete', 'Admin\UserController@postDelete');
	Route::get('users/video/{id}', 'Admin\UserController@getVideo');
    Route::get('users/data', 'Admin\UserController@data');
	Route::get('users/data/{id}', 'Admin\UserController@data');
	Route::get('users/export-csv/{name}', 'Admin\UserController@getExportCsv');
	Route::post('users/export-csv', 'Admin\UserController@postExportCsv');
	#END-----------------------------------------------------------------------------
	
	
});