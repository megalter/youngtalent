<?php

namespace App\Helpers;

define("...", ' ... '); // lets set a constant for recurring ellipsis

class Pagination {
	
	public $current_page, $total_pages, $boundaries, $arround, $template;
	public $output = "";
	
	public function __construct($args){
		foreach($args as $key => $argument)
			$this->$key = $argument;
	}
	
	//Outputs pagination
	public function show(){
		if($this->current_page == 0)
			return;
		$hasRight = ($this->current_page + $this->arround) < ($this->total_pages - $this->boundaries); //Check if right elipsis (...) is needed
		$hasLeft = ($this->current_page - $this->arround) > ($this->boundaries + 1);  //Check if left elipsis (...) is needed

		if($this->current_page != 1)
			$this->output .= str_replace(array('{{number}}', '{{symbol}}', '{{active}}'), array($this->current_page-1, '<', ''), $this->template);
		switch((int)$hasLeft*2 + (int)$hasRight){
			case 0: //no ellipsis at all case
				$this->build_str(1, $this->total_pages);
				break;
			case 1: //ellipsis to the right
				$this->build_str(1, $this->current_page + $this->arround);
				$this->output .= '<li> ... </li>';
				$this->build_str($this->total_pages - $this->boundaries + 1, $this->total_pages);
				break;
			case 2: //ellipsis to the left
				$this->build_str(1, $this->boundaries); 
				$this->output .= '<li> ... </li>';
				$this->build_str($this->current_page - $this->arround, $this->total_pages);
				break;
			case 3: //both
				$this->build_str(1, $this->boundaries);
				$this->output .= '<li> ... </li>';
				$this->build_str($this->current_page - $this->arround, $this->current_page + $this->arround);
				$this->output .= '<li> ... </li>';
				$this->build_str($this->total_pages - $this->boundaries + 1, $this->total_pages);
				break;
		}
		if($this->current_page < $this->total_pages)
			$this->output .= str_replace(array('{{number}}', '{{symbol}}'), array($this->current_page+1, '>'), $this->template);
		
		echo $this->output;
			
	}
	
	//Builds a html string within a set range based on template
	function build_str($start = NULL, $finish = NULL){
		foreach (range($start, $finish) as $number){
			$active = $this->current_page == $number ? 'active' : '';
			$this->output .= str_replace(array('{{active}}', '{{number}}', '{{symbol}}'), array($active, $number, $number), $this->template);
		}
	}
}
