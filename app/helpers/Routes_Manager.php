<?php

namespace App\Helpers;
use File;
use ArrayObject;

class Routes_Manager {
	
	public $lines;
	
	//All route files available to the project
	public $route_files;
	
	//Route file instatiated
	public $file;
	
	//Areas of the route file
	public $areas;
	
	//Specific routes from this route file
	public $routes;
	
	//Specific groups from this route file
	public $groups = array();
	
	//Specific patterns from this route file
	public $patterns = array();
	
	public $current_area;
	
	public $current_group;
	
	public function __construct($file){
		$this->areas = new AreasArrayObject([]);
		$this->routes = new RoutesArrayObject([]);
		if(is_numeric($file) && $file != 6){
			$temp_files = File::files('app/Http');
			foreach($temp_files as $key=>$path)
				if(str_contains($path, 'route'))
					$this->route_files[$key] = $path;
			$this->file = $this->route_files[$file];
		}
		elseif($file == 6)
			$this->file = 'app/Http/routes.php';
		else
			$this->file = $file;
		
		$this->lines = file($this->file, FILE_USE_INCLUDE_PATH);
		
		//Add default area
		$this->current_area = new Area('Nenhuma');
		$this->areas[0] = $this->current_area;
		
		//Find and store areas names and corresponding line ids (lines that start by #'Something')
		foreach($this->lines as $key=>$line){
			//Route groups
			if(strpos(strtolower($line), 'route::group') !== false){
				$this->current_group = new Group($line, $key);
				$this->groups[$key] = $this->current_group;
			}
			elseif(strpos($line, '});') !== false){
				if(!empty($this->current_group))
					$this->current_group->end = $key;
			}
			//Route areas
			elseif(strpos($line, '#') !== false && strpos($line, '#END') === false){
				$this->areas[$key] = new Area($line, $key);
				$this->current_area = $this->areas[$key];
			}
			elseif(strpos($line, '#END') !== false){
				$this->current_area->end = $key;
				$this->current_area = $this->areas[0];
			}
			//Route paterns
			elseif(strpos(strtolower($line), 'route::pattern') !== false){
				$this->patterns[$key] = new Pattern($line, $key);
			}
			//Routes
			elseif(strpos(strtolower($line), 'route::') !== false){
				$this->routes[$key] = new Route($key, $line, $this->current_area);
			}
			
		}
	}
	
	public function getRoute($line){
		if(is_numeric($line) && !empty($this->routes[$line]))
			return $this->routes[$line];
		elseif(!is_numeric($line)){
			foreach($this->routes as $route){
				if(strpos($route->line,$line) != false)
					return $route;
			}
		}
		else
			return false;
	}
	
	public function addRoute($line, $area = NULL){
		if(is_numeric($area)){
			$area = $this->areas[$area];
		}
		else
			$area = $this->getAreaByName($area);
		
		$key = $area->key == 0 ? $this->getInsertPoint() : $area->end;
		$this->changeIndexes(1, $key);
		
		$route = new Route($key, $line, $area);
		$this->routes[] = $route;
		if($area->key != 0)
			$this->lines[$key] = $route->__toString() . " \n";
		else
			$this->lines[$key] = "\n" . $route->__toString();
		return $route;
	}
	
	public function delRoute($line){
		if(is_numeric($line) && !empty($this->routes[$line])){
			if(!empty($this->routes[$line])){
				unset($this->routes[$line]);
				$this->changeIndexes(-1, $line);
			}
			
			return true;
		}
		elseif(!is_numeric($line)){
			foreach($this->routes as $route){
				if(strpos($route->line,$line) != false){
					foreach($args as $key=>$val){
						$route->$key = $val;
					}
					
					unset($this->routes[$key]);
					$this->changeIndexes(-1, $key);
					return true;
				}
			}
		}
		return false;
	}
	
	public function editRoute($line, $args = NULL){
		if(!empty($args)){
			if(is_numeric($line) && !empty($this->routes[$line])){
				$route = $this->routes[$line];
				foreach($args as $key=>$value){
					$route->$key = $value;
				}
				$this->lines[$line] = $route->__toString() . " \n";
				return true;
			}
				
			elseif(!is_numeric($line)){
				foreach($this->routes as $route){
					if(strpos($route->line,$line) != false){
						foreach($args as $key=>$val){
							$route->$key = $val;
						}
						$this->lines[$route->key] = $route->__toString();
						return true;
					}
				}
			}
			
		}
		
		return false;
		
	}
	
	private function getAreaByName($name){
		foreach($this->areas as $item){
			if(strpos(strtolower($item->name), strtolower($name)) !== false){
				return $item;
			}
		}
		return $this->areas[0];
	}
	
	private function getInsertPoint(){
		return end($this->areas)->end + 1;
	}
	
	private function changeIndexes($offset, $from = 0){
		if($offset > 0)
			for($i = count($this->lines)-1; $i >= $from; $i--)
				$this->lines[$i+$offset] = $this->lines[$i];
		else
			for($i = $from; $i < count($this->lines)-1; $i++)
				$this->lines[$i] = $this->lines[$i-$offset];
		foreach(['areas', 'routes', 'groups', 'patterns'] as $item){
			foreach($this->$item as $key => $obj){
				if($key >= $from){
					$obj->key = $obj->key + $offset;
					if(!empty($obj->end))
						$obj->end = $obj->end + $offset;
					//$this->$item[$key+$offset] = $obj;
					//unset($this->$item[$key]);
				}
				elseif(!empty($obj->end) && $obj->end >= $from){
					$obj->end = $obj->end + $offset;
					//$this->$item[$key+$offset] = $obj;
					//unset($this->$item[$key]);
				}
			}
		}
	}
	
	public function save(){
		file_put_contents($this->file, $this->lines);
	}
	
}

class Area{
	public $key;
	public $end;
	public $name;
	
	public function __construct($content, $key = 0){
		$this->name = trim(str_ireplace (['#', '-', ' ', '	', '\n', '\r'], '', $content));
		$this->key = $key;
	}
	
	public function __toString(){
		return $this->name;
	}
	
}

class Route{
	public static $methods = ['post', 'get', 'any'];
	
	public $key;
	
	public $line;
	
	public $method;
	
	public $name;
	
	public $controller;
	
	public $area;
	
	public function __construct($key, $line, $area){
		if(is_array($line)){
			foreach($line as $key => $value){
				if($key == 'method')
					$this->method = self::$methods[$value];
				else
					$this->$key = $value;
			}
		}
		else{
			$this->key = $key;
			$this->line = $line;
			$this->area = $area;

			$temp = explode("'", $line);
			foreach(self::$methods as $method){
				if(strpos(strtolower($line), $method))
					$this->method = $method;
			}
			$this->name = $temp[1];
			$this->controller = $temp[3];
		}
	}
	
	public function __toString(){
		$this->updateLine();
		return "	" . $this->line;
	}
	
	private function updateLine(){
		$this->line = "Route::" . $this->method . "('".$this->name."', '".$this->controller."');";
	}
}

class Pattern{
	
	public $line;
	
	public $name;
	
	public $value;
	
	public $key;
	
	public function __construct($line, $key){
		$temp = explode("'", $line);
		$this->line = $line;
		$this->name = $temp[1];
		$this->value = $temp[3];
		$this->key = $key;
	}
	
	public function __toString(){
		return $this->line;
	}
}

class Group{
	
	public $line;
	
	public $name;
	
	public $type;
	
	public $key;
	
	public $end;
	
	public function __construct($line, $key){
		$temp = explode("'", $line);
		$this->line = $line;
		$this->type = $temp[1];
		$this->name = $temp[3];
		$this->key = $key;
	}
	
	public function __toString(){
		return $this->line;
	}
}

class AreasArrayObject extends ArrayObject{
	public function __construct($array){
        parent::__construct($array,ArrayObject::ARRAY_AS_PROPS); 
    } 

	public function output(){
		$temp = [];
		foreach($this as $key => $object)
			$temp[$key] = $object->name;
		return $temp;
	}
}

class RoutesArrayObject extends ArrayObject{
	public function __construct($array){
		parent::__construct($array, ArrayObject::ARRAY_AS_PROPS);
	}
	
	public function output($id){
		$temp=[];
		foreach($this as $key => $object){
			if($id == 0 || $object->area->key == $id)
			$temp[] = [
				'id' => $key,
				'area' => $object->area->name,
				'method' => $object->method, 
				'name' => $object->name, 
				'controller' => $object->controller,
			];
		}
		return $temp;
	}
}