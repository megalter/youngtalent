<?php

namespace App\Helpers;

class TreeMaker {
	//Simple recursion tree while theres childs
	public static function printTree($categories, $options = 0, $name = NULL, $existingCategories = NULL){					
		foreach($categories as $category):?>
		<li data-category="<?=$category->id ?>">
			<?php if(!empty($existingCategories) && in_array($category->id, $existingCategories)) $checked = true; else $checked = false; ?>
			<?php echo self::printTemplate($options, $category, $name, isset($checked) && $checked==true);
			if($category->hasChild): ?>
			<ul>
				<?php self::printTree($category->children, $options, $name, $existingCategories); ?>
			</ul>
		<?php endif;?>
		</li>
	<?php endforeach;
	}
	
	public static function printTemplate($option, $category, $name, $checked = false){
		$checked = $checked ? 'checked="checked"' : '';
		switch($option){
			case 0:
				//Adding categories tree
				return '<input type="radio" name="parent_category" value="'.$category->id.'" data-name="'.$category->name.'" '.$checked.'>
				<a class="spanCatDisable">'.$category->name.'</a>';
				break;
			case 1:
				//departments, categories and sectors
				return '<a href="#" data-category="'.$category->id.'" class="category_item">'.$category->name.'</a>';
			case 2:
				//Doc categories
				return '<input type="checkbox" name="'.$name.'" value="'.$category->id.'" '.$checked.'>
					<a class="spanCatDisable">'.$category->name.'</a>';
		}
	}
}
