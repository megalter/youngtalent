<?php

namespace App\Helpers;

class ParentGetter {
	
	public $parents = array();
	
	//Recursive function to get parents
	public function getParents($object){					
		if($object->parent_id != 0){
			$class = get_class($object);
			$parentObj = $class::find($object->parent_id);
			if(!empty($parentObj)){
				$this->getParents($parentObj);
			}
		}
		array_push($this->parents, $object->name);
		return $this->parents;

	}
}
