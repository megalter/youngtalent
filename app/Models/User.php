<?php namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Auth\Authenticatable;
use Illuminate\Foundation\Auth\User as AuthenticatableExtension;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Hash;

class User extends AuthenticatableExtension implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword, SoftDeletes;
	
	protected $dates = ['deleted_at'];
	protected $primaryKey = 'id';
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name','email', 'password', 'confirmed'];
	protected $visible = ['id','name', 'email', 'confirmed', 'created_at', 'votes', 'views', 'average', 'video', 'birthdate', 'phone', 'address', 'university', 'university_custom', 'degree', 'degree_custom', 'degree_type'];
	
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];
	
	public function permissions(){
		return $this->belongsToMany('App\Models\Permission', 'users_permissions');
	}
	
	public function roles(){
		return $this->belongsToMany('App\Models\Role', 'assigned_roles');
	}
	
	public function countries(){
		return $this->belongsToMany('App\Models\Country', 'assigned_countries');
	}
	
	public function favourites(){
		return $this->belongsToMany('App\Models\File', 'users_favourites')->withPivot('category_id');
	}
	
	public function avaliation(){
		return $this->hasOne('App\Models\Avaliation');
	}
	
	public function videos(){
        return $this->hasMany('App\Models\Video');
    }
	
	public function is($roleName)
    {
        foreach ($this->roles()->get() as $role)
        {
            if ($role->name == $roleName)
            {
                return true;
            }
        }

        return false;
    }
	
	/**
	 * Fill the model with an array of attributes.
	 *
	 * @param  array  $attributes
	 * @return $this
	 *
	 * @throws \Illuminate\Database\Eloquent\MassAssignmentException
	 */
	public function fill(array $attributes){
		$totallyGuarded = $this->totallyGuarded();

		foreach ($this->fillableFromArray($attributes) as $key => $value){
			$key = $this->removeTableFromKey($key);

			// The developers may choose to place some attributes in the "fillable"
			// array, which means only those attributes may be set through mass
			// assignment to the model, and all others will just be ignored.
			if ($this->isFillable($key)){
				if($key == 'password')
					$this->setAttribute($key, Hash::make($value));
				else
					$this->setAttribute($key, $value);
			}
			elseif ($totallyGuarded){
				throw new MassAssignmentException($key);
			}
		}

		return $this;
	}
	
}
