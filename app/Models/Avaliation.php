<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Avaliation extends Model {
	
	use SoftDeletes;
	
	
	
	protected $dates = ['deleted_at'];
	
	protected $table = 'avaliation';
	
	protected $fillable = ['id', 'user_id', 
		'agility1', 'agility2', 'agility3', 
		'leadership1', 'leadership2', 'leadership3', 
		'connectivity1', 'connectivity2', 'connectivity3',
		'determination1', 'determination2', 'determination3',
		'pragmatism1', 'pragmatis2', 'pragmatism3',
		'notes', 'average'];
	protected $visible = ['id', 'user_id', 
		'agility1', 'agility2', 'agility3', 
		'leadership1', 'leadership2', 'leadership3', 
		'connectivity1', 'connectivity2', 'connectivity3',
		'determination1', 'determination2', 'determination3',
		'pragmatism1', 'pragmatis2', 'pragmatism3',
		'notes', 'average'];
	
}
