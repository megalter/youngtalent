<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PermissionRole extends Model {

	protected $fillable = [];
	
	protected $table = 'permission_role';

}
