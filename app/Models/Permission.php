<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model {

	
	protected $table = 'permissions';

	protected $fillable = ['display_name', 'id', 'front_end', 'value'];
	protected $visible = ['display_name', 'id', 'front_end', 'value'];
	
	
}
