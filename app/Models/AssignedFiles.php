<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssignedFiles extends Model {
	
	use SoftDeletes;
	
    protected $guarded = array();

    public static $rules = array();
	
	protected $table = 'assigned_files';
	
	
}