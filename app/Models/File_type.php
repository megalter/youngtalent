<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File_type extends Model {
	
		/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'file_types';
	
}
