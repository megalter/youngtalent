<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Video extends Model {
	
	use SoftDeletes;
	
	protected $dates = ['deleted_at'];
	
	protected $table = 'videos';
	
    protected $fillable = ['name','description', 'thumbnail','video_path', 'audio_path', 'file_type_id', 'votes_count', 'votes_score', 'votes_average', 'thumbnail_base64', 'file_duration', 'user_id'];
	protected $visible = ['id','name','description', 'thumbnail','video_path', 'audio_path', 'file_type_id', 'votes_count', 'votes_score', 'votes_average', 'thumbnail_base64', 'category_id', 'created_at', 'file_duration', 'cat_name', 'views', 'viewed', 'user_id'];
}
