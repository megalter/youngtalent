<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Entity;
use App\Helpers\ParentGetter;

class Filterable extends Model {
	
	public function getAssociatedElements($class, $entity, $user, $filters){
		$cl = explode("\\",$class);
		$cl = strtolower(end($cl));
		$parent_entity = Entity::find($entity->parent_id);
		$file_types = \DB::table('file_types')->lists('name')->toArray();
		
		$docs = self::query()
				->select($cl.'_documents.*', 'users.name as owner', 'file_types.icon as icon', $cl.'_categories.common')
				->join('users', 'users.id', '=', $cl.'_documents.owner_id')
				->rightJoin($cl.'_documents_categories', $cl.'_documents_categories.'.$cl.'_document_id', '=', $cl.'_documents.id')
				->leftJoin('file_types', 'file_types.id', '=', $cl.'_documents.file_type')
				->leftjoin($cl.'_categories', $cl.'_documents_categories.'.$cl.'_category_id', '=', $cl.'_categories.id')
				->leftjoin($cl.'_documents_tags', $cl.'_documents_tags.'.$cl.'_document_id', '=', $cl.'_documents.id')
				->join($cl.'_documents_share', $cl.'_documents_share.'.$cl.'_document_id', '=', $cl.'_documents.id')
				->where(function($query) use ($entity, $parent_entity, $cl){
					if(!empty($parent_entity))
						$query
							->where($cl.'_documents.entity_id', $entity->id)
							->orWhere(function($subQuery) use($parent_entity, $cl){
								$subQuery
								->where($cl.'_documents.entity_id', $parent_entity->id)
								->where($cl.'_categories.common', 1);
							});
					else
						$query->where($cl.'_documents.entity_id', $entity->id);
				})
				->where(function($query) use($user, $cl){
					$query
						->where($cl.'_documents_share.department_id', $user->department)
						->orWhere($cl.'_documents_share.department_category_id', $user->department_category)
						->orWhere($cl.'_documents_share.department_sector_id', $user->department_sector)
						->orWhere($cl.'_documents_share.share_all', 1)
						->orWhere($cl.'_documents.owner_id', $user->id);
				})
				->where(function($query) use ($filters, $cl){
					if(!empty($filters['left_category']))
						$query->where($cl.'_documents_categories.'.$cl.'_category_id', $filters['left_category']);
				})
				->where(function($query) use ($filters, $cl, $entity, $parent_entity, $file_types){
					foreach($filters as $key=>$value){
						if($key == 'common' && $value < 2)
							$query->where($cl.'_categories.common', $value);
						elseif($key == 'owner')
							$query->where('users.name', 'LIKE','%'.$value.'%');
						elseif(in_array($key, array('categories', 'commonCategories')) && !empty($value))
							$query->whereIn($cl.'_documents_categories.'.$cl.'_category_id', $value);
						elseif($key == 'new_tags'  && !empty($value))
							$query->whereIn($cl.'_documents_tags.'.$cl.'_tag_id', $filters['tagsIdsArray']);
						elseif(in_array($key, array('fromDate', 'toDate'))  && !empty($value))
							$query->where($cl.'_documents.created_at', $key == 'fromDate' ? '>=' : '<=', $value);
						elseif(in_array($key, array('title','description', 'author'))  && !empty($value))
							$query->where($cl.'_documents.'.$key, 'LIKE', '%'.$value.'%');
						elseif($key == 'simple' && $value != NULL){
							$tags = explode(' ', $value);
							$tagsIdsArray = \DB::table('dm_tags')
								->select('dm_tags.id')
								->whereIn('name', $tags)
								->where('entity_id', $entity->id)
								->where(function($query) use($parent_entity){
									if(!empty($parent_entity))
										$query->where('entity_id', $parent_entity->id);
								})
								->lists('id');
							$query->whereIn($cl.'_documents_tags.'.$cl.'_tag_id', $tagsIdsArray)
									->orWhere($cl.'_documents.title', 'LIKE', '%'.$value.'%')
									->orWhere($cl.'_documents.description', 'LIKE', '%'.$value.'%');
							if(in_array($cl, array('dm', 'cs', 'uag')) && in_array(substr($value, 2), $file_types)){
								$query->orWhere($cl.'_documents.file', 'LIKE', '%.'.substr($value, 2).'%');
							}
						}
					}	
				})
				->groupBy($cl.'_documents.id')
				->orderBy($cl.'_documents.'.$filters['order_field'], $filters['order_field'] == 'created_at' ? 'DESC' : 'ASC')->get();
				
				$this->countDocs = count($docs);
				
				$docs = $docs->slice(($filters['current_page']-1)*$filters['items_per_page'], $filters['items_per_page']);
		
				if(!empty($docs)){
					foreach($docs as $doc){
						//doc file icon
						if(!empty($doc->file))
							$doc->file_icon = \DB::table('file_types')->where('id', $doc->file_type)->first();
						else
							$doc->file_icon = \DB::table('file_types')->where('icon', 'link_mini.png')->first();

						if(empty($doc->file_icon))
							$doc->file_icon = \DB::table('file_types')->where('icon', 'default_mini.png')->first();

						$categories = self::find($doc->id)->categories;
						$final_categories = array();
						foreach($categories as $key=>$category){
							$parents = new ParentGetter;
							$temp = $parents->getParents($category);
							$final_categories[] = $temp;
							if($key == count($categories)-1)
								$doc->first_category = $temp;
						}
						$doc->final_categories = $final_categories;
					}
				}
				
		return $docs;
	}
	
}
