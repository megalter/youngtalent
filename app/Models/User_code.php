<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class User_code extends Model {
	
	use SoftDeletes;
	
	protected $dates = ['deleted_at'];
	
	public $created_at, $updated_at;
	
	protected $primaryKey = 'user_code';


	public function __contstruct(){
		$this->created_at = new DateTime;
		$this->updated_at = new DateTime;
	}
	
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user_codes';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['user_code', 'user_id', 'obs'];

	public $visible = ['user_code', 'user_id', 'obs'];

}
