<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model {
	
	use SoftDeletes;
	
	protected $dates = ['deleted_at'];
	
	protected $table = 'projects';
	
	protected $fillable = ['name', 'folder', 'thumbnail', 'client_id', 'platform_id', 'dev_path', 'prod_path', 'year'];
}
