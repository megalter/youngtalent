<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {
	
	public function getCssClassAttribute(){
		return 'expandable';
	}
	
	public function getCategoryIdAttribute(){
		return $this->id;
	}
	
	public function files(){
		return $this->belongsToMany('App\Models\File', 'category_id', 'id');
	}
	
	public function assigned_files(){
		return $this->belongsToMany('App\Models\File', 'assigned_files')->withPivot('available_at', 'unpublished_at');
	}
	
	public function dtable_files(){
		return $this->belongsToMany('App\Models\File', 'assigned_files')->wherePivot('deleted_at', NULL);
	}
	
    protected $fillable = ['name'];
	
		/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'categories';
}
