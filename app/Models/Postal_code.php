<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Postal_code extends Model {
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'postal_codes';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['postal_code', 'postal_code2', 'adress', 'district', 'municipality', 'parish'];

}
