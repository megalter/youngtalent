@extends('backoffice.layouts.modal') @section('content')

<video id="video" class="col-sm-6 col-sm-offset-3" controls>
	<source src="{{URL::to('appfiles/videos/'.$video->video_path)}}" type="video/webm">
</video>
<audio id="buzzer" src="{{URL::to("appfiles/videos/$video->audio_path")}}" type="audio/wav">Your browser does not support the &#60;audio&#62; element.</audio>

@stop @section('scripts')
<script>
	$("video").on('play',function(){
		//my code here
		$('#buzzer')[0].play();
	});
	$("video").on('pause',function(){
		//my code here
		$('#buzzer')[0].pause();
	});
</script>
@stop
