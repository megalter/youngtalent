@extends('backoffice.layouts.default') {{-- Web site Title --}}
@section('title') {{{ $title }}} :: @parent @stop {{-- Content --}}
@section('content')
<div class="page-header">
	<h2>{{$title}}</h2>
</div>
<div class="row">

	<div class="col-lg-3 col-md-6">
		<div class="panel panel-success">
			<div class="panel-heading">
				<div class="row">
					<div class="col-xs-3">
						<i class="glyphicon glyphicon-user fa-5x"></i>
					</div>
					<div class="col-xs-9 text-right">
						<div class="huge">{{$total_users}}</div>
						<div>{{ Lang::get("admin/admin.users") }}!</div>
					</div>
				</div>
			</div>
			<a href="{{URL::to('backoffice/users')}}">
				<div class="panel-footer">
					<span class="pull-left">{{ Lang::get("admin/admin.view_detail") }}</span>
					<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
					<div class="clearfix"></div>
				</div>
			</a>
		</div>
	</div>
</div>
@stop
