<!DOCTYPE html>

<html lang="en">

	<head id="Starter-Site">

		<meta charset="UTF-8">

		<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title>Administration :: Young Talent</title>

		<meta name="keywords" content="" />
		<meta name="author" content="" />
		<meta name="description" content="" />
		<meta name="google-site-verification" content="">
		<meta name="DC.title" content="Laravel 5 Starter Site">
		<meta name="DC.subject" content="">
		<meta name="DC.creator" content="">
		<meta name="viewport"
			  content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
		<link href="{{asset('assets/backoffice/css/bootstrap.min.css')}}" rel="stylesheet">
<!--		<link
			href="{{asset('assets/admin/css/plugins/metisMenu/metisMenu.min.css')}}"
			rel="stylesheet">-->
		<link href="{{asset('assets/backoffice/css/sb-admin-2.min.css')}}" rel="stylesheet">

		<link href="{{asset('assets/backoffice/css/plugins/dataTables/jquery.dataTables.css')}}" rel="stylesheet">
		<link href="{{asset('assets/backoffice/css/plugins/dataTables/dataTables.bootstrap.min.css')}}" rel="stylesheet">
		<link href="{{asset('assets/backoffice/css/plugins/colorbox.min.css')}}" rel="stylesheet">
		<link href="{{asset('assets/backoffice/css/fonts/font-awesome.min.css')}}" rel="stylesheet">
<!--		<link rel="stylesheet" href="{{asset('assets/admin/css/jquery-confirm.css')}}">-->
		<!--[if lt IE 9]>
					<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
					<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
				<![endif]-->
		<link rel="shortcut icon"
			  href="{{{ asset('assets/backoffice/ico/favicon.ico') }}}">

	</head>
	<body>
		<div id="wrapper">
			<nav class="navbar navbar-default navbar-static-top" role="navigation"
				 style="margin-bottom: 0">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
							data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="{!!URL::to('/backoffice/dashboard')!!}">Administração</a>
				</div>
				<ul class="nav navbar-top-links navbar-right">
					<li class="dropdown"><a class="btn account dropdown-toggle"
											data-toggle="dropdown" href="#">
							<div class="user">
								<span class="hello"> {{ Lang::get('admin/admin.welcome') }}!</span>
								<span class="name"> {{{ Auth::user()->first_name }}}</span>
							</div>
						</a>
						<ul class="dropdown-menu">
							<li><a href="{{{ URL::to('/') }}}"><i
										class="glyphicon glyphicon-home"></i> FRONT END</a></li>
							<li><a href="{{{ URL::to('sair') }}}"><i
										class="glyphicon glyphicon-off"></i> {{
								Lang::get('site/site.logout') }}</a></li>
						</ul></li>
				</ul>
				<div class="navbar-default sidebar" role="navigation">
					<div class="sidebar-nav navbar-collapse">
						@yield('menu')
						<ul class="nav" id="side-menu">
							<li class="sidebar-search">
								<div class="input-group custom-search-form">
									<input type="text" class="form-control" placeholder="Search...">
									<span class="input-group-btn">
										<button class="btn btn-default" type="button">
											<i class="fa fa-search"></i>
										</button>
									</span>
								</div>
							</li>
							<li><a href="{{URL::to('backoffice/dashboard')}}"
								   {{ (Request::is('backoffice/dashboard') || Request::is('backoffice') ? ' class=active' : '') }}> <i
										class="fa fa-dashboard fa-fw"></i><span class="hidden-sm text">
										Tabliê</span></a></li>
							<li><a href="{{URL::to('backoffice/users')}}"
								   {{ (Request::is('backoffice/users*') ? ' class=active' : '') }}> <i
										class="glyphicon glyphicon-user"></i><span
										class="hidden-sm text"> Utilizadores</span></a></li>
						</ul>
					</div>

					<br/>
					@if(Request::segment(2) == 'files')
					(Arraste para aqui os itens)
					@if(isset($channel))
					<i id="unassociate" class="glyphicon glyphicon-transfer fa-5x admin-trash"><h5>Desassociar</h5></i>
					<br/>
					<i id="garbage" class="glyphicon glyphicon-trash fa-5x admin-trash"><h5>Apagar</h5></i>
					@else
					<i id="garbage" class="glyphicon glyphicon-trash fa-5x admin-trash"><h5>Apagar</h5></i>
					@endif
					@endif
				</div>
			</nav>
		</header>
		<div id="page-wrapper">
			<div class="row">@yield('content')</div>
		</div>
	</div>
	<div class="clearfix"></div>
	<footer>
		<p>
			<span style="text-align: left; float: left">&copy; 2015 <a href="https://pt.linkedin.com/in/miguel-poço-2a954852" target="_blank">Miguel Poço</a></span> <span class="hidden-phone"
																																			style="text-align: right; float: right">Powered by: <a
					href="http://laravel.com/" alt="Laravel 5.2">Laravel 5.2</a></span>
		</p>

	</footer>
	<!--[if !IE]>-->
	<script src="{{asset('assets/backoffice/js/jquery-2.1.1.min.js')}}"></script>
	<script src="{{asset('assets/backoffice/js/jquery-ui.1.11.2.min.js')}}"></script>
	<!--<![endif]-->
	<!--[if IE]>
        <script src="{{asset('assets/admin/js/jquery-1.11.1.min.js')}}"></script>
        <![endif]-->
	<script src="{{asset('assets/backoffice/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('assets/backoffice/js/plugins/metisMenu.min.js')}}"></script>
	<script src="{{asset('assets/backoffice/js/sb-admin-2.min.js')}}"></script>
	<!-- DataTables -->
	<script src="{{asset('assets/backoffice/js/plugins/dataTables/jquery.dataTables.min.js')}}"></script>
	<script src="{{asset('assets/backoffice/js/plugins/dataTables/dataTables.bootstrap.min.js')}}"></script>
	<script src="{{asset('assets/backoffice/js/plugins/dataTables/bootstrap-dataTables-paging.min.js')}}"></script>
	<script src="{{asset('assets/backoffice/js/plugins/dataTables/datatables.fnReloadAjax.min.js')}}"></script>
	
	<script src="{{asset('assets/backoffice/js/plugins/jquery.colorbox.min.js')}}"></script>
	<script src="{{asset('assets/backoffice/js/jquery-migrate-1.2.1.min.js')}}"></script>

	@yield('scripts')
</body>
</html>
