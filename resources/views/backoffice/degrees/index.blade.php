@extends('backoffice.layouts.default') {{-- Web site Title --}}
@section('title') {{{ Lang::get("admin/users.users") }}} :: @parent
@stop {{-- Content --}} @section('content')
<div class="page-header">
	<h3>
		{{{ Lang::get("admin/users.users") }}}
		<div class="pull-right">
			<div class="pull-right">
				<a href="{{{ URL::to('backoffice/users/create/') }}}"
					class="btn btn-sm  btn-primary iframe"><span
					class="glyphicon glyphicon-plus-sign"></span> {{
					Lang::get("admin/modal.new") }}</a>
			</div>
		</div>
	</h3>
</div>

<table id="table" class="table table-striped table-hover">
	<thead>
		<tr>
			<th>{{{ Lang::get("admin/users.name") }}}</th>
			<th>{{{ Lang::get("admin/users.email") }}}</th>
			<th>{{{ Lang::get("admin/users.video") }}}</th>
			<th>{{{ Lang::get("admin/users.average") }}}</th>
			<th>{{{ Lang::get("admin/admin.action") }}}</th>
		</tr>
	</thead>
	<tbody></tbody>
</table>
@stop {{-- Scripts --}} @section('scripts')
<script type="text/javascript">
	var Table;
	$(document).ready(function() {
		Table = $('#table').dataTable({
			"dom" : "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
			"paginationType" : "bootstrap",
			"columns" : [
				null,
				null,
				{className: 'dt-body-center', searchable: false, orderable: false},
				{name: 'avaliation.average'},
				{searchable: false, orderable: false},
			],
			"processing" : true,
			"serverSide" : true,
			"ajax" : "{{ URL::to('backoffice/users/data/') }}",
			"fnDrawCallback" : function(settings) {
				$(".iframe").colorbox({
					iframe : true,
					width : "80%",
					height : "80%",
					onClosed : function() {
						window.location.reload();
					}
				});
			}
		});
	});
</script>
@stop
