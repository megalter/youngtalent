@extends('backoffice.layouts.modal') @section('content')
<ul class="nav nav-tabs">
	<li class="active"><a href="#tab-general" data-toggle="tab">{{{
			Lang::get('admin/modal.general') }}}</a></li>
</ul>
<form class="form-horizontal" method="post"
	action="{{ URL::to('backoffice/users/avaliate/'.$id) }}"
	autocomplete="off">
	<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
	<div class="tab-content">
		<div class="tab-pane active" id="tab-general">
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label class="col-md-2 control-label" for="agility">
							<h4>{{Lang::get('admin/users.agility') }}</h4>
						</label>
						<div class="col-md-10">
							<div class="row">
								<div class="col-sm-9">
									<label class="control-label" for="agility1">
										Reage com rapidez e criatividade à mudança
									</label>
								</div>
								<div class="col-sm-3">
									<select class="form-control stars" name="agility1" id="agility1">
										@foreach([1,2,3,4] as $item)
										<option value='{{$item}}' {{isset($avaliation) && $item == $avaliation->agility1 ? 'selected' : ''}}>{{$item}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-9">
									<label class="control-label" for="agility2">
										Adapta-se com eficácia a contextos incertos e complexos
									</label>
								</div>
								<div class="col-sm-3">
									<select class="form-control stars" name="agility2" id="agility2">
										@foreach([1,2,3,4] as $item)
										<option value='{{$item}}' {{isset($avaliation) && $item == $avaliation->agility2 ? 'selected' : ''}}>{{$item}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-9">
									<label class="control-label" for="agility3">
										Demonstra flexibilidade e polivalência no desempenho de diferentes tarefas e papéis
									</label>
								</div>
								<div class="col-sm-3">
									<select class="form-control stars" name="agility3" id="agility3">
										@foreach([1,2,3,4] as $item)
										<option value='{{$item}}' {{isset($avaliation) && $item == $avaliation->agility3 ? 'selected' : ''}}>{{$item}}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label class="col-md-2 control-label" for="leadership">
							<h4>{{Lang::get('admin/users.leadership') }}</h4>
						</label>
						<div class="col-md-10">
							<div class="row">
								<div class="col-sm-9">
									<label class="control-label" for="leadership1">
										Mostra compromisso e motivação para com o seu propósito
									</label>
								</div>
								<div class="col-sm-3">
									<select class="form-control stars" name="leadership1" id="leadership1">
										@foreach([1,2,3,4] as $item)
										<option value='{{$item}}' {{isset($avaliation) && $item == $avaliation->leadership1 ? 'selected' : ''}}>{{$item}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-9">
									<label class="control-label" for="leadership2">
										Mobiliza outros para atingir metas coletivas
									</label>
								</div>
								<div class="col-sm-3">
									<select class="form-control stars" name="leadership2" id="leadership2">
										@foreach([1,2,3,4] as $item)
										<option value='{{$item}}' {{isset($avaliation) && $item == $avaliation->leadership2 ? 'selected' : ''}}>{{$item}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-9">
									<label class="control-label" for="leadership3">
										Comunica de forma inspiradora e persuasiva combinando as emoções e a lógica
									</label>
								</div>
								<div class="col-sm-3">
									<select class="form-control stars" name="leadership3" id="leadership3">
										@foreach([1,2,3,4] as $item)
										<option value='{{$item}}' {{isset($avaliation) && $item == $avaliation->leadership3 ? 'selected' : ''}}>{{$item}}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label class="col-md-2 control-label" for="connectivity">
							<h4>{{Lang::get('admin/users.connectivity') }}</h4>
						</label>
						<div class="col-md-10">
							<div class="row">
								<div class="col-sm-9">
									<label class="control-label" for="connectivity1">
										Cria e desenvolve com facilidade relações com variados perfis individuais
									</label>
								</div>
								<div class="col-sm-3">
									<select class="form-control stars" name="connectivity1" id="connectivity1">
										@foreach([1,2,3,4] as $item)
										<option value='{{$item}}' {{isset($avaliation) && $item == $avaliation->connectivity1 ? 'selected' : ''}}>{{$item}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-9">
									<label class="control-label" for="connectivity2">
										Está atento e ligado ao mundo e aos eventos que o rodeiam, envolvendo-se em causas que considera relevantes
									</label>
								</div>
								<div class="col-sm-3">
									<select class="form-control stars" name="connectivity2" id="connectivity2">
										@foreach([1,2,3,4] as $item)
										<option value='{{$item}}' {{isset($avaliation) && $item == $avaliation->connectivity2 ? 'selected' : ''}}>{{$item}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-9">
									<label class="control-label" for="connectivity3">
										Revela curiosidade em aprender novas abordagens, procurando ativamente novas experiências, ideias e conhecimentos
									</label>
								</div>
								<div class="col-sm-3">
									<select class="form-control stars" name="connectivity3" id="connectivity3">
										@foreach([1,2,3,4] as $item)
										<option value='{{$item}}' {{isset($avaliation) && $item == $avaliation->connectivity3 ? 'selected' : ''}}>{{$item}}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label class="col-md-2 control-label" for="determination">
							<h4>{{Lang::get('admin/users.determination') }}</h4>
						</label>
						<div class="col-md-10">
							<div class="row">
								<div class="col-sm-9">
									<label class="control-label" for="determination1">
										É apaixonado por desafios, demonstrando energia e entusiasmo para ultrapassar obstáculos e insucessos
									</label>
								</div>
								<div class="col-sm-3">
									<select class="form-control stars" name="determination1" id="determination1">
										@foreach([1,2,3,4] as $item)
										<option value='{{$item}}' {{isset($avaliation) && $item == $avaliation->determination1 ? 'selected' : ''}}>{{$item}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-9">
									<label class="control-label" for="determination2">
										Revela coragem e vontade de assumir riscos
									</label>
								</div>
								<div class="col-sm-3">
									<select class="form-control stars" name="determination2" id="determination2">
										@foreach([1,2,3,4] as $item)
										<option value='{{$item}}' {{isset($avaliation) && $item == $avaliation->determination2 ? 'selected' : ''}}>{{$item}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-9">
									<label class="control-label" for="determination3">
										Procura um propósito para as suas ações e revela sentido de missão
									</label>
								</div>
								<div class="col-sm-3">
									<select class="form-control stars" name="determination3" id="determination3">
										@foreach([1,2,3,4] as $item)
										<option value='{{$item}}' {{isset($avaliation) && $item == $avaliation->determination3 ? 'selected' : ''}}>{{$item}}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label class="col-md-2 control-label" for="pragmatism">
							<h4>{{Lang::get('admin/users.pragmatism') }}</h4>
						</label>
						<div class="col-md-10">
							<div class="row">
								<div class="col-sm-9">
									<label class="control-label" for="pragmatism1">
										Resolve problemas analisando a informação e gerando novas perspetivas
									</label>
								</div>
								<div class="col-sm-3">
									<select class="form-control stars" name="pragmatism1" id="pragmatism1">
										@foreach([1,2,3,4] as $item)
										<option value='{{$item}}' {{isset($avaliation) && $item == $avaliation->pragmatism1 ? 'selected' : ''}}>{{$item}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-9">
									<label class="control-label" for="pragmatism2">
										Foca-se na procura de soluções e nos resultados a atingir, com criação de valor efetivo
									</label>
								</div>
								<div class="col-sm-3">
									<select class="form-control stars" name="pragmatism2" id="pragmatism2">
										@foreach([1,2,3,4] as $item)
										<option value='{{$item}}' {{isset($avaliation) && $item == $avaliation->pragmatism2 ? 'selected' : ''}}>{{$item}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-9">
									<label class="control-label" for="pragmatism3">
										Tem uma atitude proativa, procurando antecipar dificuldades e identificar oportunidades
									</label>
								</div>
								<div class="col-sm-3">
									<select class="form-control stars" name="pragmatism3" id="pragmatism3">
										@foreach([1,2,3,4] as $item)
										<option value='{{$item}}' {{isset($avaliation) && $item == $avaliation->pragmatism3 ? 'selected' : ''}}>{{$item}}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-md-12">
				<br>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label class="col-md-2 control-label" for="notes">{{
						Lang::get('admin/users.notes') }}</label>
					<div class="col-md-10">
						<textarea class="form-control" tabindex="5"
							placeholder="{{ Lang::get('admin/users.notes') }}" type="text"
							name="notes" id="notes">
						{{$avaliation->notes or ''}}
						</textarea>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<button type="reset" class="btn btn-sm btn-warning close_popup">
				<span class="glyphicon glyphicon-ban-circle"></span> {{
				Lang::get("admin/modal.cancel") }}
			</button>
			<button type="reset" class="btn btn-sm btn-default">
				<span class="glyphicon glyphicon-remove-circle"></span> {{
				Lang::get("admin/modal.reset") }}
			</button>
			<button type="submit" class="btn btn-sm btn-success">
				<span class="glyphicon glyphicon-ok-circle"></span> 
				        {{ Lang::get("admin/modal.avaliate") }}
			</button>
		</div>
	</div>
</form>
@stop @section('scripts')
<script src="{{  asset('assets/backoffice/js/plugins/jquery.barrating.min.js') }}"></script>
<script type="text/javascript">
   $(function() {
      $('.stars').barrating({
        theme: 'fontawesome-stars'
      });
   });
</script>
@stop
