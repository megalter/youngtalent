@extends('backoffice.layouts.modal') @section('content')
<ul class="nav nav-tabs">
	<li class="active"><a href="#tab-general" data-toggle="tab">{{{
			Lang::get('admin/modal.general') }}}</a></li>
</ul>
<form class="form-horizontal" method="post"
	action="@if (isset($edit)){{ URL::to('backoffice/users/edit/' . $edit->id) }}@endif"
	autocomplete="off">
	<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
	<div class="tab-content">
		<div class="tab-pane active" id="tab-general">
			<div class="col-md-12">
				<div class="form-group">
					<label class="col-md-2 control-label" for="name">{{
						Lang::get('admin/users.name') }}</label>
					<div class="col-md-10">
						<input class="form-control" tabindex="1"
							placeholder="{{ Lang::get('admin/users.name') }}" type="text"
							name="name" id="name"
							value="{{{ Input::old('name', isset($edit) ? $edit->name : null) }}}">
					</div>
				</div>
			</div>
			@if(!isset($edit))
			<div class="col-md-12">
				<div
					class="form-group {{{ $errors->has('email') ? 'has-error' : '' }}}">
					<label class="col-md-2 control-label" for="email">{{
						Lang::get('admin/users.email') }}</label>
					<div class="col-md-10">
						<input class="form-control" type="email" tabindex="4"
							placeholder="{{ Lang::get('admin/users.email') }}" name="email"
							id="email"
							value="{{{ Input::old('email', isset($edit) ? $edit->email : null) }}}" />
						{!! $errors->first('email', '<label class="control-label"
							for="email">:message</label>')!!}
					</div>
				</div>
			</div>
			@endif
			<div class="col-md-12">
				<div
					class="form-group {{{ $errors->has('password') ? 'has-error' : '' }}}">
					<label class="col-md-2 control-label" for="password">{{
						Lang::get('admin/users.password') }}</label>
					<div class="col-md-10">
						<input class="form-control" tabindex="5"
							placeholder="{{ Lang::get('admin/users.password') }}"
							type="password" name="password" id="password" value="" />
						{!!$errors->first('password', '<label class="control-label"
							for="password">:message</label>')!!}
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div
					class="form-group {{{ $errors->has('password_confirmation') ? 'has-error' : '' }}}">
					<label class="col-md-2 control-label" for="password_confirmation">{{
						Lang::get('admin/users.password_confirmation') }}</label>
					<div class="col-md-10">
						<input class="form-control" type="password" tabindex="6"
							placeholder="{{ Lang::get('admin/users.password_confirmation') }}"
							name="password_confirmation" id="password_confirmation" value="" />
						{!!$errors->first('password_confirmation', '<label
							class="control-label" for="password_confirmation">:message</label>')!!}
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label class="col-md-2 control-label" for="birthdate">Data de nascimento</label>
					<div class="col-md-10">
						<input class="form-control" tabindex="1"
							placeholder="Data de nascimento" type="text"
							name="birthdate" id="birthdate"
							value="{{{ Input::old('birthdate', isset($edit) ? $edit->birthdate : null) }}}">
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label class="col-md-2 control-label" for="phone">Telemóvel</label>
					<div class="col-md-10">
						<input class="form-control" tabindex="1"
							placeholder="Telemóvel" type="text"
							name="phone" id="phone"
							value="{{{ Input::old('phone', isset($edit) ? $edit->phone : null) }}}">
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label class="col-md-2 control-label" for="address">Morada</label>
					<div class="col-md-10">
						<input class="form-control" tabindex="1"
							placeholder="Morada" type="text"
							name="address" id="address"
							value="{{{ Input::old('address', isset($edit) ? $edit->address : null) }}}">
					</div>
				</div>
			</div>
			@if(isset($edit) && !empty($edit->university))
			<div class="col-md-12">
				<div class="form-group">
					<label class="col-md-2 control-label" for="university">Universidade</label>
					<div class="col-md-6">
						<select name="university" id="university" class="form-control" style="width: 100%;">
							@foreach ($universities as $item)
							<option value="{{{ $item->id }}}" @if(isset($edit) && !empty($edit->university) && $edit->university == $item->id) selected="selected" @endif>
								{{{ $item->name }}}
							</option> 
							@endforeach
						</select>
					</div>
				</div>
			</div>
			@endif
			@if(isset($edit) && !empty($edit->university_custom))
			<div class="col-md-12">
				<div class="form-group">
					<label class="col-md-2 control-label" for="university_custom">Universidade</label>
					<div class="col-md-6">
						<input class="form-control" tabindex="1"
							placeholder="Curso" type="text"
							name="university_custom" id="university_custom"
							value="{{{ Input::old('university_custom', isset($edit) ? $edit->university_custom : null) }}}">
					</div>
				</div>
			</div>
			@endif
			@if(isset($edit) && !empty($edit->degree))
			<div class="col-md-12">
				<div class="form-group">
					<label class="col-md-2 control-label" for="degree">Curso</label>
					<div class="col-md-6">
						<select name="degree" id="degree" class="form-control" style="width: 100%;">
							@foreach ($degrees as $item)
							<option value="{{{ $item->id }}}" @if(isset($edit) && !empty($edit->degree) && $edit->degree == $item->id) selected="selected" @endif>
								{{{ $item->name }}}
							</option> 
							@endforeach
						</select>
					</div>
				</div>
			</div>
			@endif
			@if(isset($edit) && !empty($edit->degree_custom))
			<div class="col-md-12">
				<div class="form-group">
					<label class="col-md-2 control-label" for="degree_custom">Curso</label>
					<div class="col-md-6">
						<input class="form-control" tabindex="1"
							placeholder="Curso" type="text"
							name="degree_custom" id="degree_custom"
							value="{{{ Input::old('degree_custom', isset($edit) ? $edit->degree_custom : null) }}}">
					</div>
				</div>
			</div>
			@endif
			<div class="col-md-12">
				<div class="form-">
					<label class="col-md-2 control-label" for="confirm">{{
						Lang::get('admin/users.activate_user') }}</label>
					<div class="col-md-6">
						<select class="form-control" name="confirmed" id="confirmed">
							<option value="1" {{{ ((isset($edit) && $edit->confirmed == 1)? '
								selected="selected"' : '') }}}>{{{ Lang::get('admin/users.yes')
								}}}</option>
							<option value="0" {{{ ((isset($edit) && $edit->confirmed == 0) ?
								' selected="selected"' : '') }}}>{{{ Lang::get('admin/users.no')
								}}}</option>
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<br>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label class="col-md-2 control-label" for="roles">{{
						Lang::get('admin/users.roles') }}</label>
					<div class="col-md-6">
						<select name="roles[]" id="roles" multiple style="width: 100%;">
							@foreach ($roles as $role)
							<option value="{{{ $role->id }}}" {{{ ( array_search($role->id,
								$selectedRoles) !== false && array_search($role->id,
								$selectedRoles) >= 0 ? ' selected="selected"' : '') }}}>{{{
								$role->name }}}</option> @endforeach
						</select> <span class="help-block"> {{
							Lang::get('admin/users.roles_info') }} </span>
					</div>
				</div>
			</div>
			<!--
			<div class="col-md-12">
				<div class="form-group">
					<label class="col-md-2 control-label" for="read_permissions">Permissões isoladas de leitura</label>
					<div class="col-md-6">
						<select name="readPermissions[]" id="read_permissions" multiple style="width: 100%;">
							@foreach ($permissions as $permission)
							<option value="{{{ $permission->id }}}" {{{ ( array_search($permission->id,
								$readPermissions) !== false && array_search($permission->id,
								$readPermissions) >= 0 ? ' selected="selected"' : '') }}}>{{{
								$permission->name }}}</option> @endforeach
						</select> <span class="help-block">Associar permissões isoladas a utilizadores.</span>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label class="col-md-2 control-label" for="write_permissions">Permissões isoladas de escrita</label>
					<div class="col-md-6">
						<select name="writePermissions[]" id="write_permissions" multiple style="width: 100%;">
							@foreach ($permissions as $permission)
							<option value="{{{ $permission->id }}}" {{{ ( array_search($permission->id,
								$writePermissions) !== false && array_search($permission->id,
								$writePermissions) >= 0 ? ' selected="selected"' : '') }}}>{{{
								$permission->name }}}</option> @endforeach
						</select> <span class="help-block">Associar permissões isoladas a utilizadores.</span>
					</div>
				</div>
			</div>
			-->
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<button type="reset" class="btn btn-sm btn-warning close_popup">
				<span class="glyphicon glyphicon-ban-circle"></span> {{
				Lang::get("admin/modal.cancel") }}
			</button>
			<button type="reset" class="btn btn-sm btn-default">
				<span class="glyphicon glyphicon-remove-circle"></span> {{
				Lang::get("admin/modal.reset") }}
			</button>
			<button type="submit" class="btn btn-sm btn-success">
				<span class="glyphicon glyphicon-ok-circle"></span> 
				    @if	(isset($edit))
				        {{ Lang::get("admin/modal.edit") }}
				    @else
				        {{Lang::get("admin/modal.create") }} 
				    @endif
			</button>
		</div>
	</div>
</form>
@stop @section('scripts')
<script type="text/javascript">
	$(function() {
		$("#roles").select2()
	});
	$(function() {
		$("#read_permissions").select2()
	});
	$(function() {
		$("#write_permissions").select2()
	});
</script>
@stop
