@extends('backoffice.layouts.default') {{-- Web site Title --}}
@section('title') {{{ Lang::get("admin/users.users") }}} :: @parent
@stop {{-- Content --}} @section('content')
<div class="page-header">
	<h3>
		{{{ Lang::get("admin/users.users") }}}
		<div class="pull-right">
			<div class="pull-right">
				<a href="{{{ URL::to('backoffice/users/create/') }}}"
					class="btn btn-sm  btn-primary iframe"><span
					class="glyphicon glyphicon-plus-sign"></span> {{
					Lang::get("admin/modal.new") }}</a>
				
				<a id="csv" href="#"
					class="btn btn-sm  btn-default"><span
					class="glyphicon glyphicon-export"></span> Exportar selecionados (CSV)</a>
				<form id="download-csv" method="GET" action="{{URL::to('backoffice/users/export-csv/')}}"></form>
			</div>
		</div>
	</h3>
</div>

<table id="table" class="table table-striped table-hover">
	<thead>
		<tr>
			<th></th>
			<th>{{{ Lang::get("admin/users.name") }}}</th>
			<th>{{{ Lang::get("admin/users.email") }}}</th>
			<th>{{{ Lang::get("admin/users.video") }}}</th>
			<th>{{{ Lang::get("admin/users.average") }}}</th>
			<th>Data registo</th>
			<th>{{{ Lang::get("admin/admin.action") }}}</th>
		</tr>
	</thead>
	<tbody></tbody>
</table>
@stop {{-- Scripts --}} @section('scripts')
<script type="text/javascript">
	var Table;
	$(document).ready(function() {
		Table = $('#table').dataTable({
			"dom" : "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
			"paginationType" : "bootstrap",
			"columns" : [
				null,
				null,
				null,
				{className: 'dt-body-center', searchable: true, orderable: false},
				{name: 'avaliation.average'},
				{name: 'users.created_at'},
				{searchable: false, orderable: false},
			],
			"processing" : true,
			"serverSide" : true,
			"ajax" : "{{ URL::to('backoffice/users/data/') }}",
			"fnDrawCallback" : function(settings) {
				$('.cbox').click(function(){
					
					if($(".cbox:checked").length > 0)
						$("#csv").attr('onClick', 'exportCsv()').addClass('btn-success').removeClass('btn-default');
					else
						$("#csv").removeAttr('onClick').addClass('btn-default').removeClass('btn-success');
						
				});
				$(".iframe").colorbox({
					iframe : true,
					width : "80%",
					height : "80%",
					onClosed : function() {
//						window.location.reload();
					}
				});
			}
		});
	});
	
	function exportCsv(){
		var values = $('.cbox:checked').map(function () {
			return this.value;
		}).get();
		$.ajax({
			headers: { 'csrftoken' : '{{ csrf_token() }}' },
			url: '{{URL::to("backoffice/users/export-csv")}}',
			type: 'post',
			dataType: 'json',
			success: function (response) {
				$('#download-csv').attr('action', "{{URL::to("backoffice/users/export-csv")}}/"+response.name).submit();
				
			},
			data: {id: values}
		});
		
	}
	
</script>
@stop
