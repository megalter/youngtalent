<!DOCTYPE html>
<html lang="pt-PT">
<head>
<meta charset="utf-8">
</head>
<body style="font-family: Calibri;">
	<h2>Obrigado por te registares e bem vindo à YTC! Estamos ansiosos por ouvir a tua história!</h2>
	<p>Para te facilitar a vida, enviamos-te os teus dados de acesso:<br>
		Email : {{$user->email}}<br>
		Password: {{$password}}
	</p>
	<p>
		Tens alguma questão sobre o processo? Dúvidas sobre a conferência?<br>
		Estamos por aqui: <a href="mailto:hello@ytconference.com">hello@ytconference.com</a><br>
		<br>
		A YTC Team
	</p>
</body>
</html>
