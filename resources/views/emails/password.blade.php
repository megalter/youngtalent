<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8">
</head>
<body>
	<h2>Redefinir password</h2>

	<div>
		Para redefinires a tua password, completa este formulário: {{ url('password/reset',
		[$token]) }}.<br /> Este link expirará dentro de {{
		config('auth.reminder.expire', 60) }} minutos.
	</div>
</body>
</html>
