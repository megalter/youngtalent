<!DOCTYPE html>
<html lang="pt-PT">
<head>
<meta charset="utf-8">
</head>
<body style="font-family: Calibri;">
	<h2>Yeah! Está feito, a tua história já cá canta!</h2>
	<p>
		As centenas de milhões de candidaturas à YTC serão analisadas durante o mês de Março.<br>
		Podes contar com o nosso feedback à tua história no início do mês de Abril!
	</p>
	<p>
		Até lá, tens alguma questão sobre a tua candidatura? Dúvidas sobre a conferência?<br>
		Apetece-te conversar e trocar umas ideias sobre isto, aquilo ou aqueloutro?<br>
		Estamos por aqui: <a href="mailto:hello@ytconference.com">hello@ytconference.com</a>
	</p>
	
</body>
</html>
