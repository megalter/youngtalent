@extends('site.layouts.default') {{-- Web site Title --}}
@section('title') {{{ Lang::get("admin/users.users") }}} :: @parent
@stop {{-- Content --}} @section('content')

<header>
	<div class="header-content" style="margin-top: 20px;">
		<div class="header-content-inner">
			<div class="form-group col-xs-12 col-sm-offset-2 col-sm-8 floating-label-form-group controls clearfix text-center">
				Vê este vídeo com atenção para perceberes como deves contar-nos a tua história.
				<br><br>
			</div>
			<div class="form-group floating-label-form-group controls clearfix">
				
				<iframe id="video" width="560" height="315" src="https://www.youtube.com/embed/dWmI4jgQajc" frameborder="0" allowfullscreen></iframe>
				<br/>
				<br/>
			</div>
			<div class="form-group col-xs-10 col-xs-offset-1 col-sm-offset-4 col-sm-4 floating-label-form-group clearfix text-center">
				<a href="{{URL::to('record')}}" class="form-control register" style="text-align: center; background-color: #199EDF; color: #FFF;">
					GRAVAR A MINHA HISTÓRIA
				</a>
				
			</div>
			<div class="form-group col-sm-offset-3 col-sm-6 floating-label-form-group text-center index_msg">
				<h6>Para garantir uma experiência bug free, usa o Chrome.</h6>
			</div>
			<div class="form-group col-sm-offset-3 col-sm-6 floating-label-form-group text-center index_msg">
				
				Tens alguma dúvida? Deixa-nos ajudar-te.<br/> Escreve-nos para <a href="mailto:hello@ytconference.com" style="color: #000; font-weight: bold;">hello@ytconference.com</a>
				
			</div>
		</div>
	</div>

</header>
@stop 
{{-- Scripts --}} 
@section('scripts')

@stop
