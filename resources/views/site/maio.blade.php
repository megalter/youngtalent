@extends('site.layouts.default') {{-- Web site Title --}}
@section('title') {{{ Lang::get("admin/users.users") }}} :: @parent
@stop {{-- Content --}} @section('content')

<div id="success" class="header-content-inner text-center" style="margin-top:100px;">
	<h1>Yeah! Está feito, a tua história já cá canta!</h1>
	<p>
		As centenas de milhões de candidaturas à YTC serão analisadas durante o mês de Março.<br/>
		Podes contar com o nosso feedback à tua história no início do mês de Abril!
	</p>
	<p>
		Até lá, tens alguma questão sobre a tua candidatura? Dúvidas sobre a conferência?<br/>
		Apetece-te conversar e trocar umas ideias sobre isto, aquilo ou aqueloutro?<br/>
		Estamos por aqui: <a href="mailto:hello@ytconference.com">hello@ytconference.com</a>
	</p>
	<p>Até breve!</p>
</div>

@stop 
{{-- Scripts --}} 
@section('scripts')

@stop
