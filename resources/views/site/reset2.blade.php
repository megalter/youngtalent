@extends('site.layouts.login_layout') {{-- Assist --}}
@section('title') {{{ Lang::get('site/user.change_password') }}} ::
@parent @stop @section('content')
<div class="container-fluid" style='margin-top: 10px;'>
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default bg-primary" style="margin-top: 20px; border: 0px;">
				<div class="modal-header" id="pedido_registo_wraper"><h4 class="modal-title">REDEFINIR PASSWORD</h4></div>
				<div class="panel-body" style="border: 15px solid; padding: 30px;">

					@if (count($errors) > 0)
					<div class="alert alert-danger">
						Os dados que colocou não são válidos<br>
							@foreach ($errors->all() as $error)
							{{ $error }}<br/>
							@endforeach
					</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="{{URL::to('/password/reset')}}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="token" value="{{ $token }}">
						
						<div class="form-group clearfix">
							<label class="control-label" for="UserName">{{Lang::get('site/user.email')}}</label>
							<div class="">
								<div class="input-group">
									<div class="input-group-addon">
										<img src="{{asset('assets/site/img/Icons/at.png')}}" alt="" class="icon" />
									</div>
									<input class="form-control" data-val="true" data-val-email="{{Lang::get('site/user.email_valid')}}" data-val-required="{{Lang::get('site/user.email_required')}}" data-val-unique="{{Lang::get('site/user.email_unique')}}" id="UserName" name="email" type="email" value="">
								</div>
							</div>
						</div>
						
						<div class="form-group clearfix">
							<label class="control-label" for="password">{{Lang::get('site/user.password')}}</label>
							<div class="">
								<div class="input-group">
									<div class="input-group-addon">
										<img src="{{asset('assets/site/img/Icons/padlock_open.png')}}" alt="" class="icon" />
									</div>
									<input class="form-control" id="password" name="password" type="password">
								</div>
							</div>
						</div>

						<div class="form-group clearfix">
							<label class="control-label" for="password_confirmation">{{Lang::get('site/user.password_confirm')}}</label>
							<div class="">
								<div class="input-group">
									<div class="input-group-addon">
										<img src="{{asset('assets/site/img/Icons/padlock_open.png')}}" alt="" class="icon" />
									</div>
									<input class="form-control" id="password_confirmation" name="password_confirmation" type="password">
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="submit-cmd btn btn-default width-small" id="submit_recover_password">
									Redefinir
								</button>
							</div>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>
@stop