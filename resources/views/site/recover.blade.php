@extends('site.layouts.default') {{-- Web site Title --}}
@section('title') {{{ Lang::get("admin/users.users") }}} :: @parent
@stop {{-- Content --}} @section('content')

<header>
	<div class="header-content">
		<div class="header-content-inner">
			<h1 style="font-size: 30px;">Recuperar Password</h1>
			<hr>
			{!! Form::open(array('url'=> 'recuperar-password', 'name'=>'recuperar-password')) !!}
			<div class="form-group col-xs-10 col-xs-offset-1 col-sm-offset-4 col-sm-4 floating-label-form-group controls clearfix ">
				<div class="">
					<div class="input-group">
						<div class="input-group-addon register-addon">
							<i class="glyphicon glyphicon-envelope"></i>
						</div>
						<input class="form-control" id="email" name="email" type="text" placeholder="E-mail">
					</div>
				</div>
			</div>
			<div class="form-group col-xs-10 col-xs-offset-1 col-sm-offset-4 col-sm-4 floating-label-form-group controls clearfix">
				<div class="input-group col-xs-12">
					<label class="control-label" for="login">{{Lang::get('site/user.register')}}</label>
					<input class="form-control" id="submit" name="recover" type="submit" value="Enviar" style="text-align: center; background-color: #199EDF; color: #FFF;">
				</div>
			</div>
			<div class="form-group col-xs-10 col-xs-offset-1 col-sm-offset-4 col-sm-4 floating-label-form-group controls clearfix">
				<div class="input-group col-xs-12">
					<label class="control-label" for="login">{{Lang::get('site/user.register')}}</label>
					<a href="{{URL::to('/')}}" class="form-control register" style="text-align: center;">Voltar</a>
				</div>
			</div>

			{!! Form::close() !!}
			
			<div class="form-group col-xs-10 col-xs-offset-1 col-sm-offset-4 col-sm-4 floating-label-form-group controls clearfix">
				@if (count($errors) > 0)
				<div class="alert alert-danger">
					Os dados que colocou não são válidos<br>
						@foreach ($errors->all() as $error)
						{{ $error }}<br/>
						@endforeach
				</div>
				@endif
				@if(!empty($status))
				<div class="alert alert-success" style="display: block;">
					Pedido de recuperação de password enviado. Consulta o teu E-mail para continuares o processo.
				</div>
				@endif
			</div>
			
		</div>
	</div>
</header>


@stop 
{{-- Scripts --}} 
@section('scripts')
<script>
	
</script>
@stop
