@extends('site.layouts.default') {{-- Web site Title --}}
@section('title') {{{ Lang::get("admin/users.users") }}} :: @parent
@stop {{-- Content --}} @section('content')

<header>
	<div class="header-content">
		<div class="header-content-inner">
			<div class="form-group col-xs-12 col-sm-offset-3 col-sm-6 floating-label-form-group controls clearfix text-center index_msg">
				
			</div>
		</div>
	</div>
</header>


@stop 
{{-- Scripts --}} 
@section('scripts')

@stop
