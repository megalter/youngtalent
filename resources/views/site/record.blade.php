@extends('site.layouts.default') {{-- Web site Title --}}
@section('title') {{{ Lang::get("admin/users.users") }}} :: @parent
@stop {{-- Content --}} @section('content')

<header>
	<div class="header-content">
			<div class="row">
				<div class="form-group col-xs-12	floating-label-form-group controls clearfix text-center">
					Estás pronto para contar a tua história?
				</div>
			</div>
			<!-- controls !-->
			<div class="row">
				<div class="col-xs-12">
					<button id="start-recording" class="form-control" style="width: 100px; display: inline-block;">REC</button>
					<button id="stop-recording" class="form-control" style="width: 100px; display: inline-block;" disabled>STOP</button>
				</div>
			</div>
			<div class="row record_row">
				<div class="twentypercent">
					<div id="recordings_container"></div>
				</div>
				<div id="container" class="video-holder">
					
				</div>
				<div class="twentypercent">
					<div id="record_explanation">
						<p>
							* Carrega em REC para começares a gravar (assim que carregares, a gravação é iniciada por isso antes de o fazeres garante que ajustas a luz, o som, etc. Tens duas oportunidades de gravar a tua história, make it count!)<br/><br/>
							* Carrega em STOP para parares a gravação.<br/><br/>
							* Escolhe a tua gravação favorita e carrega em ENVIAR.
						</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<button id="send-recording" class="form-control" style="width: 100px; display: inline-block;" disabled>ENVIAR</button>
				</div>
			</div>
			<div class="row">
				<div id="three_questions" class="form-group col-xs-8 col-xs-offset-2 floating-label-form-group controls clearfix text-center">
					<p style="font-weight: bold;">
						Lembra-te das 3 perguntas:<br/>
						#1 Fala-nos de uma situação em que tenhas tido que lidar com uma mudança ou situação imprevista. O que fizeste?<br/>
						#2 Se pudesses dedicar-te a uma causa atual que te apaixona, qual seria? Que equipa reunirias para trabalhar contigo, e como os convencerias a fazer parte do projeto?<br/>
						#3 Fala-nos do projeto de que mais te orgulhas: qual era o desafio, que riscos assumiste, como lidaste com os obstáculos, o que aprendeste e que resultados alcançaste?
					</p>
				</div>
			</div>
	</div>
</header>
<div id="success" class="text-center" style="display: none; margin-top:100px;">
	<h1>Yeah! Está feito, a tua história já cá canta!</h1>
	<p>
		As centenas de milhões de candidaturas à YTC serão analisadas durante o mês de Março.<br/>
		Podes contar com o nosso feedback à tua história no início do mês de Abril!
	</p>
	<p>
		Até lá, tens alguma questão sobre a tua candidatura? Dúvidas sobre a conferência?<br/>
		Apetece-te conversar e trocar umas ideias sobre isto, aquilo ou aqueloutro?<br/>
		Estamos por aqui: <a href="mailto:hello@ytconference.com">hello@ytconference.com</a>
	</p>
	<p>Até breve!</p>
</div>
<div id="video_preview" class="modalDialog-uploadcv">
	<div style="height:300px; overflow:auto; padding-top:25px;">
		<div class="col-sm-12 text-center">
			<video id="video" controls>
				<source src="" type="video/webm">
			</video>
			<audio id="buzzer" src="#" type="audio/wav">Your browser does not support the &#60;audio&#62; element.</audio>
		</div>
		<div class="col-sm-12 text-center">
			<a href="#close" class="text-center" style="color:#000; text-transform:uppercase; font-weight:bold;">Fechar</a>
		</div>
			
		
		
	</div>
	
</div>

<!--<video id="video" controls>
	<source src="{{URL::to('appfiles/videos/VJAhDWy89g9FQTaVTu9BukoPPasQZmiTs9Z5VUPV.webm')}}" type="video/webm">
</video>
<audio id="buzzer" src="{{URL::to('appfiles/videos/w6jAqIAidzurhsx1MzDejFX1DYwn0e6u6u7RYAaO.wav')}}" type="audio/wav">Your browser does not support the &#60;audio&#62; element.</audio>-->

@stop 
{{-- Scripts --}} 
@section('scripts')
<script>
	$("video").on('play',function(){
		//my code here
		$('#buzzer')[0].play();
	});
	$("video").on('pause',function(){
		//my code here
		$('#buzzer')[0].pause();
	});
	
	function captureUserMedia(mediaConstraints, successCallback, errorCallback) {
		navigator.mediaDevices.getUserMedia(mediaConstraints).then(successCallback).catch(errorCallback);
	}
	// commonly allowed resolutions:
	// ['1920:1080', 
	// '1280:720', 
	// '960:720', 
	// '640:360', 
	// '640:480', 
	// '320:240', 
	// '320:180']
	var resolution_x = 320;
	var resolution_y = 240;

	var mediaConstraints = {
		audio: true,
		video: IsEdge ? true : 
				!!navigator.mozGetUserMedia ? {width: { min: 320, ideal: 320, max: 320}} :
							{mandatory: { maxWidth: resolution_x, maxHeight: resolution_y,}}
	};
	
	var aud; var aud2;
	var vid; var vid2;
	var counter = 0;
	
	function stoptracks(element, index, array) {
		array[index].stop();
	}

	function stopRecording(){
		console.log('stopped');
		
		document.querySelector('#start-recording').disabled = counter == 2 ? true : false;
		
		multiStreamRecorder.stop();
		multiStreamRecorder.stream.getAudioTracks().forEach(stoptracks);
		multiStreamRecorder.stream.getVideoTracks().forEach(stoptracks);
		document.querySelector('#send-recording').disabled = false;
		document.querySelector('#stop-recording').disabled = true;
	}

	document.querySelector('#start-recording').onclick = function () {
		counter++;
		setTimeout(function(){ stopRecording(); }, 302 * 1000);
		
		this.disabled = true;
		var d = new Date();
		var start = d.getTime();
		//Set timer for 5 minutes
		
		captureUserMedia(mediaConstraints, onMediaSuccess, onMediaError);
		console.log('passed here');
		//setTimeout(stopRecording(), 5000);
	};
	document.querySelector('#stop-recording').onclick = function () {
		stopRecording();
	};
	document.querySelector('#send-recording').onclick = function(){
		var selection = $('input[name=video_select]:checked', '#recordings_container').val();
		if(selection == 1){
			var formData = new FormData();
			formData.append('audio-filename', 'audio.wav');
			formData.append('audio-blob', aud);
			formData.append('video-filename', 'video.webm');
			formData.append('video-blob', vid);
			
			xhr("{{URL::to('record')}}", formData, function (fileURL) {
				//Do something after post
				$('header').hide('slow');
				$('#success').show('slow');
				document.querySelector('#send-recording').disabled = true;
			});
		}
		else if(selection == 2){
			var formData = new FormData();
			formData.append('audio-filename', 'audio.wav');
			formData.append('audio-blob', aud2);
			formData.append('video-filename', 'video.webm');
			formData.append('video-blob', vid2);
			
			xhr("{{URL::to('record')}}", formData, function (fileURL) {
				//Do something after post
				$('header').hide('slow');
				$('#success').show('slow');
				document.querySelector('#send-recording').disabled = true;
			});
		}
		else
			alert('Por favor seleciona uma das gravações');
		
		function xhr(url, data, callback) {
			var request = new XMLHttpRequest();
			request.onreadystatechange = function () {
				if (request.readyState == 4 && request.status == 200) {
					callback(location.href + request.responseText);
				}
			};
			request.open('POST', url);
			request.send(data);
		}
	};
	
	var multiStreamRecorder;
	var audioVideoBlobs = {};
	var recordingInterval = 0;
	//When stream begins
	function onMediaSuccess(stream) {
		var video = document.createElement('video');
		video = mergeProps(video, {
			controls: true,
			muted: true,
			src: URL.createObjectURL(stream),
			autoplay: true
		});
		
		video.addEventListener('loadedmetadata', function () {
			multiStreamRecorder = new MultiStreamRecorder(stream);
			multiStreamRecorder.stream = stream;
			
			multiStreamRecorder.canvas = {
				width: video.width,
				height: video.height
			};
			multiStreamRecorder.video = video;
			multiStreamRecorder.ondataavailable = function (blobs) {
				appendLink(blobs.video, blobs.audio);
			};

			timeInterval = 400 * 1000;
			// get blob after specific time interval
			multiStreamRecorder.start(timeInterval);
			document.querySelector('#stop-recording').disabled = false;			
		}, false);
		
		if(counter == 0){
			container.appendChild(video);
			container.appendChild(document.createElement('hr'));
		}
		else{
			$('#container').children().first().remove();
			$('#container').prepend(video);
		}
		video.play();
	}
	
	function appendLink(blobV, blobA) {
		if(counter == 1){
			vid = blobV;
			aud = blobA;
		}
		else{
			vid2 = blobV;
			aud2 = blobA;
		}

		var atext = 'GRAVAÇÃO Nº'+counter;
		$('<a href="#video_preview" onclick="loadMedia('+counter+')">'+atext+'</a>').appendTo('#recordings_container');
		$('<input type="radio" name="video_select" value="'+counter+'" class="record_radio"/>').appendTo('#recordings_container');
		recordings_container.appendChild(document.createElement('hr'));

	}
	
	function onMediaError(e) {
		console.error('media error', e);
	}
	var container = document.getElementById('container');
	var recordings_container = document.getElementById('recordings_container');
	var index = 1;
	window.onbeforeunload = function () {
		document.querySelector('#start-recording').disabled = false;
	};
	
	function loadMedia(what){
		var temp_video = what == 1 ? vid : vid2;
		var temp_audio = what == 1 ? aud : aud2;
		$('#video source').attr('src', URL.createObjectURL(temp_video));
		$('#buzzer').attr('src', URL.createObjectURL(temp_audio));
		$("#video")[0].load();
		$("#buzzer")[0].load();
	}
</script>
@stop
