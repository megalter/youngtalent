<!DOCTYPE html>

<html lang="en">

<head id="Starter-Site">

<meta charset="UTF-8">

<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<title>Young Talent :: @yield('title')</title>

@include('site.common.head')

<script src="https://cdn.webrtc-experiment.com/MediaStreamRecorder.js"></script>
<!-- for Edige/FF/Chrome/Opera/etc. getUserMedia support -->
<script src="https://cdn.rawgit.com/webrtc/adapter/master/adapter.js"></script>

<style>
/*    input {
        border: 1px solid rgb(46, 189, 235);
        border-radius: 3px;
        font-size: 1em;
        outline: none;
        padding: .2em .4em;
        width: 60px;
        text-align: center;
    }
    button, select {
        font-family: Myriad, Arial, Verdana;
        font-weight: normal;
        border-top-left-radius: 3px;
        border-top-right-radius: 3px;
        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
        padding: 4px 12px;
        text-decoration: none;
        color: rgb(27, 26, 26);
        display: inline-block;
        box-shadow: rgb(255, 255, 255) 1px 1px 0px 0px inset;
        text-shadow: none;
        background: -webkit-gradient(linear, 0% 0%, 0% 100%, color-stop(0.05, rgb(241, 241, 241)), to(rgb(230, 230, 230)));
        font-size: 20px;
        border: 1px solid red;
        outline:none;
    }
    button:active, button:focus, select:active {
        background: -webkit-gradient(linear, 0% 0%, 0% 100%, color-stop(5%, rgb(221, 221, 221)), to(rgb(250, 250, 250)));
        border: 1px solid rgb(142, 142, 142);
    }
    button[disabled], select[disabled] {
        background: rgb(249, 249, 249);
        border: 1px solid rgb(218, 207, 207);
        color: rgb(197, 189, 189);
    }
    blockquote {
        font-size: 20px;
        color: rgb(172, 10, 10);
        border: 1px solid rgb(172, 10, 10);
        padding: 5px 10px;
        border-radius: 5px;
        margin: 9px 10px;
    }
    span {
        border: 1px dotted red;
        background: yellow;
        padding: 0 5px;
    }*/
    </style>

</head>
<body id="page-top">
	@if(count($errors) > 0  && Request::is('entrar'))
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
	@endif
	
    @include('site.common.header')

	@yield('content')

    @include('site.common.footer')

	@include('site.common.scripts')
	
	@yield('scripts')
	
</body>
</html>
