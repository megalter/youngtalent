@extends('site.layouts.default') {{-- Web site Title --}}
@section('title') {{{ Lang::get("admin/users.users") }}} :: @parent
@stop {{-- Content --}} @section('content')

<header>
	<div class="header-content">
		<div class="header-content-inner">
			<div class="form-group col-xs-8 col-xs-offset-2 floating-label-form-group controls clearfix text-center index_msg">
				Tens a melhor média do teu curso? <b>So what?</b><br/>
				Nunca chumbaste um ano? <b>Who cares?</b><br/>
				Um track record imaculado não nos impressiona.<br/>
				<br/>
				Mais do que CVs perfeitos, queremos conhecer pessoas que erram, que se enganam,<br/>
				que mudam de opinião, mas que estão <b>determinadas a cumprir a sua missão</b>, seja ela qual for.<br/>
				<b>Queremos conhecer as suas histórias.</b><br/>
				<h5>
					<span class="index_msg2"><b>Conta-nos a tua</b> e impressiona-nos com as tuas true colors.<br/></span>
				</h5>
				<h5><b>Vemo-nos em Maio?</b></h5>
			</div>
			{!! Form::open(array('url'=> 'entrar', 'name'=>'login')) !!}
			<div class="form-group col-xs-10 col-xs-offset-1 col-sm-offset-4 col-sm-4 floating-label-form-group controls clearfix ">
				<label class="control-label" for="email">{{Lang::get('site/user.email')}}</label>
				<div class="">
					<div class="input-group">
						<div class="input-group-addon">
							<img src="{{asset('assets/backoffice/img/at.png')}}" alt="" class="icon" />
						</div>
						<input class="form-control" id="email" name="email" type="text" placeholder="E-mail">
					</div>
				</div>
			</div>
			<div class="form-group col-xs-10 col-xs-offset-1 col-sm-offset-4 col-sm-4 floating-label-form-group controls clearfix">
				<label class="control-label" for="password">{{Lang::get('site/user.password')}}</label>
				<div class="">
					<div class="input-group">
						<div class="input-group-addon">
							<img src="{{asset('assets/backoffice/img/padlock_open.png')}}" alt="" class="icon" />
						</div>
						<input class="form-control" id="password" name="password" type="password" placeholder="Password">
					</div>
				</div>
			</div>
			
			<div class="form-group col-xs-10 col-xs-offset-1 col-sm-offset-4 col-sm-4 floating-label-form-group controls clearfix">
				<div class="input-group col-xs-12">
					<label class="control-label" for="login">{{Lang::get('site/user.login')}}</label>
					<input class="form-control" id="submit" name="login" type="submit" value="Entrar" style="text-align: center;">
				</div>
			</div>
			
			<div class="form-group col-xs-10 col-xs-offset-1 col-sm-offset-4 col-sm-4 floating-label-form-group controls clearfix">
				<div class="input-group col-xs-12">
					<label class="control-label" for="login">{{Lang::get('site/user.register')}}</label>
					<a href="{{URL::to('registar')}}" class="form-control register" style="text-align: center; background-color: #199EDF; color: #FFF;">Registar</a>
				</div>
			</div>
			
			<div class="form-group col-xs-10 col-xs-offset-1 col-sm-offset-4 col-sm-4 floating-label-form-group controls clearfix">
				<div class="text-center">
					<br/>
					<a href="{{URL::to('recuperar-password')}}">Esqueceu-se da password?</a>
				</div>
			</div>
			
			@if(count($errors) > 0)
			<div class="form-group col-xs-10 col-xs-offset-1 col-sm-offset-4 col-sm-4 floating-label-form-group controls clearfix">
				<div class="alert alert-danger">
				@foreach ($errors->all() as $error)
					{{ $error }}
				@endforeach
				</div>
			</div>
			@endif
			@if(session('reset'))
			<div class="form-group col-xs-10 col-xs-offset-1 col-sm-offset-4 col-sm-4 floating-label-form-group controls clearfix">
				<div class="alert alert-success">
					{{session('reset')}}
				</div>
			</div>
			@endif
			
			{!! Form::close() !!}
		</div>
	</div>
</header>


@stop 
{{-- Scripts --}} 
@section('scripts')

@stop
