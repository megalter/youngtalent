@extends('site.layouts.default') {{-- Web site Title --}}
@section('title') {{{ Lang::get("admin/users.users") }}} :: @parent
@stop {{-- Content --}} @section('content')

<header>
	<div class="header-content">
		<div class="header-content-inner">
			<h1 style="font-size: 30px;">Recuperar Password</h1>
			<hr>
			
			@if (count($errors) > 0)
			<div class="form-group col-xs-10 col-xs-offset-1 col-sm-offset-4 col-sm-4 floating-label-form-group controls clearfix">
				<div class="alert alert-danger">
					Os dados que colocou não são válidos<br>
				</div>
			</div>
			@endif
			
			{!! Form::open(array('url'=> 'password/reset', 'name'=>'reset-password')) !!}
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="token" value="{{ $token }}">
			<div class="form-group col-xs-10 col-xs-offset-1 col-sm-offset-4 col-sm-4 floating-label-form-group controls clearfix ">
				<div class="">
					<div class="input-group">
						<div class="input-group-addon register-addon">
							<i class="glyphicon glyphicon-envelope"></i>
						</div>
						<input class="form-control" data-val="true" data-val-email="{{Lang::get('site/user.email_valid')}}" data-val-required="{{Lang::get('site/user.email_required')}}" data-val-unique="{{Lang::get('site/user.email_unique')}}" id="UserName" name="email" type="email" value="" placeholder="E-mail">
					</div>
				</div>
			</div>
			<div class="form-group col-xs-10 col-xs-offset-1 col-sm-offset-4 col-sm-4 controls clearfix">

				<div class="">
					<div class="input-group">
						<div class="input-group-addon register-addon">
							<i class="glyphicon glyphicon-lock"></i>
						</div>
						<input class="form-control" id="password" name="password" type="password" placeholder="Password">
					</div>
				</div>
			</div>
			<div class="form-group col-xs-10 col-xs-offset-1 col-sm-offset-4 col-sm-4 floating-label-form-group controls clearfix">
				<div class="">
					<div class="input-group">
						<div class="input-group-addon register-addon">
							<i class="glyphicon glyphicon-lock"></i>
						</div>
						<input class="form-control" id="password_confirmation" name="password_confirmation" type="password" placeholder="Confirmar password">
					</div>
				</div>
			</div>
			
			<div class="form-group col-xs-10 col-xs-offset-1 col-sm-offset-4 col-sm-4 floating-label-form-group controls clearfix">
				<div class="input-group col-xs-12">
					<label class="control-label" for="login">{{Lang::get('site/user.register')}}</label>
					<input class="form-control" id="submit" name="recover" type="submit" value="Alterar" style="text-align: center; background-color: #199EDF; color: #FFF;">
				</div>
			</div>
			<div class="form-group col-xs-10 col-xs-offset-1 col-sm-offset-4 col-sm-4 floating-label-form-group controls clearfix">
				<div class="input-group col-xs-12">
					<label class="control-label" for="login">{{Lang::get('site/user.register')}}</label>
					<a href="{{URL::to('/')}}" class="form-control register" style="text-align: center;">Cancelar</a>
				</div>
			</div>

			{!! Form::close() !!}
			
		</div>
	</div>
</header>


@stop 
{{-- Scripts --}} 
@section('scripts')
<script>
	
</script>
@stop
