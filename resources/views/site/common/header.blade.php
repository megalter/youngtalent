<!-- Navigation -->
<nav class="navbar navbar-trn" role="navigation" id="navbarSettings" style="">
	<div class="container">
		<div class="row">
			<div class="col-xs-2">&nbsp;
			</div>
			<div class="col-xs-8">
				<div class="text-center">
					<img src="{{asset('assets/backoffice/img/Logotipo-YTC-Plataforma.png')}}" alt="" class="logo"/>
				</div>
			</div>
			<div class="col-xs-2 logo">
				@if(!empty($user))
				<ul class="nav navbar-top-links navbar-right">
					<li class="dropdown"><a class="btn account dropdown-toggle"
											data-toggle="dropdown" href="#">
							<div class="user">
								<span class="hello"> Olá, </span>
								<span class="name"> {{{ $user->first_name }}}!</span>
							</div>
						</a>
						<ul class="dropdown-menu" style="max-width: 146px;">
							<li><a href="{{{ URL::to('intro') }}}"><i
										class="glyphicon glyphicon-play"></i> Introdução</a></li></li>
							<li><a href="{{{ URL::to('record') }}}"><i
										class="glyphicon glyphicon-facetime-video"></i> Gravação</a></li></li>
							<li><a href="{{{ URL::to('sair') }}}"><i
										class="glyphicon glyphicon-off"></i> {{
								Lang::get('site/site.logout') }}</a></li>
						</ul></li>
				</ul>
				@endif
			</div>
		</div>
	</div>
</nav>

<!-- If navbar is fixed to top and it has the class navbar-default or navbar-inverse this div will add space below the navbar-->
<div id="navbarSpaceBottom"></div>