<!-- Side Menu -->
<div class="sidebar-off hidden-xs sidebar">
	<div class="wrap">

		<!--<a id="sidebar-close" title="Close sidebar">
			<div class="clearfix">
				<h2 class="title">Side Menu</h2>
				<div class="close-icon">
					<span class="close-a"></span>
					<span class="close-b"></span>
				</div>
			</div>
		</a>-->

		<div class="form-lg">
			<div class="input-group all-trn" role="search">
				<input type="text" class="form-control" placeholder="{{Lang::get('site/site.search')}}">
				<span class="input-group-btn">
					<button class="btn btn-all-trn" title="Search" type="button">
						<i class="fa fa-search"></i>
					</button>
				</span> 
			</div>
		</div>

		<div class="space-sm"></div>
		@if(!Auth::check())
		<h4>Entrar <i class="glyphicon fa fa-user"></i></h4>
		{!! Form::open(array('url'=> 'entrar', 'name'=>'login')) !!}
			<div class="row">
				<div class="col-xs-12 text-left">
					<div class="form-group clearfix">
						<label class="control-label" for="UserName">{{Lang::get('site/user.email')}}</label>
						<div class="">
							<div class="input-group">
								<div class="input-group-addon">
									<img src="{{asset('assets/backoffice/img/at.png')}}" alt="" class="icon" />
								</div>
								<input class="form-control" data-val="true" data-val-email="{{Lang::get('site/user.email_valid')}}" data-val-required="{{Lang::get('site/user.email_required')}}" data-val-unique="{{Lang::get('site/user.email_unique')}}" id="UserName" name="email" type="email" value="">
							</div>
						</div>
					</div>
					<div class="form-group clearfix">
						<label class="control-label" for="Password">{{Lang::get('site/user.password')}}</label>
						<div class="">
							<div class="input-group">
								<div class="input-group-addon">
									<img src="{{asset('assets/backoffice/img/padlock_open.png')}}" alt="" class="icon" />
								</div>
								<input class="form-control" data-val="true" data-val-required="{{Lang::get('site/user.password_required')}}" id="Password" name="password" type="password">
							</div>
						</div>
					</div>
				</div>
			</div>
			<span class="pull-right">
				<button id="loginFormSubmitCmd" type="submit" class="submit-cmd btn btn-default width-small">{{Lang::get('site/site.login')}} <i class="fa fa-arrow-circle-right"></i></button>
			</span>

			<div class="clearfix"></div>
		{!! Form::close() !!}
		@else
		
		<div class="navbar-default sidebar" role="navigation">
			<div class="sidebar-nav navbar-collapse">
				@yield('menu')
				<ul class="nav" id="side-menu">
					@if($user->Dev || $user->Admin || $user->Float)
					<li class="li_area">Administrador</li>
					<li><a href="{{URL::to('backoffice/dashboard')}}"
						{{ (Request::is('backoffice/dashboard') || Request::is('backoffice') ? ' class=active' : '') }}> <i
							class="fa fa-dashboard fa-fw"></i><span class="hidden-sm text">
								Dashboard</span></a></li>
					<li><a href="{{URL::to('backoffice/categories')}}"
						{{ (Request::is('backoffice/categories*') ? ' class=active' : '') }}> <i
							class="glyphicon glyphicon-list-alt"></i><span
							class="hidden-sm text"> Categorias</span></a></li>
					<li><a href="{{URL::to('backoffice/files')}}"
						{{ (Request::is('backoffice/files*') ? ' class=active' : '') }}> <i
							class="glyphicon glyphicon-file"></i><span
							class="hidden-sm text"> Ficheiros</span></a></li>
					<li><a href="{{URL::to('backoffice/users')}}"
						{{ (Request::is('backoffice/users*') ? ' class=active' : '') }}> <i
							class="glyphicon glyphicon-user"></i><span
							class="hidden-sm text"> Utilizadores</span></a></li>
					<li><a href="{{URL::to('backoffice/statistics')}}"
						{{ (Request::is('backoffice/statistics*') ? ' class=active' : '') }}> <i
							class="glyphicon glyphicon-signal"></i><span
							class="hidden-sm text"> Estatísticas</span></a></li>
					@endif
					@if($user->Dev)
					<li class="li_area">Developer</li>
					<li><a href="{{URL::to('backoffice/roles')}}"
						{{ (Request::is('backoffice/roles*') ? ' class=active' : '') }}> <i
							class="glyphicon glyphicon-tasks"></i><span
							class="hidden-sm text"> Funções Utilizador</span></a></li>
					<li><a href="{{URL::to('backoffice/views')}}"
						{{ (Request::is('backoffice/views*') ? ' class=active' : '') }}> <i
							class="glyphicon glyphicon-eye-open"></i><span
							class="hidden-sm text"> Vistas</span></a></li>
					<li><a href="{{URL::to('backoffice/navigation')}}"
						{{ (Request::is('backoffice/navigation*') ? ' class=active' : '') }}> <i
							class="glyphicon glyphicon-road"></i><span
							class="hidden-sm text"> Navegação</span></a></li>
					@endif
				</ul>
			</div>

			<br/>
			@if(Request::segment(2) == 'files')
			(Arraste para aqui os itens)
				@if(isset($category) && $category->id != 1)
				<i id="archivate" class="glyphicon glyphicon-folder-open fa-5x admin-trash"><h5>Arquivo</h5></i>
				<br/>
				@endif
				<i id="garbage" class="glyphicon glyphicon-trash fa-5x admin-trash"><h5>Apagar</h5></i>
			@endif
		</div>
		
		@endif
	</div>
</div>