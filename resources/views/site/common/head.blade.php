<meta name="keywords" content="" />
<meta name="author" content="" />
<meta name="description" content="" />
<meta name="google-site-verification" content="">
<meta name="title" content="Dev">
<meta name="subject" content="Development @ float">
<meta name="creator" content="Float">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
<link href="{{asset('assets/backoffice/css/bootstrap.min.css')}}" rel="stylesheet">

<!--=== CSS - Dev ===-->
<link href="{{asset('assets/backoffice/css/styles.min.css')}}" rel="stylesheet" id="colors">

<!-- Google Fonts - Lato -->
<link href="{{asset('assets/backoffice/css/fonts/lato.min.css')}}" rel="stylesheet">
<!-- Google Fonts - Roboto -->
<link href="{{asset('assets/backoffice/css/fonts/roboto.min.css')}}" type="text/css" rel="stylesheet">

<!-- Font Awesome Icons -->
<link href="{{asset('assets/backoffice/css/fonts/font-awesome.min.css')}}" rel="stylesheet">
	
<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<link rel="shortcut icon"
	href="{{{ asset('assets/backoffice/ico/favicon.ico') }}}">

<link href="{{asset('assets/backoffice/css/plugins/dataTables/jquery.dataTables.css')}}"
	rel="stylesheet">
<link href="{{asset('assets/backoffice/css/plugins/dataTables/dataTables.bootstrap.min.css')}}"
	rel="stylesheet">
<link href="{{asset('assets/backoffice/css/plugins/colorbox.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/backoffice/css/sb-admin-2.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/backoffice/css/jquery-ui-1.10.3.custom.min.css')}}" rel="stylesheet">