<!-- Modernizr -->
<script src="{{asset('assets/backoffice/js/modernizr.custom.min.js')}}"></script>

<!-- jQuery -->
<script src="{{asset('assets/backoffice/js/jquery-2.1.1.min.js')}}"></script>
<script src="{{asset('assets/backoffice/js/jquery-ui.1.11.2.min.js')}}"></script>
<!--<![endif]-->
<!--[if IE]>
	<script src="{{asset('assets/admin/js/jquery-1.11.1.min.js')}}"></script>
	<![endif]-->

<!--<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/2.1.0/less.min.js"></script>-->

<!-- Bootstrap JS -->
<script src="{{asset('assets/backoffice/js/bootstrap.min.js')}}"></script>

<!-- Bootstrap Plugin - open dropdown on hover -->
<script src="{{asset('assets/backoffice/js/plugins/bootstrap-hover-dropdown.min.js')}}"></script>

<!-- LESS preprocessor -->
<script src="{{asset('assets/backoffice/js/less.min.js')}}"></script>

<!-- DataTables -->
<script src="{{asset('assets/backoffice/js/plugins/dataTables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/backoffice/js/plugins/dataTables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/backoffice/js/plugins/dataTables/bootstrap-dataTables-paging.min.js')}}"></script>
<script src="{{asset('assets/backoffice/js/plugins/dataTables/datatables.fnReloadAjax.min.js')}}"></script>

<script src="{{asset('assets/backoffice/js/plugins/jquery.colorbox.min.js')}}"></script>
<script src="{{asset('assets/backoffice/js/jquery-migrate-1.2.1.min.js')}}"></script>