@extends('site.layouts.default') {{-- Web site Title --}}
@section('title') {{{ Lang::get("admin/users.users") }}} :: @parent
@stop {{-- Content --}} @section('content')

<header>
	<div class="header-content">
		<div class="header-content-inner">
			{!! Form::open(array('url'=> 'registar', 'name'=>'login')) !!}
			<div class="form-group col-xs-10 col-xs-offset-1 col-sm-offset-4 col-sm-4 floating-label-form-group controls clearfix ">

				<div class="">
					<div class="input-group">
						<div class="input-group-addon register-addon">
							<i class="glyphicon glyphicon-font"></i>
						</div>
						<input class="form-control" id="name" name="name" type="text" placeholder="Primeiro e último nome">
					</div>
				</div>
			</div>
			<div class="form-group col-xs-10 col-xs-offset-1 col-sm-offset-4 col-sm-4 floating-label-form-group controls clearfix ">

				<div class="">
					<div class="input-group">
						<div class="input-group-addon register-addon">
							<i class="glyphicon glyphicon-envelope"></i>
						</div>
						<input class="form-control" id="email" name="email" type="email" placeholder="E-mail">
					</div>
				</div>
			</div>
			<div class="form-group col-xs-10 col-xs-offset-1 col-sm-offset-4 col-sm-4 controls clearfix">

				<div class="">
					<div class="input-group">
						<div class="input-group-addon register-addon">
							<i class="glyphicon glyphicon-lock"></i>
						</div>
						<input class="form-control" id="password" name="password" type="password" placeholder="Password">
					</div>
				</div>
			</div>
			<div class="form-group col-xs-10 col-xs-offset-1 col-sm-offset-4 col-sm-4 controls clearfix">

				<div class="">
					<div class="input-group">
						<div class="input-group-addon register-addon">
							<i class="glyphicon glyphicon-lock"></i>
						</div>
						<input class="form-control" id="confirm_password" name="confirm_password" type="password" placeholder="Confirmar password">
					</div>
				</div>
			</div>
			<div class="form-group  col-xs-10 col-xs-offset-1 col-sm-offset-4 col-sm-4 floating-label-form-group controls clearfix ">

				<div class="">
					<div class="input-group">
						<div class="input-group-addon register-addon">
							<i class="glyphicon glyphicon-calendar"></i>
						</div>
						<input class="form-control" id="birthdate" name="birthdate" type="text" placeholder="Data de nascimento">
					</div>
				</div>
			</div>
			<div class="form-group col-xs-10 col-xs-offset-1 col-sm-offset-4 col-sm-4 floating-label-form-group controls clearfix ">

				<div class="">
					<div class="input-group">
						<div class="input-group-addon register-addon">
							<i class="glyphicon glyphicon-phone"></i>
						</div>
						<input class="form-control" id="phone" name="phone" type="text" placeholder="Telemóvel">
					</div>
				</div>
			</div>
			<div class="form-group  col-xs-10 col-xs-offset-1 col-sm-offset-4 col-sm-4 floating-label-form-group controls clearfix ">

				<div class="">
					<div class="input-group">
						<div class="input-group-addon register-addon">
							<i class="glyphicon glyphicon-globe"></i>
						</div>
						<input class="form-control" id="address" name="address" type="text" placeholder="Morada">
					</div>
				</div>
			</div>
			<div class="form-group col-xs-10 col-xs-offset-1 col-sm-offset-4 col-sm-4 floating-label-form-group controls clearfix ">

				<div class="">
					<div class="input-group">
						<div class="input-group-addon register-addon">
							<i class="glyphicon glyphicon-font"></i>
						</div>
						<select class="form-control" id="university" name="university" placeholder="Universidade">
							@foreach($universities as $item)
							<option value="{{$item->id}}" @if(empty($item->dependency)) class="university" @endif>{{$item->name}}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
			<div id="university_block" class="form-group col-xs-10 col-xs-offset-1 col-sm-offset-4 col-sm-4 floating-label-form-group controls clearfix" style="display: none;">

				<div class="">
					<div class="input-group">
						<div class="input-group-addon register-addon">
							<i class="glyphicon glyphicon-font"></i>
						</div>
						<input class="form-control" id="university_custom" name="university_custom" type="text" placeholder="Universidade">
					</div>
				</div>
			</div>
			<div class="form-group col-xs-10 col-xs-offset-1 col-sm-offset-4 col-sm-4 floating-label-form-group controls clearfix ">

				<div class="">
					<div class="input-group">
						<div class="input-group-addon register-addon">
							<i class="glyphicon glyphicon-font"></i>
						</div>
						<select class="form-control" id="degree" name="degree" placeholder="Curso">
							<option value="">Selecione a universidade 1º</option>
							<option value="">Outro</option>
						</select>
					</div>
				</div>
			</div>
			<div id="degree_block" class="form-group col-xs-10 col-xs-offset-1 col-sm-offset-4 col-sm-4 floating-label-form-group controls clearfix" style="display: none;">

				<div class="">
					<div class="input-group">
						<div class="input-group-addon register-addon">
							<i class="glyphicon glyphicon-font"></i>
						</div>
						<input class="form-control" id="degree_custom" name="degree_custom" type="text" placeholder="Curso">
					</div>
				</div>
			</div>
			<div class="form-group col-xs-10 col-xs-offset-1 col-sm-offset-4 col-sm-4 floating-label-form-group controls clearfix">
				<div class="">
					<div class="input-group">
						<br/>
						<h6>Na YTC adoramos a tua privacidade! E odiamos spam. Nunca partilharemos os teus dados com ninguém sem a tua autorização. Pinky promise.</h6>
						<span style="position: relative; top: -3px; padding-right: 5px;">Aceito os <a href="#termosecondicoes">termos e condições </a></span>
						<input id="termos" name="termos" type="checkbox">
					</div>
				</div>
			</div>
			<div class="form-group col-xs-10 col-xs-offset-1 col-sm-offset-4 col-sm-4 floating-label-form-group controls clearfix">
				<div class="input-group col-xs-12">
					<label class="control-label" for="login">{{Lang::get('site/user.register')}}</label>
					<input class="form-control" id="submit" name="register" type="submit" value="{{Lang::get('site/user.register')}}" style="text-align: center; background-color: #199EDF; color: #FFF;">
				</div>
			</div>
			<div class="form-group col-xs-10 col-xs-offset-1 col-sm-offset-4 col-sm-4 floating-label-form-group controls clearfix">
				<div class="input-group col-xs-12">
					<label class="control-label" for="login">{{Lang::get('site/user.register')}}</label>
					<a href="{{URL::to('/')}}" class="form-control register" style="text-align: center;">Voltar</a>
				</div>
			</div>

			<div id="success" style="display: none;" class="form-group col-xs-10 col-xs-offset-1 col-sm-offset-4 col-sm-4 floating-label-form-group controls clearfix">
				<div class="alert alert-success">
					Utilizador registado com sucesso
				</div>
			</div>

			@if(count($errors) > 0)
			<div class="form-group col-xs-10 col-xs-offset-1 col-sm-offset-4 col-sm-4 floating-label-form-group controls clearfix">
				<div class="alert alert-danger">
					@foreach ($errors->all() as $error)
					{{ $error }}<br/>
					@endforeach
				</div>
			</div>
			@endif

			{!! Form::close() !!}
			<div id="termosecondicoes" class="modalDialog-uploadcv">
				<div style="height:500px; overflow:auto; text-align:left;">
					<h2>Terms of Use</h2>
					<br>
					<p class="p1"><strong>REGULAMENTO&nbsp;</strong><strong>“YOUNG TALENT”</strong></p>
					<p class="p1"><strong><br></strong></p>
					<p class="p1">A THE TALENT CITY, UNIPESSOAL, LDA, n.º 508254043, com o capital social de 50.000.000 euros, com sede na Rua de Gondarém, nº 956, Porto, Portugal, lançará no período compreendido entre 01 de Março e 30 de Abril de 2016, o programa “Tell Us Your Story” destinado a estudantes universitários maiores de idade que frequentem o ensino superior. O presente desafio obedecerá às seguintes condições:</p>
					<p class="p1"><strong>1.Objectivo</strong></p>
					<p class="p1"><strong>1.1.</strong>O Tell Us Your Story tem por objectivo fomentar uma maior proximidade entre os estudantes universitários e as empresas parceiros no que toca a valores, cultura e reputação enquanto empregador, facultando oportunidades de desenvolvimento de “soft skills” que contribuirão para o sucesso dos participantes em futuros processos de recrutamento, tanto nacionais como internacionais.</p>
					<p class="p1"><strong>1.2.</strong>O Desafio Tell Us Your Story consiste na gravação de vídeos de auto apresentação (adiante Story), comuns em contexto de processos de recrutamento, através de um website que apoia os participantes na respectiva elaboração dos mesmos.</p>
					<p class="p1"><strong>1.3.</strong>Os participantes poderão gravar e apresentar mais do que um Story, selecionando os factores que consideram mais relevantes e/ou adequados.</p>
					<p class="p1"><strong><br></strong></p>
					<p class="p1"><strong>2.Participação</strong></p>
					<p class="p1">Cada participante poderá apresentar o seu Story, através dos seguintes canais:&nbsp;</p>
					<p class="p1"><strong>2.1.</strong>Através do website yourstory.ytconference.com;</p>
					<p class="p1"><strong>2.2.</strong>Caso queiram participar, o universitários devem (1) Inscrever-se na plataforma online (2) Aceitar o Regulamento e a Política de Privacidade e Termos de Utilização (3) Gravar em vídeo a sua Story (4) Fazer o upload da sua Story.</p>
					<p class="p1"><strong>2.3.</strong>A cada inscrição válida corresponderá uma senha de acesso pessoal e intransmissível. </p>
					<p class="p1"><strong>2.4.</strong>O participante terá de proceder à gravação da sua Story directamente na plataforma online.&nbsp;</p>
					<p class="p1"><strong> </strong></p>
					<p class="p1"><strong>3.Avaliação</strong></p>
					<p class="p1"><strong>3.1.</strong>A avaliação das Stories terá por base os seguintes elementos:</p>
					<p class="p1">3.1.1.Estrutura do Discurso;</p>
					<p class="p1">3.1.2.Relevância do Conteúdo;</p>
					<p class="p1">3.1.3.Impacto e Influência;</p>
					<p class="p1">3.1.4.Linguagem não-verbal;</p>
					<p class="p1">3.1.5.Dinamismo.</p>
					<p class="p1"><strong>3.2.</strong>A avaliação dos Storys será realizada em conjunto por representantes da “The Talent City”.</p>
					<p class="p1"><strong><br></strong></p>
					<p class="p1"><strong>4.Prémios</strong></p>
					<p class="p1"><strong>4.1.</strong>Todos os participantes receberão, através de e-mail, avaliação relativamente à Story apresentada, bem como sugestões de como melhorar as suas competências de comunicação.</p>
					<p class="p1"><strong>4.2.</strong>Os participantes autores das 150 melhores Stories serão convidados a participar na Tell Us Your Story Conference, uma conferência para jovens talentos durante a qual serão realizadas diferentes actividades.</p>
					<p class="p1"><strong><br></strong></p>
					<p class="p1"><strong>5.Fale Connosco&nbsp;</strong></p>
					<p class="p1">Para qualquer esclarecimento adicional sobre o Regulamento do Tell Us Your Story, envie-nos um email para hello@ytconference.com ou uma carta para a morada Av. Fontes Pereira de Melo 14, 4º Lisboa, 1050-121 Portugal.</p>
					<p class="p1"><strong><br></strong></p>
					<p class="p1">&nbsp;</p>
					<p class="p1"><strong>POLÍTICA DE PRIVACIDADE E TERMOS DE UTILIZAÇÃO</strong></p>
					<p class="p2">&nbsp;</p>
					<p class="p1">A presente Política de Privacidade e Termos de Utilização destina-se a informar o utilizador do Tell Us Your Story das suas políticas e procedimentos no que respeita à recolha, utilização e divulgação de quaisquer Dados Pessoais enviados através do nosso website http://yourstory.ytconference.com</p>
					<p class="p1">Os dados pessoais que sejam disponibilizados pelo utilizador através do registo no website só poderão ser acedidos pelo utilizador e pela administração do website.</p>
					<p class="p2">&nbsp;<br><strong>1.Objectivo/Serviços</strong></p>
					<p class="p2">&nbsp;</p>
					<p class="p1">O Tell Us Your Story consiste num website que tem por objectivo apoiar os seus utilizadores na elaboração de um discurso de auto-apresentação, através do qual o utilizador receberá dicas como melhor se destacar neste exercício muito comum nos processos de recrutamento actuais.</p>
					<p class="p1">Assim, o Tell Us Your Story apoia o utilizador na elaboração do discurso perfeito, filmando um vídeo sobre o qual poderá receber feedback. Para tal, o utilizador, após registar-se no Tell Us Your Story, terá acesso a um vídeo em que lhe é explicado porque é importante trabalhar o seu Story de apresentação, bem como lhe é passado algumas dicas relativamente a técnicas que poderá utilizar para que o discurso seja o mais próximo da “perfeição”.</p>
					<p class="p1">O respeito pelos dados pessoais que o utilizador fornece ao Tell Us Your Story é muito importante para a The Talent City, tendo-se criado uma Política de Privacidade e Termos de Utilização que o utilizador pode facilmente compreender.</p>
					<p class="p2">&nbsp;</p>
					<p class="p2">&nbsp;<br><strong>2.</strong><strong>Conteúdo do website / Propriedade Intelectual</strong></p>
					<p class="p4">&nbsp;</p>
					<p class="p3">Entende-se por “conteúdo do website”, toda a informação presente no mesmo, nomeadamente texto, imagens, ilustrações, design gráfico, webdesign e software. Os direitos de propriedade intelectual de todos os conteúdos do Tell Us Your Story que não sejam de fornecimento externo e como tal devidamente identificado, são pertença da The Talent City, incluindo as informações, as ferramentas, o desenho gráfico das páginas na Internet, com todos os seus componentes, e todas as figuras, gráficos ou textos. O conteúdo presente neste website não poderá ser copiado, alterado ou distribuído, salvo com autorização expressa da The Talent City. Este website contém ainda textos, ilustrações e fotografias devidamente licenciadas pelos seus autores e que não podem ser copiadas, alteradas ou distribuídas, salvo com autorização expressa dos mesmos. A The Talent City rejeita qualquer responsabilidade pela usurpação e uso indevido dos elementos acima citados, e não se responsabiliza pela veracidade dos conteúdos de todos e quaisquer anúncios, mensagens e demais informação a que o utilizador possa ter acesso através deste website, a qual caberá em exclusivo ao respectivo utilizador ou anunciante.</p>
					<p class="p2">&nbsp;<br><strong>3.</strong><strong>Confidencialidade e Utilização de Dados Pessoais</strong></p>
					<p class="p4">&nbsp;</p>
					<p class="p3">Pela própria natureza e objectivos dos serviços interactivos disponibilizados noTell Us Your Story, é requerido ao utilizador o fornecimento de contactos e/ou de informações que podem ser considerados dados pessoais. A The Talent City garante, no entanto, a todos os seus utilizadores que:
						Nenhum dado pessoal será facultado a terceiros sem o prévio consentimento do seu titular; Nenhum dos dados pessoais que nos seja confiado será facultado, por via gratuita ou comercial, a empresas de marketing directo ou outras entidades para fins de ganho financeiro ou de marketing.
						Apenas o utilizador e a administração do website poderão ter acesso aos dados pessoais introduzidos pelo utilizador (nome completo, endereço de e-mail, número de contacto telefónico e identificação do estabelecimento de ensino superior frequentado e curso).
						Caso o utilizador se identifique enviando um e-mail ou qualquer outro tipo de comunicação com perguntas ou comentários ao Tell Us Your Story, os dados cedidos poderão ser utilizados para responder às perguntas ou comentários efectuados pelo utilizador, podendo apresentar as questões ou comentários colocados pelo utilizador para referência futura.
						O Tell Us Your Story poderá também complementar as informações que recolhe com informações de outras fontes para melhor avaliar e melhorar o website.
						O Tell Us Your Story reserva-se o direito de divulgar dados pessoais actuais ou antigos:
						(i) nos casos em que entende que o website está a ser ou foi utilizado em violação dos Termos de utilização ou foram cometidos actos ilícitos;
						(ii) se o Tell Us Your Story for intimado para ceder as informações relativamente ao utilizador, nos casos previstos na lei, oTell Us Your Story remeter-lhe-á um aviso por email, dando ao utilizador a oportunidade para se defender da intimação, antes da divulgação de quaisquer informações pessoais em conformidade com a intimação;
						(iii) se o Tell Us Your Story for vendido, incorporado ou adquirido. No entanto, se o Tell Us Your Story estiver envolvido numa fusão, aquisição ou venda de todos ou parte dos seus activos, o utilizador será notificado via e-mail e/ou através de um aviso em destaque no website, de qualquer mudança na propriedade ou uso das suas informações pessoais.
						Os serviços fornecidos no Tell Us Your Story respeitarão sempre o previsto na Lei de Protecção de Dados Pessoais - Lei n.º 67/98, de 26 de Outubro.
					</p>
					<p class="p4">&nbsp;<br><strong>4.Declaração para cedência de imagens</strong></p>
					<p class="p4">&nbsp;</p>
					<p class="p1">O utilizador do Tell Us Your Story declara, para os devidos efeitos legais, que autoriza a empresa The Talent City, ou terceiros por aquela contratados, a proceder à edição, fixação e reprodução da sua voz e da sua imagem, animada ou não, captada individual ou colectivamente, para posterior publicação, reprodução e/ou divulgação, sem limite temporal ou territorial, em suporte de vídeo com o objectivo de elaboração do currículo do referido utilizador e ainda para fins comerciais, nomeadamente, de divulgação e promoção do websiteTell Us Your Story. Pela utilização das imagens do utilizador não será devida qualquer contrapartida. A The Talent City e Tell Us Your Story comprometem-se a solicitar a autorização do utilizador para a utilização das imagens para quaisquer outros fins que não os constantes da presente declaração.
						As imagens cedidas não poderão, em caso algum, ser cedidas a outrem sem a expressa e prévia autorização do utilizador.
					</p>
					<p class="p2">&nbsp;<br><strong>5.Recolha de Dados Não-Pessoais e respectiva Utilização</strong></p>
					<p class="p2">&nbsp;</p>
					<p class="p1">O Tell Us Your Story, a fim de melhorar o design e o conteúdo do website e para poder personalizar a experiência do utilizador na Internet poderá proceder à recolha de determinadas informações que não permitam a identificação do utilizador quando este visitar muitas das páginas do website. Tais informações incluem o tipo de navegador que o utilizador usa (por exemplo, Safari, Chrome, Internet Explorer), o seu endereço de IP, o tipo de sistema operacional (por exemplo, Windows ou iOS ) e o nome de domínio do fornecedor de serviços de Internet.</p>
					<p class="p2">&nbsp;<br><strong>6.Cookies&nbsp;</strong></p>
					<p class="p2">&nbsp;</p>
					<p class="p1">Visando proporcionar aos nossos utilizadores uma maior rapidez e personalização do serviço prestado, o Tell Us Your Story poderá recorrer a uma funcionalidade do “browser” conhecida como “cookie”. Um “cookie” é um pequeno ficheiro de texto, automaticamente guardado pelo computador do utilizador, e que permite a sua identificação sempre que este volte a consultar, neste caso, o website Tell Us Your Story.
						Qualquer utilizador pode, no entanto, configurar o seu “browser” por forma a impedir a instalação de “cookies” no seu computador. Contudo, essa opção poderá tornar a sua navegação mais lenta, neste como noutros sites da Internet. Para obter mais informações sobre cookies, visite www.cookiecentral.com.
					</p>
					<p class="p2">&nbsp;<br><strong>7.Dados Agregados ou de Grupo</strong></p>
					<p class="p2">&nbsp;</p>
					<p class="p1">O Tell Us Your Story reserva-se o direito de transferir e/ou vender os dados agregados ou de grupo sobre os utilizadores para fins legais. Os dados agregados ou de grupo são dados que descrevem os dados demográficos, uso ou outras características dos utilizadores do Tell Us Your Story como um grupo, sem revelar informações que permitam identificar os utilizadores.</p>
					<p class="p2">&nbsp;<br><strong>8.Deveres dos Utilizadores/ Utilizações Proibidas</strong></p>
					<p class="p2">&nbsp;</p>
					<p class="p1">O utilizador obriga-se a não atacar ou usar ilicitamente os sistemas ou o website Tell Us Your Story, sendo responsabilizado e suportando todos os custos associados a acções ilícitas que lhe sejam atribuídas. Entre outras, consideram-se como acções ilícitas:</p>
					<p class="p1"><strong>a)</strong> Aceder a uma área/conta não autorizada e respectiva informação;</p>
					<p class="p1"><strong>b)</strong> Testar e avaliar a vulnerabilidade do sistema e quebrar a segurança instalada;</p>
					<p class="p1"><strong>c)</strong> Instalar ou tentar instalar um vírus no portal;</p>
					<p class="p1"><strong>d)</strong> Envio de informações não solicitadas que incluam promoções ou publicidade a produtos ou serviços;</p>
					<p class="p1"><strong>e)</strong> Desencadear ou tentar desencadear ataques tipo "denial of service".</p>
					<p class="p1">&nbsp;<br><strong>9. Suspensão ou Interrupção de Acesso</strong></p>
					<p class="p2">&nbsp;</p>
					<p class="p1"><strong>&nbsp;</strong>A The Talent City reserva-se o direito de interromper ou suspender o acesso ao website Tell Us Your Story, pelo período que entenda necessário, por quaisquer razões de ordem técnica, administrativa, de força maior ou outras.</p>
					<p class="p1"><strong>&nbsp;<br></strong><strong>10.Segurança</strong></p>
					<p class="p2">&nbsp;</p>
					<p class="p1"><strong>&nbsp;</strong>A privacidade dos dados pessoais dos seus utilizadores é muito importante para a The Talent City. Como tal, a The Talent City esforça-se para salvaguardar e proteger as informações pessoais dos seus utilizadores. Quando os seus utilizadores disponibilizam os seus dados pessoais estes são protegidos nos modos on-line e off-line. Caso sejam cedidos dados pessoais sensíveis pelos utilizadores, tais dados são criptografados e protegidos com um software de criptografia SSL.</p>
					<p class="p1">O acesso aos dados pessoais dos utilizadores é estritamente limitada, e a The Talent City dispõe e coloca em prática as medidas técnicas e organizativas adequadas a proteger os dados pessoais contra a destruição acidental ou ilícita, a alteração, difusão ou o acesso não autorizados. Para tal, os servidores que armazenam os dados pessoais são mantidos num ambiente físico seguro, ressalvando que nenhuma instalação de armazenamento, tecnologia, software, protocolos de segurança ou transmissão de dados pela Internet pode ser garantida como 100% segura.</p>
					<p class="p1">Todos os dados pessoais dos utilizadores do Story só são acessíveis pelos escritórios da The Talent City, sendo que somente os funcionários ou agentes terceiros que precisam de informações pessoais para realizar um trabalho específico têm acesso às informações pessoais dos utilizadores. A The Talent City informa ainda que, para garantir a segurança dos seus utilizadores, os funcionários que não aderem às suas políticas firmes de protecção de dados estão sujeitos a responsabilidade nos termos previstos na Lei de Protecção de Dados Pessoais – Lei n.º 67/98, de 26 de Outubro.</p>
					<p class="p1">Caso seja identificada uma violação de segurança no que diz respeito aos dados pessoais do utilizador, este será notificado via e-email. A The Talent City informa que a referida notificação poderá não ser automática, a fim de atender às necessidades de determinação da extensão dos danos da rede, podendo envolver medidas correctivas.</p>
					<p class="p1">Os utilizadores do Tell Us Your Story declaram e garantem que conhecem perfeitamente as características e os constrangimentos, limitações e defeitos da Internet, e nomeadamente que as transmissões de dados e de informações via Internet beneficiam apenas duma fiabilidade técnica relativa, circulando em redes heterogéneas de características e capacidades técnicas diversas, que perturbam o acesso ou que o tornam impossível em certos períodos. Os utilizadores reconhecem que qualquer website está sujeito a intromissões de terceiros não autorizados e que pode consequentemente ficar interrompido, e que as informações que circulam na Internet não estão protegidas contra eventuais desvios (acesso livre), contra eventuais vírus, e que qualquer pessoa é susceptível de criar uma ligação com acesso ao site/portal e/ou a elementos lá contidos, aceitando correr os riscos inerentes.</p>
					<p class="p1">A The Talent City não poderá em nenhum caso ser responsabilizada por danos acidentais ou involuntários sofridos pelos utilizadores e provocados ou não por terceiros no âmbito da utilização dos serviços fornecidos neste website ou em outros lugares na Internet a que tenham tido acesso através dele.</p>
					<p class="p1">A The Talent City não é responsável por quaisquer danos que possam ser causados pela utilização do serviço, incluindo a contaminação de vírus.</p>
					<p class="p2">&nbsp;<br><strong>11.Exclusão, Correcção e Actualização de Informação dos Dados Pessoais</strong></p>
					<p class="p2">&nbsp;</p>
					<p class="p1">Cada utilizador dos serviços interactivos disponibilizados no Tell Us Your Story é responsável e titular dos dados que transmita ao Tell Us Your Story, podendo controlar a quantidade de informação fornecida e quando (e em que circunstâncias) esta poderá, ou deverá, ser facultada a terceiros.</p>
					<p class="p1">O utilizador poderá a todo o momento alterar e eliminar os dados inseridos no Tell Us Your Story, bastando para tal enviar um e-mail <a href="mailto:ajuda@theperfectpitch.pt">ajuda@theperfectpitch.pt</a>&nbsp;para solicitar a eliminação, com a respectiva identificação, especificando quais os dados que pretende ver alterados ou removidos.</p>
					<p class="p1">Informa-se que a exclusão dos seus dados pessoais irá determinar a impossibilidade de acesso aos serviços do Tell Us Your Story, determinando também o encerramento da sua conta associada.</p>
					<p class="p1">Se o utilizador pretender continuar a utilizar a sua conta não poderá apagar os dados pessoais existentes no seu arquivo.</p>
					<p class="p1">Informa-se, previamente, que a exclusão dos seus dados pessoais e cópias residuais poderá demorar um período de tempo até que sejam excluídas dos servidores activos, podendo permanecer nos sistemas de backup.</p>
					<p class="p2">&nbsp;<br><strong>12.Alterações à Política de Privacidade e Termos de Utilização</strong></p>
					<p class="p2">&nbsp;</p>
					<p class="p1">O Tell Us Your Story reserva-se o direito de alterar, actualizar suspender ou cancelar a presente Política de Privacidade e Termos de Utilização a qualquer momento, publicando uma informação no website para conhecimento dos seus utilizadores. Se a forma como o Tell Us Your Story utiliza os dados pessoais for alterada, o Tell Us Your Story notificará os seus utilizadores membros através do envio da política modificada para os seus utilizadores através de e-mail, para que prestem o seu consentimento.&nbsp;</p>
					<p class="p2">&nbsp;<br><strong>13. Aceitação e Vinculação</strong></p>
					<p class="p2">&nbsp;</p>
					<p class="p1"><strong>&nbsp;</strong>Sem prejuízo do compromisso de confidencialidade (que se deve ter como uma obrigação de meios) referente à utilização de dados pessoais, a The Talent City alerta que existem riscos relacionados com a Internet e bases de dados, sendo possível que os dados pessoais constantes do website possam ser captados e/ou transferidos por terceiros, nomeadamente em países onde os sistemas de protecção de bases de dados pessoais se encontrem ainda em fases pouco desenvolvidas e onde a protecção é escassa e ineficaz.</p>
					<p class="p1">Os utilizadores ao acederem ao website Tell Us Your Story deverão aceitar correr os riscos inerentes à sua actividade como Internauta, nomeadamente o risco de eventual transferência de dados em aberto.</p>
					<p class="p1">Todo o utilizador dos serviços disponibilizados neste sítio está vinculado à aceitação e respeito pelas condições aqui expressas.</p>
					<p class="p2">&nbsp;<br><strong>14.Fale Connosco&nbsp;</strong></p>
					<p class="p2">&nbsp;&nbsp;</p>
					<p class="p1">Para qualquer esclarecimento adicional sobre a Política de Privacidade e Termos de Utilização do Tell Us Your Story, envie-nos um email para <a href="mailto:hello@ytconference.com">hello@ytconference.com</a> ou uma carta para a morada Av. Fontes Pereira de Melo nº 14 - 4º andar, 1050-121 Lisboa,&nbsp; Portugal.</p>					<br><br>
					<a href="#close" style="color:#000; text-transform:uppercase; font-weight:bold;">Fechar</a>
				</div>
			</div>
		</div>
	</div>
</header>


@stop 
{{-- Scripts --}} 
@section('scripts')
<script>
	//Asynchronous call
	$('form').submit(function (event) {
		event.preventDefault();
		var form = $(this);

		$.ajax({
			type: form.attr('method'),
			url: form.attr('action'),
			data: form.serialize()
		}).success(function () {
			$('#success').show('slow');
			setTimeout(function () {
				window.location = "{{URL::to('intro')}}";
			}, 10);
		}).fail(function (jqXHR, textStatus, errorThrown) {
			// Optionally alert the user of an error here...
			var textResponse = jqXHR.responseText;
			var alertText = "As seguintes condições não estão preenchidas:\n\n";
			var jsonResponse = jQuery.parseJSON(textResponse);

			$.each(jsonResponse, function (n, elem) {
				alertText = alertText + elem + "\n";
			});

			alert(alertText);
		});
	});
	$('#birthdate').datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: "yy-mm-dd",
		yearRange: "-100:+0",
		dayNamesMin: ['Do', 'Se', 'Te', 'Qa', 'Qi', 'Se', 'Sa'],
		monthNames: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
		monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez']
	});
	
	//Hiden fields
	$('#university').change(function () {
		var selected = $(this).find(":selected").text();
		var id = $(this).find(":selected").val();
		$('#university_block').hide();
		$('#university_custom').val('');
		if (selected == 'Outra') {
			$('#university_block').show();
		}
		else if (selected != 'Universidade') {
			$.ajax({
				url: '{{URL::to("getDegrees")}}',
				type: 'post',
				dataType: 'json',
				success: function (response) {
					$('#degree').html('');
					$.each(response, function () {
						$('#degree').append($("<option />").val(this.id).text(this.name + (typeof this.degree_type !== "undefined" ? ' - '+this.degree_type : '')));
					});
				},
				data: {id: id}
			});
		}

	});
	$('#degree').change(function () {
		var selected = $(this).find(":selected").text();

		$('#degree_block').hide();
		$('#degree_custom').val('');
		if (selected == 'Outro') {
			$('#degree_block').show();
		}
	});
	
	//Requirements
	$('#name').focusin(function(){
		$('<div class="outer_note">\
				<div class="inner_note">\
					Campo de preenchimento obrigatório.\
				</div>\
			</div>').hide().insertAfter(this).fadeIn("slow");
	}).focusout(function(){
		$(this).next().remove().fadeIn("slow");
	});
	$('#email').focusin(function(){
		$('<div class="outer_note">\
				<div class="inner_note">\
					Campo de preenchimento obrigatório.\
				</div>\
			</div>').hide().insertAfter(this).fadeIn("slow");
	}).focusout(function(){
		$(this).next().remove().hide().fadeIn("slow");
	});
	$('#password').focusin(function(){
		$('<div class="outer_note">\
				<div class="inner_note">\
					Campo de preenchimento obrigatório.<br/>\
					Tamanho mínimo de 8 caracteres e deve conter pelo menos uma letra maiúscula, uma minúscula e um algarismo.\
				</div>\
			</div>').hide().insertAfter(this).fadeIn("slow");
	}).focusout(function(){
		$(this).next().remove();
	});
	$('#confirm_password').focusin(function(){
		$('<div class="outer_note">\
				<div class="inner_note">\
					Campo de preenchimento obrigatório.\
				</div>\
			</div>').hide().insertAfter(this).fadeIn("slow");
	}).focusout(function(){
		$(this).next().remove();
	});
	$('#birthdate').focusin(function(){
		$('<div class="outer_note">\
				<div class="inner_note">\
					Campo de preenchimento obrigatório.\
				</div>\
			</div>').hide().insertAfter(this).fadeIn("slow");
	}).focusout(function(){
		$(this).next().remove();
	});
	$('#phone').focusin(function(){
		$('<div class="outer_note">\
				<div class="inner_note">\
					Campo de preenchimento obrigatório.\
					Para te actualizarmos sobre a tua viagem na YTC. Uma mensagem por semana, máx! No stalking.\
				</div>\
			</div>').hide().insertAfter(this).fadeIn("slow");
	}).focusout(function(){
		$(this).next().remove();
	});
	$('#address').focusin(function(){
		$('<div class="outer_note">\
				<div class="inner_note">\
					Para te enviarmos YTC Awesomenesses para casa. Se quiseres.\
				</div>\
			</div>').hide().insertAfter(this).fadeIn("slow");
	}).focusout(function(){
		$(this).next().remove();
	});
	$('#university').focusin(function(){
		$('<div class="outer_note">\
				<div class="inner_note">\
					Campo de preenchimento obrigatório.\
					Caso a tua universidade não conste da lista, escolhe a opção "outra" e preenche com o nome da mesma.\
				</div>\
			</div>').hide().insertAfter(this).fadeIn("slow");
	}).focusout(function(){
		$(this).next().remove();
	});
</script>
@stop
