<?php

return [
    'create' => 'Create',
    'edit' => 'Edit',
    'reset' => 'Reset',
    'cancel' => 'Cancel',
    'general' => 'General',
    'title' => 'Title',
    'new' => 'New',
	'associate' => 'Associate',
    'delete' => 'Delete',
    'items' => 'Items',
    'delete_message' => 'Did you want to delete this item?',
    'delete' => 'Delete',
	'thumbnail' => 'Thumbnail',
	'new_and_associate' => 'New and associate',
	'document' => 'Document',
	'detail' => 'Detail',
	'now' => 'If no value is submited defaults to "Now"',
	'unpublish' => 'Unpublish',
	'name' => 'Name',
	'description' => 'Description',
	'file' => 'File',
	'available_at' => 'Available at',
	'category' => 'Category'
];