<?php

return [
	'home' => 'Home',
	'login' => 'Login',
	'sign_up' => 'Sign up',
	'admin_panel' => 'Admin Panel',
	'logout' => 'Logout',
	'login_as' => 'Logged in as',
	'display_favourites' => 'Display the favorite contents',
	'account_management' => 'Account management',
	'cancel' => 'Cancel',
	'back' => 'Back',
	'close' => 'Close',
	'register' => 'Register',
	'privacy' => 'Privacy policy',
	'terms' => 'Terms of use',
];
