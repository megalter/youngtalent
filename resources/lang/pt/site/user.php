<?php

return [
	'login' => 'Entrar',
	'login_to_account' => 'Entrar na conta',
	'submit' => 'Gravar',
	'register' => 'Registar',
	'remember' => 'Guardar sessão',
	'password' => 'Password',
	'e_mail' => 'E-mail',
	'email' => 'E-mail',
	'password_confirm' => 'Confirmar password',
	'confirmation_required' => 'Confirmação é obrigatória',
	'name' => 'Nome',
	'first_name' => 'Primeiro nome',
	'last_name' => 'Último nome',
	'home' => 'Home',
	'email_invalid' => 'O E-mail introduzido não é válido',
	'email_required' => 'O E-mail é obrigatório',
	'email_unique' => 'O E-mail introduzido já foi utilizado',
	'name_required' => 'O Nome é obrigatório',
	'password_required' => 'A Password é obrigatória',
	'password_confirmed' => 'A Confirmação de password é obrigatória',
    'change_password' => 'Mudar password',
	'lost_password' => 'Recuperar password',
	'send' => 'Enviar',
	'user_code' => 'Código de registo'
];
