<?php

return array(

    /*
     |--------------------------------------------------------------------------
     | Laravel CORS Defaults
     |--------------------------------------------------------------------------
     |
     | The defaults are the default values applied to all the paths that match,
     | unless overridden in a specific URL configuration.
     | If you want them to apply to everything, you must define a path with *.
     |
     | allowedOrigins, allowedHeaders and allowedMethods can be set to array('*') 
     | to accept any value, the allowed methods however have to be explicitly listed.
     |
     */
    'defaults' => [
		'supportsCredentials' => true,
		'allowedOrigins' => ['*'],
		'allowedHeaders' => ['Content-Type', 'Accept', 'Authorization'],
		'allowedMethods' => ['GET', 'POST', 'PUT', 'DELETE'],
		'exposedHeaders' => [],
		'maxAge' => 3600,
		'hosts' => [],
	],
	'paths' => [
		'api/*' => [
			'alllowedOrigins' => ['*'],
			'supportsCredentials' => true,
			'allowedHeaders' => ['Content-Type', 'Accept', 'Authorization'],
			'allowedMethods' => ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
		]
	],
);
